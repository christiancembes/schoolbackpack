<?php

return [

    'registration_prefix' => env('REGISTRATION_PREFIX', 'R'),

    'quotation_prefix' => env('QUOTATION_PREFIX', 'Q'),

    'quotation_schedule_prefix' => env('QUOTATION_SCHEDULE_PREFIX', 'QS'),

    'marketer_fee_trigger' => env('MARKETER_FEE_TRIGGER', 0),

    'marketer_fee' => env('MARKETER_FEE', 0)

];

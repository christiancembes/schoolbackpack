#!/bin/bash

### Fresh Setup
php artisan migrate:fresh
php artisan passport:install --force
php artisan db:seed

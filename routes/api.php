<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::group(['middleware' => 'auth:api'], function() {
        Route::group(['namespace' => 'Api\User'], function() {
            Route::get('/me', 'UserController@me');
            Route::get('/me/students', 'UserController@meStudent');
        });

        Route::group(['namespace' => 'Api\Payment'], function() {
            Route::get('payments', 'PaymentController@getAll');
            Route::get('payments/{code}', 'PaymentController@getByCode');
        });

        Route::group(['namespace' => 'Api\Quotation'], function() {
            Route::get('quotations', 'QuotationController@getAll');
            Route::get('quotations/{code}', 'QuotationController@getByCode');
        });

        Route::group(['namespace' => 'Api\Report'], function() {
            Route::get('reports', 'ReportController@getAll');
            Route::get('reports/{id}', 'ReportController@getById');

            Route::get('reports/dormitory/monthly', 'ReportController@getDormitoryMonthlyAll');
            Route::get('reports/dormitory/monthly/{id}', 'ReportController@getDormitoryMonthlyById');

            Route::get('reports/dormitory/term', 'ReportController@getDormitoryTermAll');
            Route::get('reports/dormitory/term/{id}', 'ReportController@getDormitoryTermById');
        });
    });

    Route::group(['namespace' => 'Api\Auth'], function() {
        Route::post('/auth/register', 'AuthController@register')->middleware('transaction');
        Route::post('/auth/login', 'AuthController@login');
        Route::post('/auth/logout', 'AuthController@logout');
    });

    Route::group(['namespace' => 'Api\Student'], function() {
        Route::get('/students/{id}', 'StudentController@getDetail');
    });

    Route::group(['namespace' => 'Api\Registration'], function() {
        Route::post('/pre-registration-1', 'RegistrationController@preRegister1');
        Route::post('/pre-registration-2', 'RegistrationController@preRegister2');
        Route::post('/pre-registration-3', 'RegistrationController@preRegister3');
        Route::post('/pre-registration-5', 'RegistrationController@preRegister5');
        Route::post('/registration', 'RegistrationController@register')->middleware('transaction');
    });

    Route::group(['namespace' => 'Api\Payment'], function() {
        Route::post('payment-confirmation', 'PaymentController@confirmation')->middleware('transaction');
    });

    Route::group(['namespace' => 'Api\Location'], function() {
        Route::get('/countries', 'LocationController@country');
        Route::get('/provinces', 'LocationController@province');
        Route::get('/cities/{provinceId}', 'LocationController@city');
        Route::get('/districts/{cityId}', 'LocationController@district');
        Route::get('/villages/{districtId}', 'LocationController@village');
    });

    Route::group(['namespace' => 'Api\General'], function() {
        Route::get('/school-periods', 'GeneralController@schoolPeriod');
        Route::get('/school-fees', 'GeneralController@schoolFee');
        Route::get('/facilities', 'GeneralController@facility');
        Route::get('/grades', 'GeneralController@grade');
        Route::get('/grade-classes/{gradeId}', 'GeneralController@gradeClasses');
        Route::get('/majors', 'GeneralController@major');
        Route::get('/marketers', 'GeneralController@marketer');
        Route::get('/student-categories', 'GeneralController@studentCategory');
        Route::get('/student-types', 'GeneralController@studentType');
        Route::get('/subjects', 'GeneralController@subject');
    });

    Route::group(['namespace' => 'Api\Mailer'], function() {
        Route::post('/mailer', 'MailerController@send')->middleware(['auth:api']);
    });
});

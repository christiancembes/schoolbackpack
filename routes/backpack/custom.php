<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes

    Route::group([
        'prefix' => 'student',
        'namespace' => 'Student',
        'middleware' => ['transaction']
    ], function() {
        Route::crud('student', 'StudentCrudController');
        Route::crud('waiting', 'StudentWaitingCrudController');
        Route::crud('waiting-payment', 'StudentWaitingPaymentCrudController');
        Route::crud('cancel', 'StudentCancelCrudController');
        Route::crud('mutation', 'StudentMutationCrudController');
        Route::crud('lost', 'StudentLostCrudController');
        Route::crud('pass-away', 'StudentPassAwayCrudController');

        Route::post('student/{id}/cancel', 'StudentCrudController@cancelRegistration');

        Route::crud('reports', 'ReportsCrudController');
        Route::post('reports/{id}/send', 'ReportsCrudController@sendByReport');

        Route::post('api/student', 'StudentCrudController@getStudent');
        Route::post('api/student/active', 'StudentCrudController@getStudentActive');
    });

    Route::group([
        'prefix' => 'payment',
        'namespace' => 'Payment',
        'middleware' => ['transaction']
    ], function() {
        Route::crud('payment-method', 'PaymentMethodCrudController');
        Route::crud('payment-gateway', 'PaymentGatewayCrudController');
        Route::crud('payment-transaction', 'PaymentTransactionCrudController');
        Route::crud('payment-confirmation', 'PaymentConfirmationCrudController');

        Route::post('payment-transaction/confirmation/{id}', 'PaymentConfirmationCrudController@confirmation');
        Route::post('payment-confirmation/confirmation/{id}', 'PaymentConfirmationCrudController@confirmation');
    });

    Route::group([
        'prefix' => 'quotation',
        'namespace' => 'Quotation',
        'middleware' => ['transaction']
    ], function() {
        Route::crud('quotation', 'QuotationCrudController');
        Route::crud('quotation_details', 'QuotationDetailCrudController');

        Route::post('quotation/send/{id}', 'QuotationCrudController@send');
    });

    Route::group([
        'prefix' => 'master',
        'namespace' => 'Master',
        'middleware' => ['transaction']
    ], function() {
        Route::crud('employee', 'EmployeeCrudController');
        Route::crud('facility', 'FacilityCrudController');
        Route::crud('grade', 'GradeCrudController');
        Route::crud('grade-class', 'GradeClassCrudController');
        Route::crud('major', 'MajorCrudController');
        Route::crud('marketer', 'MarketerCrudController');
        Route::crud('marketer-payout', 'MarketerPayoutCrudController');
        Route::crud('school-period', 'SchoolPeriodCrudController');
        Route::crud('student-type', 'StudentTypeCrudController');
        Route::crud('subject', 'SubjectCrudController');
        Route::crud('dormitory-subject', 'DormitorySubjectCrudController');
        Route::crud('school-fee', 'SchoolFeeCrudController');
        Route::crud('student-category', 'StudentCategoryCrudController');

        Route::post('api/school-fee/user', 'SchoolFeeCrudController@schoolFeeByUser');
        Route::post('api/marketer/request-payout', 'MarketerCrudController@requestPayout');
        Route::post('api/marketer/confirm-payout/{id}', 'MarketerCrudController@confirmPayout');
    });

    Route::group([
        'prefix' => 'general',
        'namespace' => 'General',
        'middleware' => ['transaction']
    ], function() {
        Route::crud('country', 'CountryCrudController');
        Route::crud('province', 'ProvinceCrudController');
        Route::crud('city', 'CityCrudController');
        Route::crud('district', 'DistrictCrudController');
        Route::crud('village', 'VillageCrudController');

        Route::post('api/province', 'ProvinceCrudController@getProvince');
        Route::post('api/city', 'CityCrudController@getCity');
        Route::post('api/district', 'DistrictCrudController@getDistrict');
        Route::post('api/village', 'VillageCrudController@getVillage');
    });

    Route::group([
        'prefix' => 'dormitory',
        'namespace' => 'Dormitory',
        'middleware' => ['transaction']
    ], function() {
        Route::crud('dormitory', 'DormitoryCrudController');
        Route::crud('dormitory-waiting-list', 'DormitoryWaitingListCrudController');

        Route::crud('report/monthly', 'Report\DormitoryReportMonthlyCrudController');
        Route::crud('report/term', 'Report\DormitoryReportTermCrudController');

        Route::post('api/available-dormitory', 'DormitoryCrudController@getAvailableDormitory');
    });
}); // this should be the absolute last line of this file
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDormitoryStudentTermsReportsTableAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dormitory_student_terms_reports', function(Blueprint $table) {
            $table->json('life_skill')->after('grade');
            $table->json('behavior')->after('grade');
            $table->json('academic')->after('grade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dormitory_student_terms_reports', function(Blueprint $table) {
            $table->dropColumn('academic');
            $table->dropColumn('behavior');
            $table->dropColumn('life_skill');
        });
    }
}

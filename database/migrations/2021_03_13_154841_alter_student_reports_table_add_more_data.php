<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterStudentReportsTableAddMoreData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_reports', function(Blueprint $table) {
            $table->text('parent_note')->after('terms')->nullable();
            $table->text('guardian_note')->after('terms')->nullable();
            $table->json('absence')->after('terms');
            $table->json('achievement')->after('terms');
            $table->json('extracurricular')->after('terms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_reports', function(Blueprint $table) {
            $table->dropColumn('parent_note');
            $table->dropColumn('guardian_note');
            $table->dropColumn('absence');
            $table->dropColumn('achievement');
            $table->dropColumn('extracurricular');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDormitoryStudentTermsReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dormitory_student_terms_reports', function (Blueprint $table) {
            $table->id();
            $table->integer('term');
            $table->integer('total_score');
            $table->integer('value');
            $table->char('grade');
            $table->unsignedBigInteger('student_id')->index();
            $table->unsignedInteger('school_period_id')->index();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('school_period_id')->references('id')->on('school_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dormitory_student_terms_reports');
    }
}

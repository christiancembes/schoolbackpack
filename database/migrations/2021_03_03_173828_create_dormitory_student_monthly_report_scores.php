<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDormitoryStudentMonthlyReportScores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dormitory_student_monthly_report_scores', function (Blueprint $table) {
            $table->id();
            $table->integer('score');
            $table->char('grade');
            $table->unsignedBigInteger('dormitory_student_monthly_report_id');
            $table->unsignedInteger('dormitory_subject_id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('dormitory_student_monthly_report_id', 'dormitory_student_monthly_report')->references('id')->on('dormitory_student_monthly_reports');
            $table->foreign('dormitory_subject_id', 'dormitory_subject_monthly')->references('id')->on('dormitory_subjects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dormitory_student_monthly_report_scores');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('name');
            $table->double('amount');
            $table->text('description');
            $table->string('payment_system');
            $table->unsignedInteger('school_period_id')->index()->nullable();
            $table->unsignedInteger('grade_id')->index()->nullable();
            $table->unsignedInteger('student_type_id')->index()->nullable();

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->foreign('school_period_id')->references('id')->on('school_periods');
            $table->foreign('grade_id')->references('id')->on('grades');
            $table->foreign('student_type_id')->references('id')->on('student_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_fees');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentHealthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_healths', function (Blueprint $table) {
            $table->id();
            $table->string('tbc')->nullable();
            $table->string('malaria')->nullable();
            $table->string('chotipa')->nullable();
            $table->string('cacar')->nullable();
            $table->string('others')->nullable();
            $table->string('physical_abnormalities')->nullable();
            $table->unsignedBigInteger('student_id')->index();

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('student_id')->references('id')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_healths');
    }
}

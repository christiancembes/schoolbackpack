<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterDormitoryStudentMonthlyReportsTableAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dormitory_student_monthly_reports', function(Blueprint $table) {
            $table->json('life_skill')->after('month');
            $table->json('behavior')->after('month');
            $table->json('academic')->after('month');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dormitory_student_monthly_reports', function(Blueprint $table) {
            $table->dropColumn('academic');
            $table->dropColumn('behavior');
            $table->dropColumn('life_skill');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('nis', 20)->unique()->index()->nullable();
            $table->string('nisn', 20)->unique()->index();
            $table->string('full_name');
            $table->string('name');
            $table->char('gender');
            $table->string('place_of_birth');
            $table->date('date_of_birth');
            $table->string('goal');
            $table->string('hobby');
            $table->string('nik');
            $table->integer('order_in_family');
            $table->integer('numbers_of_siblings');
            $table->integer('numbers_of_step_families');
            $table->integer('numbers_of_adopted_families');
            $table->integer('weight');
            $table->integer('height');
            $table->char('blood_type');
            $table->char('mother_language');
            $table->string('name_of_sibling')->nullable();
            $table->tinyInteger('status');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('country_id')->index()->nullable();
            $table->unsignedInteger('student_category_id')->index()->nullable();
            $table->unsignedInteger('student_type_id')->index()->nullable();
            $table->unsignedInteger('grade_id')->index()->nullable();
            $table->unsignedInteger('grade_class_id')->index()->nullable();
            $table->unsignedInteger('grade_of_sibling_id')->nullable();
            $table->unsignedInteger('facility_id')->index()->nullable();
            $table->unsignedInteger('major_id')->index()->nullable();
            $table->unsignedInteger('school_period_id')->index()->nullable();
            $table->unsignedInteger('marketer_id')->index()->nullable();

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('student_category_id')->references('id')->on('student_categories');
            $table->foreign('student_type_id')->references('id')->on('student_types');
            $table->foreign('grade_id')->references('id')->on('grades');
            $table->foreign('grade_class_id')->references('id')->on('grade_classes');
            $table->foreign('grade_of_sibling_id')->references('id')->on('grades');
            $table->foreign('facility_id')->references('id')->on('facilities');
            $table->foreign('major_id')->references('id')->on('majors');
            $table->foreign('school_period_id')->references('id')->on('school_periods');
            $table->foreign('marketer_id')->references('id')->on('marketers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}

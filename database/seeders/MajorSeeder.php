<?php

namespace Database\Seeders;

use App\Modules\Generals\Models\Major;
use Illuminate\Database\Seeder;

class MajorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Major::create([
            'name' => 'IPA'
        ]);

        Major::create([
            'name' => 'IPS'
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            FacilitySeeder::class,
            GradeSeeder::class,
            MajorSeeder::class,
            CountrySeeder::class,
            StudentCategorySeeder::class,
            StudentTypeSeeder::class,
            SubjectSeeder::class,
            SchoolPeriodSeeder::class,
            SchoolFeeSeeder::class,
            PaymentGatewaySeeder::class,
            PaymentMethodSeeder::class,
            DormitorySeeder::class,
            DormitorySubjectSeeder::class
        ]);
    }
}

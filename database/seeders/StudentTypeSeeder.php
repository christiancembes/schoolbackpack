<?php

namespace Database\Seeders;

use App\Modules\Generals\Models\StudentType;
use Illuminate\Database\Seeder;

class StudentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StudentType::create([
            'name' => 'Fullday'
        ]);

        StudentType::create([
            'name' => 'Boarding'
        ]);
    }
}

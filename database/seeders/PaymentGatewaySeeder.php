<?php

namespace Database\Seeders;

use App\Modules\Location\Models\Country;
use App\Modules\Payment\Models\PaymentGateway;
use App\Modules\Payment\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentGatewaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentGateway::create([
            'name' => 'Manual',
            'slug' => 'manual'
        ]);
    }
}

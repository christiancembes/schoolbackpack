<?php

namespace Database\Seeders;

use App\Modules\Generals\Models\StudentCategory;
use Illuminate\Database\Seeder;

class StudentCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StudentCategory::create([
            'name' => 'Baru'
        ]);

        StudentCategory::create([
            'name' => 'Lama'
        ]);

        StudentCategory::create([
            'name' => 'Pindahan'
        ]);
    }
}

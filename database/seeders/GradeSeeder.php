<?php

namespace Database\Seeders;

use App\Modules\Generals\Models\Grade;
use App\Modules\Generals\Models\GradeClass;
use Illuminate\Database\Seeder;

class GradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Grade::create([
            'name' => 'SMP'
        ]);

        GradeClass::create([
            'name' => '7',
            'grade_id' => 1
        ]);

        GradeClass::create([
            'name' => '8',
            'grade_id' => 1
        ]);

        GradeClass::create([
            'name' => '9',
            'grade_id' => 1
        ]);

        Grade::create([
            'name' => 'SMA'
        ]);

        GradeClass::create([
            'name' => '10',
            'grade_id' => 2
        ]);

        GradeClass::create([
            'name' => '11',
            'grade_id' => 2
        ]);

        GradeClass::create([
            'name' => '12',
            'grade_id' => 2
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Modules\Location\Models\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::create([
            'name' => 'Indonesia'
        ]);
    }
}

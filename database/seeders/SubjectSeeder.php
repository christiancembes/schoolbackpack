<?php

namespace Database\Seeders;

use App\Modules\Generals\Models\Grade;
use App\Modules\Generals\Models\Subject;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
            'name' => 'PAI',
            'group' => 'General'
        ]);

        Subject::create([
            'name' => 'Bahasa Indonesia',
            'group' => 'General'
        ]);

        Subject::create([
            'name' => 'Matematika',
            'group' => 'General'
        ]);

        Subject::create([
            'name' => 'IPA',
            'group' => 'General'
        ]);

        Subject::create([
            'name' => 'IPS',
            'group' => 'General'
        ]);

        Subject::create([
            'name' => 'Prestasi Lain',
            'group' => 'General'
        ]);
    }
}

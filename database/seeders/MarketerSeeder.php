<?php

namespace Database\Seeders;

use App\Modules\Generals\Models\Marketer;
use App\Modules\User\User;
use Illuminate\Database\Seeder;

class MarketerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Rangga',
            'email' => 'rangga@mailinator.com',
            'password' => bcrypt('password')
        ]);

        $user->assignRole('marketing');

        Marketer::create([
            'name' => 'Mr. Rangga',
            'user_id' => $user->getId()
        ]);

        $user = User::create([
            'name' => 'Cinta',
            'email' => 'cinta@mailinator.com',
            'password' => bcrypt('password')
        ]);

        $user->assignRole('marketing');

        Marketer::create([
            'name' => 'Ms. Cinta',
            'user_id' => $user->getId()
        ]);
    }
}

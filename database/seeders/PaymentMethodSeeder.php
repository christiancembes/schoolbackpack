<?php

namespace Database\Seeders;

use App\Modules\Location\Models\Country;
use App\Modules\Payment\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMethod::create([
            'name' => 'Bank Transfer',
            'slug' => 'bank-transfer',
            'bank' => 'BSM',
            'account_no' => '1234567890',
            'payment_gateway_id' => 1
        ]);
    }
}

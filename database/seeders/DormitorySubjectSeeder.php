<?php

namespace Database\Seeders;

use App\Modules\Generals\Models\DormitorySubject;
use Illuminate\Database\Seeder;

class DormitorySubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DormitorySubject::create([
            'name' => 'Leadership',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Self Discipline',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Commitment',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Self Confidence',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Responsibility',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Involvement',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Interaction',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Cooperation',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Respect',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Obedience',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Punctuality',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Cleanliness',
            'group' => 'Behaviour'
        ]);

        DormitorySubject::create([
            'name' => 'Shalat',
            'group' => 'Dormitory Activity'
        ]);

        DormitorySubject::create([
            'name' => 'Fardu Prayer',
            'group' => 'Dormitory Activity'
        ]);

        DormitorySubject::create([
            'name' => 'Sunnah Prayer',
            'group' => 'Dormitory Activity'
        ]);

        DormitorySubject::create([
            'name' => 'Quran Recitation',
            'group' => 'Dormitory Activity'
        ]);

        DormitorySubject::create([
            'name' => 'Fasting',
            'group' => 'Dormitory Activity'
        ]);

        DormitorySubject::create([
            'name' => 'Fasting Ramadhan',
            'group' => 'Dormitory Activity'
        ]);

        DormitorySubject::create([
            'name' => 'Fasting Sunnah',
            'group' => 'Dormitory Activity'
        ]);

        DormitorySubject::create([
            'name' => 'Saturday and Sunday Activity',
            'group' => 'Dormitory Activity'
        ]);

        DormitorySubject::create([
            'name' => 'Al-Quran and Tajwid',
            'group' => 'Dormitory Curriculum'
        ]);

        DormitorySubject::create([
            'name' => 'English Language',
            'group' => 'Dormitory Curriculum'
        ]);

        DormitorySubject::create([
            'name' => 'Arabic Language',
            'group' => 'Dormitory Curriculum'
        ]);

        DormitorySubject::create([
            'name' => 'Islamic Studies',
            'group' => 'Dormitory Curriculum'
        ]);

        DormitorySubject::create([
            'name' => 'Memorizing Quran',
            'group' => 'Special Achievement'
        ]);

        DormitorySubject::create([
            'name' => 'Language',
            'group' => 'Special Achievement'
        ]);

        DormitorySubject::create([
            'name' => 'Public Speaking',
            'group' => 'Special Achievement'
        ]);
        DormitorySubject::create([
            'name' => 'Other Skill',
            'group' => 'Special Achievement'
        ]);
    }
}

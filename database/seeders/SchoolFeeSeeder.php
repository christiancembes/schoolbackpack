<?php

namespace Database\Seeders;

use App\Modules\Generals\Models\Grade;
use App\Modules\Generals\Models\SchoolFee;
use App\Modules\Generals\Models\StudentType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SchoolFeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $combinations = [
            [
                'school_period_id' => 1,
                'type' => Str::slug('Registration Fee'),
                'name' => 'Registration Fee',
                'amount' => 500000,
                'description' => 'Tes',
                'payment_system' => 'Non Refund',
                'payment_period' => 'none',
            ],
            [
                'school_period_id' => 1,
                'type' => Str::slug('Development Fee'),
                'name' => 'Development Fee',
                'amount' => 25000000,
                'description' => 'Biaya fasilitas sekolah',
                'payment_system' => 'Non Refund',
                'payment_period' => 'none',
            ],
            [
                'school_period_id' => 1,
                'type' => Str::slug('Text Book'),
                'name' => 'Text Book',
                'amount' => 1500000,
                'description' => 'Biaya Buku',
                'payment_system' => 'Setiap tahun ajaran baru',
                'payment_period' => 'every_year',
            ],
            [
                'school_period_id' => 1,
                'type' => Str::slug('Uniform'),
                'name' => 'Uniform',
                'amount' => 2000000,
                'description' => 'Seragam',
                'payment_system' => '',
                'payment_period' => 'none',
            ],
            [
                'school_period_id' => 1,
                'type' => Str::slug('Extra Curricular'),
                'name' => 'Extra Curricular',
                'amount' => 2500000,
                'description' => 'Extra Curricular',
                'payment_system' => 'Setiap tahun ajaran baru',
                'payment_period' => 'every_year',
            ],
            [
                'school_period_id' => 1,
                'type' => Str::slug('SPP'),
                'name' => 'SPP',
                'amount' => 2000000,
                'description' => 'SPP',
                'payment_system' => 'Setiap bulan',
                'payment_period' => 'every_month',
            ],
            [
                'school_period_id' => 1,
                'type' => Str::slug('Graduation Fee'),
                'name' => 'Graduation Fee',
                'amount' => 2000000,
                'description' => 'Graduation Fee',
                'payment_system' => 'Semester 6',
                'payment_period' => 'every_six_month',
            ],
            [
                'school_period_id' => 1,
                'type' => Str::slug('Overseas Program'),
                'name' => 'Overseas Program',
                'amount' => 0,
                'description' => 'Overseas Program',
                'payment_system' => '',
                'payment_period' => 'none',
            ]
        ];

        $studentTypes = StudentType::all();
        $grades = Grade::all();

        foreach ($studentTypes as $studentType) {
            foreach ($grades as $grade) {
                foreach ($combinations as $combination) {
                    $combination['student_type_id'] = $studentType->id;
                    $combination['grade_id'] = $grade->id;
                    $combination['type'] = Str::slug($combination['name'] . ' - ' . $studentType->name);
                    $combination['name'] = $combination['name'] . ' - ' . $studentType->name;
                    $combination['description'] = $combination['description'] . ' - ' . $studentType->name;

                    SchoolFee::create($combination);
                }
            }
        }

    }
}

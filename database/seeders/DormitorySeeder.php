<?php

namespace Database\Seeders;

use App\Modules\Dormitory\Models\Dormitory;
use Illuminate\Database\Seeder;

class DormitorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Dormitory::create([
            'name' => 'A101',
            'capacity' => 4,
            'remaining_capacity' => 4,
            'building' => 'A',
            'floor' => '1',
            'type' => 'L',
        ]);

        Dormitory::create([
            'name' => 'A102',
            'capacity' => 4,
            'remaining_capacity' => 4,
            'building' => 'A',
            'floor' => '1',
            'type' => 'P',
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Modules\Generals\Models\SchoolPeriod;
use App\Modules\Student\Models\StudentQuota;
use Illuminate\Database\Seeder;

class SchoolPeriodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schoolPeriod = SchoolPeriod::create([
            'name' => '2021/2022',
            'active' => 1
        ]);

        StudentQuota::create([
            'capacity' => 100,
            'remaining_capacity' => 100,
            'school_period_id' => $schoolPeriod->getId()
        ]);

        $schoolPeriod = SchoolPeriod::create([
            'name' => '2022/2023'
        ]);

        StudentQuota::create([
            'capacity' => 100,
            'remaining_capacity' => 100,
            'school_period_id' => $schoolPeriod->getId()
        ]);

        $schoolPeriod = SchoolPeriod::create([
            'name' => '2023/2024'
        ]);

        StudentQuota::create([
            'capacity' => 100,
            'remaining_capacity' => 100,
            'school_period_id' => $schoolPeriod->getId()
        ]);

        $schoolPeriod = SchoolPeriod::create([
            'name' => '2024/2025'
        ]);

        StudentQuota::create([
            'capacity' => 100,
            'remaining_capacity' => 100,
            'school_period_id' => $schoolPeriod->getId()
        ]);

        $schoolPeriod = SchoolPeriod::create([
            'name' => '2025/2026'
        ]);

        StudentQuota::create([
            'capacity' => 100,
            'remaining_capacity' => 100,
            'school_period_id' => $schoolPeriod->getId()
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Modules\User\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create(['name' => 'dashboard']);
        Permission::create(['name' => 'student']);
        Permission::create(['name' => 'academic']);
        Permission::create(['name' => 'payment']);
        Permission::create(['name' => 'quotation']);
        Permission::create(['name' => 'dormitory']);
        Permission::create(['name' => 'marketer']);
        Permission::create(['name' => 'master-data']);
        Permission::create(['name' => 'administration']);
        Permission::create(['name' => 'permission']);

        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(['dashboard', 'student', 'academic', 'payment', 'quotation', 'dormitory', 'marketer', 'master-data', 'administration', 'permission']);

        $role = Role::create(['name' => 'finance']);
        $role->givePermissionTo(['dashboard', 'student', 'payment', 'quotation']);

        $role = Role::create(['name' => 'dormitory']);
        $role->givePermissionTo(['dashboard', 'student', 'academic', 'dormitory']);

        $role = Role::create(['name' => 'marketing']);
        $role->givePermissionTo(['dashboard', 'marketer']);

        Role::create(['name' => 'user']);

        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@mailinator.com',
            'password' => bcrypt('password')
        ]);

        $user->assignRole('super-admin');

        $user = User::create([
            'name' => 'finance',
            'email' => 'finance@mailinator.com',
            'password' => bcrypt('password')
        ]);

        $user->assignRole('finance');

        $user = User::create([
            'name' => 'dormitory',
            'email' => 'dormitory@mailinator.com',
            'password' => bcrypt('password')
        ]);

        $user->assignRole('dormitory');

        $this->call([
            MarketerSeeder::class
        ]);

        $user = User::create([
            'name' => 'john',
            'email' => 'john@mailinator.com',
            'password' => bcrypt('123123')
        ]);

        $user->assignRole('user');

        $user = User::create([
            'name' => 'jane',
            'email' => 'jane@mailinator.com',
            'password' => bcrypt('123123')
        ]);

        $user->assignRole('user');
    }
}

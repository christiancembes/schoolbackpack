<?php

namespace Database\Seeders;

use App\Modules\Generals\Models\Facility;
use Illuminate\Database\Seeder;

class FacilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Facility::create([
            'name' => 'Beasiswa'
        ]);

        Facility::create([
            'name' => 'NBS'
        ]);
    }
}

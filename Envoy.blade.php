@servers(['web' => 'root@149.129.255.29'])

@setup
    $repository = 'git@bitbucket.org:arthurnumen/alexandria-school.git';
    $releases_dir = '/var/www/app/releases';
    $app_dir = '/var/www/app';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
    run_migration
    backpack_install
    down
    up
    clean_old_releases
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --recursive --depth 1 --branch {{ $branch }} {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env

    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
@endtask

@task('run_migration')
    echo "Run migration ({{ $release }})"
    cd {{ $new_release_dir }}
    php artisan migrate
@endtask

@task('backpack_install')
    echo "Install Backpack ({{ $release }})"
    cd {{ $new_release_dir }}
    php artisan backpack:install
@endtask

@task('down')
    cd {{ $new_release_dir }};
    php artisan down;
@endtask

@task('up')
    cd {{ $new_release_dir }};
    php artisan up;
@endtask

@task('clean_old_releases')
    # This will list our releases by modification time and delete all but the 3 most recent.
    purging=$(ls -dt {{ $releases_dir }}/* | tail -n +3);

    if [ "$purging" != "" ]; then
        echo Purging old releases: $purging;
        rm -rf $purging;
    else
        echo "No releases found for purging at this time";
    fi
@endtask

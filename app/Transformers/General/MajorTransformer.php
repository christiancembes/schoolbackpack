<?php

namespace App\Transformers\General;

use App\Modules\Generals\Models\Major;
use League\Fractal\TransformerAbstract;

class MajorTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Major $major)
    {
        return [
            'id' => $major->getId(),
            'name' => $major->getName(),
            'label' => $major->getName(),
        ];
    }
}

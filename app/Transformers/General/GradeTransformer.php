<?php

namespace App\Transformers\General;

use App\Modules\Generals\Models\Grade;
use League\Fractal\TransformerAbstract;

class GradeTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Grade $grade)
    {
        return [
            'id' => $grade->getId(),
            'name' => $grade->getName(),
            'label' => $grade->getName()
        ];
    }
}

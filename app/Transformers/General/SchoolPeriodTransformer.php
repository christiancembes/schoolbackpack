<?php

namespace App\Transformers\General;

use App\Modules\Generals\Models\SchoolPeriod;
use League\Fractal\TransformerAbstract;

class SchoolPeriodTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SchoolPeriod $schoolPeriod)
    {
        return [
            'id' => $schoolPeriod->getId(),
            'name' => $schoolPeriod->getName(),
            'label' => $schoolPeriod->getName(),
        ];
    }
}

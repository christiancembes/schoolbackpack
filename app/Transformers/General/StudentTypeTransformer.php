<?php

namespace App\Transformers\General;

use App\Modules\Generals\Models\StudentType;
use League\Fractal\TransformerAbstract;

class StudentTypeTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(StudentType $studentType)
    {
        return [
            'id' => $studentType->getId(),
            'name' => $studentType->getName(),
            'label' => $studentType->getName(),
        ];
    }
}

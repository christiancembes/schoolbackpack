<?php

namespace App\Transformers\General;

use App\Modules\Generals\Models\Grade;
use App\Modules\Generals\Models\GradeClass;
use League\Fractal\TransformerAbstract;

class GradeClassTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(GradeClass $gradeClass)
    {
        return [
            'id' => $gradeClass->getId(),
            'name' => $gradeClass->getName(),
            'label' => $gradeClass->getName()
        ];
    }
}

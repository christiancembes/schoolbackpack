<?php

namespace App\Transformers\General;

use App\Modules\Generals\Models\StudentCategory;
use League\Fractal\TransformerAbstract;

class StudentCategoryTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(StudentCategory $studentCategory)
    {
        return [
            'id' => $studentCategory->getId(),
            'name' => $studentCategory->getName(),
            'label' => $studentCategory->getName(),
        ];
    }
}

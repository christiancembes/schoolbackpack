<?php

namespace App\Transformers\General;

use App\Modules\Generals\Models\DormitorySubject;
use App\Modules\Generals\Models\Grade;
use App\Modules\Generals\Models\Subject;
use League\Fractal\TransformerAbstract;

class DormitorySubjectTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(DormitorySubject $dormitorySubject)
    {
        return [
            'id' => $dormitorySubject->getId(),
            'name' => $dormitorySubject->getName(),
            'label' => $dormitorySubject->getName(),
            'group' => $dormitorySubject->getGroup(),
        ];
    }
}

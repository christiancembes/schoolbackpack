<?php

namespace App\Transformers\General;

use App\Modules\Generals\Models\SchoolFee;
use App\Modules\Generals\Models\SchoolPeriod;
use League\Fractal\TransformerAbstract;

class SchoolFeeTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SchoolFee $schoolFee)
    {
        return [
            'id' => $schoolFee->getId(),
            'name' => $schoolFee->getName(),
            'label' => $schoolFee->getName(),
            'type' => $schoolFee->getType(),
            'amount' => $schoolFee->getAmount(),
            'description' => $schoolFee->getDescription(),
            'payment_system' => $schoolFee->getPaymentSystem(),
        ];
    }
}

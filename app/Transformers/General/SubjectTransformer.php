<?php

namespace App\Transformers\General;

use App\Modules\Generals\Models\Grade;
use App\Modules\Generals\Models\Subject;
use League\Fractal\TransformerAbstract;

class SubjectTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Subject $subject)
    {
        return [
            'id' => $subject->getId(),
            'name' => $subject->getName(),
            'label' => $subject->getName(),
            'group' => $subject->getGroup(),
        ];
    }
}

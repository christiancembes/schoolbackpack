<?php

namespace App\Transformers\General;

use App\Modules\Generals\Models\Facility;
use League\Fractal\TransformerAbstract;

class FacilityTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param  Facility  $facility
     *
     * @return array
     */
    public function transform(Facility $facility)
    {
        return [
            'id' => $facility->getId(),
            'name' => $facility->getName(),
            'label' => $facility->getName(),
        ];
    }
}

<?php

namespace App\Transformers\General;

use App\Modules\Generals\Models\Marketer;
use League\Fractal\TransformerAbstract;

class MarketerTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Marketer $marketer)
    {
        return [
            'id' => $marketer->getId(),
            'name' => $marketer->getName(),
            'label' => $marketer->getName(),
        ];
    }
}

<?php

namespace App\Transformers\Student;

use App\Modules\Student\Models\StudentPreviousEducation;
use League\Fractal\TransformerAbstract;

class StudentPreviousEducationTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(StudentPreviousEducation $studentPreviousEducation)
    {
        return [
            'school_origin' => $studentPreviousEducation->getSchoolOrigin(),
            'school_address' => $studentPreviousEducation->getSchoolAddress(),
            'phone' => $studentPreviousEducation->getPhone(),
            'head_master_name' => $studentPreviousEducation->getHeadMasterName(),
            'nisn' => $studentPreviousEducation->getNisn(),
            'school_status' => $studentPreviousEducation->getSchoolStatus(),
            'education_system' => $studentPreviousEducation->getEducationSystem(),
            'period' => $studentPreviousEducation->getPeriod(),
        ];
    }
}

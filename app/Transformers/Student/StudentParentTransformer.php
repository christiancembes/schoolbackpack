<?php

namespace App\Transformers\Student;

use App\Modules\Student\Models\StudentParent;
use App\Transformers\Location\CountryTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class StudentParentTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'country',
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(StudentParent $studentParent)
    {
        return [
            'type' => $studentParent->getType(),
            'name' => $studentParent->getName(),
            'place_of_birth' => $studentParent->getPlaceOfBirth(),
            'date_of_birth' => $studentParent->getDateOfBirth(),
            'profession' => $studentParent->getProfession(),
            'last_education' => $studentParent->getLastEducation(),
            'religion' => $studentParent->getReligion(),
            'mobile' => $studentParent->getMobile(),
            'email' => $studentParent->getEmail(),
            'company' => $studentParent->getCompany(),
            'office_address' => $studentParent->getOfficeAddress(),
            'income' => $studentParent->getIncome(),
        ];
    }

    public function includeCountry(StudentParent $studentParent)
    {
        return new Item($studentParent->getCountry(), new CountryTransformer());
    }
}

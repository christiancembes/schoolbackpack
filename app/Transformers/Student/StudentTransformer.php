<?php

namespace App\Transformers\Student;

use App\Modules\Student\Models\Student;
use App\Transformers\General\FacilityTransformer;
use App\Transformers\General\GradeTransformer;
use App\Transformers\General\MajorTransformer;
use App\Transformers\General\MarketerTransformer;
use App\Transformers\General\StudentCategoryTransformer;
use App\Transformers\General\StudentTypeTransformer;
use App\Transformers\Location\CountryTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class StudentTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'country',
        'studentCategory',
        'studentType',
        'grade',
        'gradeOfSibling',
        'facility',
        'major',
        'marketer',
        'address',
        'parents',
        'health',
        'previousEducation',
        'previousReports'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Student $student)
    {
        return [
            'nis' => $student->getNis() === null ? '' : $student->getNis(),
            'nisn' => $student->getNisn(),
            'full_name' => $student->getFullName(),
            'name' => $student->getName(),
            'gender' => $student->getGender(),
            'place_of_birth' => $student->getPlaceOfBirth(),
            'date_of_birth' => $student->getDateOfBirth(),
            'goal' => $student->getGoal(),
            'hobby' => $student->getHobby(),
            'nik' => $student->getNik(),
            'order_in_family' => $student->getOrderInFamily(),
            'numbers_of_siblings' => $student->getNumbersOfSiblings(),
            'numbers_of_step_families' => $student->getNumbersOfStepFamilies(),
            'numbers_of_adopted_families' => $student->getNumbersOfAdoptedFamilies(),
            'weight' => $student->getWeight(),
            'height' => $student->getHeight(),
            'blood_type' => $student->getBloodType(),
            'mother_language' => $student->getMotherLanguage(),
            'name_of_sibling' => $student->getNameOfSibling() === null ? '' : $student->getNameOfSibling(),
            'status' => $student->getStatus(),
        ];
    }

    public function includeCountry(Student $student)
    {
        return new Item($student->getCountry(), new CountryTransformer());
    }

    public function includeStudentCategory(Student $student)
    {
        return new Item($student->getStudentCategory(), new StudentCategoryTransformer());
    }

    public function includeStudentType(Student $student)
    {
        return new Item($student->getStudentType(), new StudentTypeTransformer());
    }

    public function includeGrade(Student $student)
    {
        return new Item($student->getGrade(), new GradeTransformer());
    }

    public function includeGradeOfSibling(Student $student)
    {
        if ($student->getGradeOfSibling() !== null) {
            return new Item($student->getGradeOfSibling(), new GradeTransformer());
        }
    }

    public function includeFacility(Student $student)
    {
        if ($student->getFacility() !== null) {
            return new Item($student->getFacility(), new FacilityTransformer());
        }
    }

    public function includeMajor(Student $student)
    {
        if ($student->getMajor() !== null) {
            return new Item($student->getMajor(), new MajorTransformer());
        }
    }

    public function includeMarketer(Student $student)
    {
        if ($student->getMarketer() !== null) {
            return new Item($student->getMarketer(), new MarketerTransformer());
        }
    }

    public function includeAddress(Student $student)
    {
        return new Item($student->getAddress(), new StudentAddressTransformer());
    }

    public function includeParents(Student $student)
    {
        return new Collection($student->getParents(), new StudentParentTransformer());
    }

    public function includeHealth(Student $student)
    {
        return new Item($student->getHealth(), new StudentHealthTransformer());
    }

    public function includePreviousEducation(Student $student)
    {
        return new Item($student->getPreviousEducation(), new StudentPreviousEducationTransformer());
    }

    public function includePreviousReports(Student $student)
    {
        return new Collection($student->getPreviousReports(), new StudentPreviousReportTransformer());
    }
}

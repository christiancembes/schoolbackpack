<?php

namespace App\Transformers\Student;

use App\Modules\Student\Models\StudentHealth;
use App\Modules\Student\Models\StudentReport;
use App\Transformers\General\SchoolPeriodTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class StudentReportTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'student',
        'schoolPeriod',
        'studentReportScores'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param  StudentReport  $studentReport
     *
     * @return array
     */
    public function transform(StudentReport $studentReport)
    {
        return [
            'id' => $studentReport->getId(),
            'terms' => $studentReport->getTerms(),
            'parent_note' => $studentReport->getParentNote(),
            'guardian_note' => $studentReport->getGuardianNote(),
            'absence' => json_decode($studentReport->getAbsence(), true),
            'achievement' => json_decode($studentReport->getAchievement(), true),
            'extracurricular' => json_decode($studentReport->getExtracurricular(), true),
        ];
    }

    public function includeStudent(StudentReport $studentReport)
    {
        return new Item($studentReport->getStudent(), new StudentTransformer());
    }

    public function includeSchoolPeriod(StudentReport $studentReport)
    {
        return new Item($studentReport->getSchoolPeriod(), new SchoolPeriodTransformer());
    }

    public function includeStudentReportScores(StudentReport $studentReport)
    {
        return new Collection($studentReport->getStudentReportScores(), new StudentReportScoreTransformer());
    }
}

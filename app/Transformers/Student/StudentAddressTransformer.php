<?php

namespace App\Transformers\Student;

use App\Modules\Student\Models\StudentAddress;
use App\Transformers\Location\CityTransformer;
use App\Transformers\Location\CountryTransformer;
use App\Transformers\Location\DistrictTransformer;
use App\Transformers\Location\ProvinceTransformer;
use App\Transformers\Location\VillageTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class StudentAddressTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'province',
        'city',
        'district',
        'village'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param  StudentAddress  $studentAddress
     *
     * @return array
     */
    public function transform(StudentAddress $studentAddress)
    {
        return [
            'address' => $studentAddress->getAddress(),
            'postal_code' => $studentAddress->getPostalCode(),
            'phone' => $studentAddress->getPhone(),
            'live_with' => $studentAddress->getLiveWith(),
        ];
    }

    public function includeProvince(StudentAddress $studentAddress)
    {
        return new Item($studentAddress->getProvince(), new ProvinceTransformer());
    }

    public function includeCity(StudentAddress $studentAddress)
    {
        return new Item($studentAddress->getCity(), new CityTransformer());
    }

    public function includeDistrict(StudentAddress $studentAddress)
    {
        return new Item($studentAddress->getDistrict(), new DistrictTransformer());
    }

    public function includeVillage(StudentAddress $studentAddress)
    {
        return new Item($studentAddress->getVillage(), new VillageTransformer());
    }

}

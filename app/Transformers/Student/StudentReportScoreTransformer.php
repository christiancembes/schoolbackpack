<?php

namespace App\Transformers\Student;

use App\Modules\Student\Models\StudentHealth;
use App\Modules\Student\Models\StudentReportScore;
use App\Transformers\General\SubjectTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class StudentReportScoreTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'subject'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(StudentReportScore $studentReportScore)
    {
        return [
            'score' => $studentReportScore->getScore(),
            'notes' => $studentReportScore->getNotes()
        ];
    }

    public function includeSubject(StudentReportScore $studentReportScore)
    {
        return new Item($studentReportScore->getSubject(), new SubjectTransformer());
    }
}

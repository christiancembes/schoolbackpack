<?php

namespace App\Transformers\Student;

use App\Modules\Student\Models\StudentPreviousReport;
use App\Transformers\General\GradeTransformer;
use App\Transformers\General\SubjectTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class StudentPreviousReportTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'grade',
        'subject'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(StudentPreviousReport $studentPreviousReport)
    {
        return [
            'first_period_score' => $studentPreviousReport->getFirstPeriodScore(),
            'second_period_score' => $studentPreviousReport->getSecondPeriodScore(),
            'notes' => $studentPreviousReport->getNotes(),
        ];
    }

    public function includeGrade(StudentPreviousReport $studentPreviousReport)
    {
        return new Item($studentPreviousReport->getGrade(), new GradeTransformer());
    }

    public function includeSubject(StudentPreviousReport $studentPreviousReport)
    {
        return new Item($studentPreviousReport->getSubject(), new SubjectTransformer());
    }
}

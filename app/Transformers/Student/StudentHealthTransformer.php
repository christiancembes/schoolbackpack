<?php

namespace App\Transformers\Student;

use App\Modules\Student\Models\StudentHealth;
use League\Fractal\TransformerAbstract;

class StudentHealthTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(StudentHealth $studentHealth)
    {
        return [
            'tbc' => $studentHealth->getTbc(),
            'malaria' => $studentHealth->getMalaria(),
            'chotipa' => $studentHealth->getChotipa(),
            'cacar' => $studentHealth->getCacar(),
            'others' => $studentHealth->getOthers(),
            'physical_abnormalities' => $studentHealth->getPhysicalAbnormalities(),
        ];
    }
}

<?php

namespace App\Transformers\Payment;

use App\Modules\Payment\Models\PaymentTransaction;
use App\Transformers\Student\StudentTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class PaymentTransactionTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'student',
        'paymentMethod'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param  PaymentTransaction  $paymentTransaction
     *
     * @return array
     */
    public function transform(PaymentTransaction $paymentTransaction)
    {
        return [
            'code' => $paymentTransaction->getCode(),
            'description' => $paymentTransaction->getDescription(),
            'total_amount' => $paymentTransaction->getTotalAmount(),
            'total_amount_money' => $paymentTransaction->getTotalAmountMoney(),
            'status' => $paymentTransaction->getStatus(),
            'status_str' => $paymentTransaction->getStatusStr(),
            'completed_at' => $paymentTransaction->getCompletedAt(),
            'expired_at' => $paymentTransaction->getExpiredAt()
        ];
    }

    public function includePaymentMethod(PaymentTransaction $paymentTransaction)
    {
        return new Item($paymentTransaction->getPaymentMethod(), new PaymentMethodTransformer());
    }

    public function includeStudent(PaymentTransaction $paymentTransaction)
    {
        return new Item($paymentTransaction->getStudent(), new StudentTransformer());
    }
}

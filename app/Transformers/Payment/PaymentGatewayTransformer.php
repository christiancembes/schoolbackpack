<?php

namespace App\Transformers\Payment;

use App\Modules\Payment\Models\PaymentGateway;
use League\Fractal\TransformerAbstract;

class PaymentGatewayTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param  PaymentGateway  $paymentGateway
     *
     * @return array
     */
    public function transform(PaymentGateway $paymentGateway)
    {
        return [
            'name' => $paymentGateway->getName(),
            'slug' => $paymentGateway->getSlug()
        ];
    }
}

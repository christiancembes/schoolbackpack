<?php

namespace App\Transformers\Payment;

use App\Modules\Payment\Models\PaymentMethod;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class PaymentMethodTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'paymentGateway'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param  PaymentMethod  $paymentMethod
     *
     * @return array
     */
    public function transform(PaymentMethod $paymentMethod)
    {
        return [
            'name' => $paymentMethod->getName(),
            'slug' => $paymentMethod->getSlug(),
            'bank' => $paymentMethod->getBank(),
            'account_no' => $paymentMethod->getAccountNo()
        ];
    }

    public function includePaymentGateway(PaymentMethod $paymentMethod)
    {
        return new Item($paymentMethod->getPaymentGateway(), new PaymentGatewayTransformer());
    }
}

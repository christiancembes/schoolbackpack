<?php

namespace App\Transformers\Location;

use App\Modules\Location\Models\Village;
use League\Fractal\TransformerAbstract;

class VillageTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Village $village)
    {
        return [
            'id' => $village->getId(),
            'name' => $village->getName(),
            'label' => $village->getName(),
        ];
    }
}

<?php

namespace App\Transformers\Location;

use App\Modules\Location\Models\Province;
use League\Fractal\TransformerAbstract;

class ProvinceTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Province $province)
    {
        return [
            'id' => $province->getId(),
            'name' => $province->getName(),
            'label' => $province->getName(),
        ];
    }
}

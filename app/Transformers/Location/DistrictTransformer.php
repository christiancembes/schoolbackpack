<?php

namespace App\Transformers\Location;

use App\Modules\Location\Models\District;
use League\Fractal\TransformerAbstract;

class DistrictTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(District $district)
    {
        return [
            'id' => $district->getId(),
            'name' => $district->getName(),
            'label' => $district->getName(),
        ];
    }
}

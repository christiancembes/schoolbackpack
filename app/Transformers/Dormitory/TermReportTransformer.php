<?php

namespace App\Transformers\Dormitory;

use App\Modules\Dormitory\Models\Report\TermReport;
use App\Transformers\General\SchoolPeriodTransformer;
use App\Transformers\Student\StudentTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class TermReportTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'student',
        'schoolPeriod',
        'termReportScore'
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TermReport $termReport)
    {
        return [
            'id' => $termReport->getId(),
            'term' => $termReport->getTerm(),
            'total_score' => $termReport->getTotalScore(),
            'value' => $termReport->getValue(),
            'grade' => $termReport->getGrade(),
            'academic' => json_decode($termReport->getAcademic(), true),
            'behavior' => json_decode($termReport->getBehavior(), true),
            'life_skill' => json_decode($termReport->getLifeSkill(), true),
        ];
    }

    public function includeTermReportScore(TermReport $termReport)
    {
        return new Collection($termReport->getTermReportScore(), new TermReportScoreTransformer());
    }

    public function includeStudent(TermReport $termReport)
    {
        return new Item($termReport->getStudent(), new StudentTransformer());
    }

    public function includeSchoolPeriod(TermReport $termReport)
    {
        return new Item($termReport->getSchoolPeriod(), new SchoolPeriodTransformer());
    }
}

<?php

namespace App\Transformers\Dormitory;

use App\Modules\Dormitory\Models\Report\MonthlyReport;
use App\Modules\Dormitory\Models\Report\MonthlyReportScore;
use App\Modules\Dormitory\Models\Report\TermReportScore;
use App\Transformers\General\DormitorySubjectTransformer;
use App\Transformers\General\SchoolPeriodTransformer;
use App\Transformers\Student\StudentTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class TermReportScoreTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'dormitorySubject'
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TermReportScore $termReportScore)
    {
        return [
            'id' => $termReportScore->getId(),
            'score' => $termReportScore->getScore(),
            'grade' => $termReportScore->getGrade()
        ];
    }

    public function includeDormitorySubject(TermReportScore $termReportScore)
    {
        return new Item($termReportScore->getDormitorySubject(), new DormitorySubjectTransformer());
    }
}

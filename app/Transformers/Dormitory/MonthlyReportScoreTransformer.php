<?php

namespace App\Transformers\Dormitory;

use App\Modules\Dormitory\Models\Report\MonthlyReport;
use App\Modules\Dormitory\Models\Report\MonthlyReportScore;
use App\Transformers\General\DormitorySubjectTransformer;
use App\Transformers\General\SchoolPeriodTransformer;
use App\Transformers\Student\StudentTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class MonthlyReportScoreTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'dormitorySubject'
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(MonthlyReportScore $monthlyReportScore)
    {
        return [
            'id' => $monthlyReportScore->getId(),
            'score' => $monthlyReportScore->getScore(),
            'grade' => $monthlyReportScore->getGrade()
        ];
    }

    public function includeDormitorySubject(MonthlyReportScore $monthlyReportScore)
    {
        return new Item($monthlyReportScore->getDormitorySubject(), new DormitorySubjectTransformer());
    }
}

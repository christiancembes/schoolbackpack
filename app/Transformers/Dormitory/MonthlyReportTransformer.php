<?php

namespace App\Transformers\Dormitory;

use App\Modules\Dormitory\Models\Report\MonthlyReport;
use App\Transformers\General\SchoolPeriodTransformer;
use App\Transformers\Student\StudentTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class MonthlyReportTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'student',
        'schoolPeriod',
        'monthlyReportScore'
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param  MonthlyReport  $monthlyReport
     *
     * @return array
     */
    public function transform(MonthlyReport $monthlyReport)
    {
        return [
            'id' => $monthlyReport->getId(),
            'name' => $monthlyReport->getMonthStr(),
            'academic' => json_decode($monthlyReport->getAcademic(), true),
            'behavior' => json_decode($monthlyReport->getBehavior(), true),
            'life_skill' => json_decode($monthlyReport->getLifeSkill(), true),
        ];
    }

    public function includeMonthlyReportScore(MonthlyReport $monthlyReport)
    {
        return new Collection($monthlyReport->getMonthlyReportScore(), new MonthlyReportScoreTransformer());
    }

    public function includeStudent(MonthlyReport $monthlyReport)
    {
        return new Item($monthlyReport->getStudent(), new StudentTransformer());
    }

    public function includeSchoolPeriod(MonthlyReport $monthlyReport)
    {
        return new Item($monthlyReport->getSchoolPeriod(), new SchoolPeriodTransformer());
    }
}

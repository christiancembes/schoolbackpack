<?php

namespace App\Transformers\Quotation;

use App\Modules\Payment\Models\PaymentTransaction;
use App\Modules\Quotation\Models\Quotation;
use App\Transformers\Student\StudentTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class QuotationTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'quotationDetails',
        'quotationSchedules'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param  Quotation  $quotation
     *
     * @return array
     */
    public function transform(Quotation $quotation)
    {
        return [
            'code' => $quotation->getCode(),
            'description' => $quotation->getDescription(),
            'total_amount' => $quotation->getTotalAmount(),
            'total_amount_money' => $quotation->getTotalAmountMoney(),
            'total_discount' => $quotation->getTotalDiscount(),
            'total_discount_money' => $quotation->getTotalDiscountMoney(),
            'remaining_amount' => $quotation->getRemainingAmount(),
            'remaining_amount_money' => $quotation->getRemainingAmountMoney(),
            'final_amount' => $quotation->getFinalAmount(),
            'final_amount_money' => $quotation->getFinalAmountMoney(),
            'completed_at' => $quotation->getCompletedAt()
        ];
    }

    public function includeQuotationDetails(Quotation $quotation)
    {
        return new Collection($quotation->getQuotationDetails(), new QuotationDetailTransformer());
    }

    public function includeQuotationSchedules(Quotation $quotation)
    {
        return new Collection($quotation->getQuotationSchedules(), new QuotationScheduleTransformer());
    }


}

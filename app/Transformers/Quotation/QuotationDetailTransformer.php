<?php

namespace App\Transformers\Quotation;

use App\Modules\Payment\Models\PaymentTransaction;
use App\Modules\Quotation\Models\Quotation;
use App\Modules\Quotation\Models\QuotationDetail;
use App\Transformers\General\SchoolFeeTransformer;
use App\Transformers\Student\StudentTransformer;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class QuotationDetailTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'schoolFee'
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param  QuotationDetail  $quotationDetail
     *
     * @return array
     */
    public function transform(QuotationDetail $quotationDetail)
    {
        return [
            'description' => $quotationDetail->getDescription(),
            'amount' => $quotationDetail->getAmount(),
            'amount_money' => $quotationDetail->getAmountMoney(),
            'discount' => $quotationDetail->getDiscount(),
            'discount_money' => $quotationDetail->getDiscountMoney()
        ];
    }

    public function includeSchoolFee(QuotationDetail $quotationDetail)
    {
        return new Item($quotationDetail->getSchoolFee(), new SchoolFeeTransformer());
    }
}

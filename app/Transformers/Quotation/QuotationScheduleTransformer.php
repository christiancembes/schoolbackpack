<?php

namespace App\Transformers\Quotation;

use App\Modules\Payment\Models\PaymentTransaction;
use App\Modules\Quotation\Models\Quotation;
use App\Modules\Quotation\Models\QuotationSchedule;
use App\Transformers\Student\StudentTransformer;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

class QuotationScheduleTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @param  QuotationSchedule  $quotationSchedule
     *
     * @return array
     */
    public function transform(QuotationSchedule $quotationSchedule)
    {
        return [
            'code' => $quotationSchedule->getCode(),
            'description' => $quotationSchedule->getDescription(),
            'amount' => $quotationSchedule->getAmount(),
            'amount_money' => $quotationSchedule->getAmountMoney(),
            'completed_at' => $quotationSchedule->getCompletedAt()
        ];
    }

}

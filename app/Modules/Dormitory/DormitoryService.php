<?php

namespace App\Modules\Dormitory;

use App\Exceptions\Dormitory\DormitoryCapacity;
use App\Exceptions\Student\StudentNotActive;
use App\Modules\Dormitory\Repository\DormitoryRepository;
use App\Modules\Dormitory\Repository\DormitoryStudentHistoryRepository;
use App\Modules\Dormitory\Repository\DormitoryStudentRepository;
use App\Modules\Dormitory\Repository\DormitoryWaitingListRepository;
use App\Modules\Notification\NotificationCenterService;
use App\Modules\Student\Models\Student;
use App\Modules\Student\Repository\StudentRepository;
use Carbon\Carbon;

class DormitoryService
{
    protected $dormitoryRepository;

    protected $dormitoryStudentRepository;

    protected $dormitoryStudentHistoryRepository;

    protected $dormitoryWaitingListRepository;

    protected $notificationCenterService;

    public function __construct(
        StudentRepository $studentRepository,
        DormitoryRepository $dormitoryRepository,
        DormitoryStudentRepository $dormitoryStudentRepository,
        DormitoryStudentHistoryRepository $dormitoryStudentHistoryRepository,
        DormitoryWaitingListRepository $dormitoryWaitingListRepository,
        NotificationCenterService $notificationCenterService
    )
    {
        $this->studentRepository = $studentRepository;
        $this->dormitoryRepository = $dormitoryRepository;
        $this->dormitoryStudentRepository = $dormitoryStudentRepository;
        $this->dormitoryStudentHistoryRepository = $dormitoryStudentHistoryRepository;
        $this->dormitoryWaitingListRepository = $dormitoryWaitingListRepository;
        $this->notificationCenterService = $notificationCenterService;
    }

    public function attachStudent($studentId, $dormitoryId, $waitingListId)
    {
        $this->studentValidation($studentId);
        $this->dormitoryValidation($dormitoryId);

        $this->dormitoryWaitingListRepository->updateById($waitingListId, [
            'completed_at' => Carbon::now()
        ]);

        $this->dormitoryStudentHistoryRepository->create([
            'description' => 'Checkin',
            'student_id' => $studentId,
            'dormitory_id' => $dormitoryId
        ]);

        $dormitoryStudent = $this->dormitoryStudentRepository->create([
            'enter_at' => Carbon::now(),
            'student_id' => $studentId,
            'dormitory_id' => $dormitoryId
        ]);

        $this->dormitoryRepository->updateCapacity($dormitoryId);

        $this->notificationCenterService->dormitoryCheckIn($dormitoryStudent->getStudent()->getUser(), $dormitoryStudent->getDormitory());

        return $dormitoryStudent;
    }

    public function detachStudent($studentId, $dormitoryId)
    {
        $this->dormitoryStudentHistoryRepository->create([
            'description' => 'Checkout',
            'student_id' => $studentId,
            'dormitory_id' => $dormitoryId
        ]);

        return $this->dormitoryStudentRepository->create([
            'enter_at' => Carbon::now(),
            'student_id' => $studentId,
            'dormitory_id' => $dormitoryId
        ]);
    }

    protected function studentValidation($studentId)
    {
        $student = $this->studentRepository->getById($studentId);

        if ($student->getStatus() === Student::ACTIVE) {
            return true;
        } else {
            throw new StudentNotActive();
        }
    }

    protected function dormitoryValidation($dormitoryId)
    {
        $dormitory = $this->dormitoryRepository->getById($dormitoryId);

        if ($dormitory->getRemainingCapacity() === 0) {
            throw new DormitoryCapacity();
        }

        return true;
    }
}
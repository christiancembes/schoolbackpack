<?php

namespace App\Modules\Dormitory\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Dormitory\Models\DormitoryStudentHistory;

class DormitoryStudentHistoryRepository extends MasterRepository implements DormitoryStudentHistoryRepositoryInterface
{
    public function model()
    {
        return DormitoryStudentHistory::class;
    }
}
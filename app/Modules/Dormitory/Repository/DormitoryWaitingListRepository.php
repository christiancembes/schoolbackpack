<?php

namespace App\Modules\Dormitory\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Dormitory\Models\DormitoryWaitingList;
use Illuminate\Support\Facades\DB;

class DormitoryWaitingListRepository extends MasterRepository implements DormitoryWaitingListRepositoryInterface
{
    public function model()
    {
        return DormitoryWaitingList::class;
    }

    public function getCapacityByType($type)
    {
        if ($this->makeModel()->select(
            DB::raw('COUNT(1) as total, dormitory_type'),
            )->where('dormitory_type', $type)->groupBy('dormitory_type')->first() !== null) {
            return $this->makeModel()->select(
                DB::raw('COUNT(1) as total, dormitory_type'),
                )->where('dormitory_type', $type)->groupBy('dormitory_type')->first()->toArray();
        } else {
            return [
                'total' => 0
            ];
        }
    }

    public function updateByStudentId($studentId, $data)
    {
        return $this->makeModel()->where('student_id', $studentId)->update($data);
    }
}
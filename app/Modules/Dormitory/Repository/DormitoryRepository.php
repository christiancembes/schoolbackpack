<?php

namespace App\Modules\Dormitory\Repository;

use App\Exceptions\Dormitory\DormitoryCapacity;
use App\Foundations\MasterRepository;
use App\Modules\Dormitory\Models\Dormitory;
use Illuminate\Support\Facades\DB;

class DormitoryRepository extends MasterRepository implements DormitoryRepositoryInterface
{
    protected $dormitoryWaitingListRepository;

    public function __construct(DormitoryWaitingListRepository $dormitoryWaitingListRepository)
    {
        parent::__construct();
        $this->dormitoryWaitingListRepository = $dormitoryWaitingListRepository;
    }

    public function model()
    {
        return Dormitory::class;
    }

    public function checkCapacity($type)
    {
        $dormitory = $this->makeModel()->select(
            DB::raw('SUM(capacity) as total_capacity'),
            DB::raw('SUM(remaining_capacity) as total_remaining_capacity')
            )->where('type', $type)->first()->toArray();

        $dormitoryWaitingList = $this->dormitoryWaitingListRepository->getCapacityByType($type);

        if (($dormitory['total_remaining_capacity'] - $dormitoryWaitingList['total']) <= 0) {
            throw new DormitoryCapacity();
        } else {
            return true;
        }
    }

    public function updateCapacity($dormitoryId)
    {
        $currentCapacity = $this->makeModel()->getDormitoryStudent()->count();

        $dormitory = $this->getById($dormitoryId);

        if (($dormitory->getRemainingCapacity() - 1) >= 0) {
            $this->updateById($dormitoryId, [
                'remaining_capacity' => $dormitory->getRemainingCapacity() - 1
            ]);
        } else {
            throw new DormitoryCapacity();
        }

        return $this->getById($dormitoryId);
    }
}
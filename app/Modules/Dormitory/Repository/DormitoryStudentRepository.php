<?php

namespace App\Modules\Dormitory\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Dormitory\Models\DormitoryStudent;

class DormitoryStudentRepository extends MasterRepository implements DormitoryStudentRepositoryInterface
{
    public function model()
    {
        return DormitoryStudent::class;
    }
}
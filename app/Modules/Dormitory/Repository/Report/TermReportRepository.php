<?php

namespace App\Modules\Dormitory\Repository\Report;

use App\Foundations\MasterRepository;
use App\Modules\Dormitory\Models\Report\TermReport;

class TermReportRepository extends MasterRepository implements TermReportRepositoryInterface
{
    public function model()
    {
        return TermReport::class;
    }

    public function getReportByUser($userId)
    {
        return $this->makeModel()->whereHas('student', function($query) use ($userId) {
            $query->where('user_id', $userId);
        })->paginate();
    }

    public function getReportByIdByUser($id, $userId)
    {
        return $this->makeModel()->where('id', $id)->whereHas('student', function($query) use ($userId) {
            $query->where('user_id', $userId);
        })->first();
    }
}
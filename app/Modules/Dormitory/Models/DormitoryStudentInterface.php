<?php

namespace App\Modules\Dormitory\Models;

use App\Foundations\Base\HaveStudent\HaveStudentModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface DormitoryStudentInterface extends MasterModelInterface, HaveTimestampModel, HaveStudentModel
{
    public function getDormitory();

    public function getEnterAt();

    public function getExitAt();

    public function getDormitoryId();

    public function getStudentId();

}
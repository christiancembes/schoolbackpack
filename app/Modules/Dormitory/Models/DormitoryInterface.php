<?php

namespace App\Modules\Dormitory\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface DormitoryInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getName();

    public function getCapacity();

    public function getRemainingCapacity();

    public function getBuilding();

    public function getFloor();

    public function getType();

}
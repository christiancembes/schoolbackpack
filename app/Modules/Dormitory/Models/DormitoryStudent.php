<?php

namespace App\Modules\Dormitory\Models;

use App\Foundations\Base\HaveStudent\HaveStudentModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class DormitoryStudent extends MasterModel implements DormitoryStudentInterface
{
    use CrudTrait, HaveTimestampModelTrait, HaveStudentModelTrait;

    protected $fillable = [
        'enter_at',
        'exit_at',
        'dormitory_id',
        'student_id',
    ];

    /**
     * RELATIONS
     */

    public function dormitory()
    {
        return $this->belongsTo(Dormitory::class);
    }

    /**
     * ACCESSOR
     */

    public function getDormitory()
    {
        return $this->dormitory;
    }

    public function getEnterAt()
    {
        return $this->enter_at;
    }

    public function getExitAt()
    {
        return $this->exit_at;
    }

    public function getDormitoryId()
    {
        return $this->dormitory_id;
    }

    public function getStudentId()
    {
        return $this->student_id;
    }

    public function setStudent(Student $student)
    {
        // TODO: Implement setStudent() method.
    }
}
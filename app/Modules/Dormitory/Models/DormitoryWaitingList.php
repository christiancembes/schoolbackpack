<?php

namespace App\Modules\Dormitory\Models;

use App\Foundations\Base\HaveStudent\HaveStudentModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class DormitoryWaitingList extends MasterModel implements DormitoryWaitingListInterface
{
    use CrudTrait, HaveTimestampModelTrait, HaveStudentModelTrait;

    protected $fillable = [
        'completed_at',
        'status',
        'dormitory_type',
        'student_id'
    ];

    public function setStudent(Student $student)
    {
        // TODO: Implement setStudent() method.
    }

    public function getStatusStr()
    {
        if ($this->status === 0) {
            return 'Menunggu Pembayaran';
        }
        else if ($this->status === 1) {
            return 'Menunggu Assign Kamar';
        }
    }
}
<?php

namespace App\Modules\Dormitory\Models;

use App\Foundations\Base\HaveStudent\HaveStudentModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface DormitoryWaitingListInterface extends MasterModelInterface, HaveTimestampModel, HaveStudentModel
{
    const WAITING = 0;
    const ACTIVE = 1;
}
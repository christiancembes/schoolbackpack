<?php

namespace App\Modules\Dormitory\Models\Report;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface MonthlyReportScoreInterface extends MasterModelInterface, HaveTimestampModel
{

}
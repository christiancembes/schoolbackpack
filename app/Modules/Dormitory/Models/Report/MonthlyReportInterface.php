<?php

namespace App\Modules\Dormitory\Models\Report;

use App\Foundations\Base\HaveStudent\HaveStudentModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface MonthlyReportInterface extends MasterModelInterface, HaveTimestampModel, HaveStudentModel
{

}
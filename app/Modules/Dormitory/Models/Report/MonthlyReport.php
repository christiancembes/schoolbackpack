<?php

namespace App\Modules\Dormitory\Models\Report;

use App\Foundations\Base\HaveStudent\HaveStudentModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Generals\Models\SchoolPeriod;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class MonthlyReport extends MasterModel implements MonthlyReportInterface
{
    use CrudTrait, HaveTimestampModelTrait, HaveStudentModelTrait;

    protected $table = 'dormitory_student_monthly_reports';

    protected $fillable = [
        'month',
        'academic',
        'behavior',
        'life_skill',
        'student_id',
        'school_period_id',
    ];

    public function setStudent(Student $student)
    {
        // TODO: Implement setStudent() method.
    }

    public function monthlyReportScore()
    {
        return $this->hasMany(MonthlyReportScore::class, 'dormitory_student_monthly_report_id', 'id');
    }

    public function schoolPeriod()
    {
        return $this->belongsTo(SchoolPeriod::class);
    }

    public function getSchoolPeriod()
    {
        return $this->schoolPeriod;
    }

    public function getMonthlyReportScore()
    {
        return $this->monthlyReportScore;
    }

    public function getMonthStr()
    {
        if ($this->month === 1)
        {
            return 'Januari';
        }
        elseif ($this->month === 2)
        {
            return 'Februari';
        }
        elseif ($this->month === 3)
        {
            return 'Maret';
        }
        elseif ($this->month === 4)
        {
            return 'April';
        }
        elseif ($this->month === 5)
        {
            return 'Mei';
        }
        elseif ($this->month === 6)
        {
            return 'Juni';
        }
        elseif ($this->month === 7)
        {
            return 'Juli';
        }
        elseif ($this->month === 8)
        {
            return 'Agustus';
        }
        elseif ($this->month === 9)
        {
            return 'September';
        }
        elseif ($this->month === 10)
        {
            return 'Oktober';
        }
        elseif ($this->month === 11)
        {
            return 'November';
        }
        elseif ($this->month === 12)
        {
            return 'Desember';
        }
    }

    public function getAcademic()
    {
        return $this->academic;
    }

    public function getBehavior()
    {
        return $this->behavior;
    }

    public function getLifeSkill()
    {
        return $this->life_skill;
    }

}
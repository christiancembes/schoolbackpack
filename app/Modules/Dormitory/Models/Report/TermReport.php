<?php

namespace App\Modules\Dormitory\Models\Report;

use App\Foundations\Base\HaveStudent\HaveStudentModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Generals\Models\SchoolPeriod;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class TermReport extends MasterModel implements TermReportInterface
{
    use CrudTrait, HaveTimestampModelTrait, HaveStudentModelTrait;

    protected $table = 'dormitory_student_terms_reports';

    protected $fillable = [
        'term',
        'total_score',
        'value',
        'grade',
        'academic',
        'behavior',
        'life_skill',
        'student_id',
        'school_period_id',
    ];

    public function setStudent(Student $student)
    {
        // TODO: Implement setStudent() method.
    }

    public function schoolPeriod()
    {
        return $this->belongsTo(SchoolPeriod::class);
    }

    public function termReportScore()
    {
        return $this->hasMany(TermReportScore::class, 'dormitory_student_terms_report_id', 'id');
    }

    public function getSchoolPeriod()
    {
        return $this->schoolPeriod;
    }

    public function getTermReportScore()
    {
        return $this->termReportScore;
    }

    public function getTerm()
    {
        return $this->term;
    }

    public function getTotalScore()
    {
        return $this->total_score;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getGrade()
    {
        return $this->grade;
    }

    public function getAcademic()
    {
        return $this->academic;
    }

    public function getBehavior()
    {
        return $this->behavior;
    }

    public function getLifeSkill()
    {
        return $this->life_skill;
    }

}
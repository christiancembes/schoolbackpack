<?php

namespace App\Modules\Dormitory\Models\Report;

use App\Foundations\Base\HaveStudent\HaveStudentModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface TermReportInterface extends MasterModelInterface, HaveTimestampModel, HaveStudentModel
{
    public function getSchoolPeriod();

    public function getTerm();

    public function getTotalScore();

    public function getValue();

    public function getGrade();

}
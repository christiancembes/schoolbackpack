<?php

namespace App\Modules\Dormitory\Models\Report;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface TermReportScoreInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getDormitorySubject();

    public function getScore();

    public function getGrade();

}
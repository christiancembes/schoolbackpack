<?php

namespace App\Modules\Dormitory\Models\Report;

use App\Foundations\Base\HaveStudent\HaveStudentModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Generals\Models\DormitorySubject;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class TermReportScore extends MasterModel implements TermReportScoreInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $table = 'dormitory_student_terms_report_scores';

    protected $fillable = [
        'score',
        'grade',
        'dormitory_student_terms_report_id',
        'dormitory_subject_id',
    ];

    public function dormitorySubject()
    {
        return $this->belongsTo(DormitorySubject::class);
    }

    public function getDormitorySubject()
    {
        return $this->dormitorySubject;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function getGrade()
    {
        return $this->grade;
    }

}
<?php

namespace App\Modules\Dormitory\Models;

use App\Foundations\Base\HaveStudent\HaveStudentModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class DormitoryStudentHistory extends MasterModel implements DormitoryStudentHistoryInterface
{
    use CrudTrait, HaveTimestampModelTrait, HaveStudentModelTrait;

    protected $fillable = [
        'description',
        'dormitory_id',
        'student_id',
    ];

    /**
     * RELATIONS
     */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function dormitory()
    {
        return $this->belongsTo(Dormitory::class);
    }

    /**
     * ACCESSOR
     */

    public function getDormitory()
    {
        return $this->dormitory;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getDormitoryId()
    {
        return $this->dormitory_id;
    }

    public function getStudentId()
    {
        return $this->student_id;
    }

    public function setStudent(Student $student)
    {
        // TODO: Implement setStudent() method.
    }
}
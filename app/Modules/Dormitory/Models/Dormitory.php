<?php

namespace App\Modules\Dormitory\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Dormitory extends MasterModel implements DormitoryInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'name',
        'capacity',
        'remaining_capacity',
        'building',
        'floor',
        'type',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function dormitoryStudent()
    {
        return $this->hasMany(DormitoryStudent::class);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getDormitoryStudent()
    {
        return $this->dormitoryStudent;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCapacity()
    {
        return $this->capacity;
    }

    public function getRemainingCapacity()
    {
        return $this->remaining_capacity;
    }

    public function getBuilding()
    {
        return $this->building;
    }

    public function getFloor()
    {
        return $this->floor;
    }

    public function getType()
    {
        return $this->type;
    }

}
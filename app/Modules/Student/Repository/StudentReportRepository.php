<?php

namespace App\Modules\Student\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Student\Models\StudentReport;

class StudentReportRepository extends MasterRepository implements StudentReportRepositoryInterface
{
    public function model()
    {
        return StudentReport::class;
    }

    public function getReportByUser($userId)
    {
        return $this->makeModel()->whereHas('student', function($query) use ($userId) {
            $query->where('user_id', $userId);
        })->paginate();
    }

    public function getReportByIdByUser($id, $userId)
    {
        return $this->makeModel()->where('id', $id)->whereHas('student', function($query) use ($userId) {
            $query->where('user_id', $userId);
        })->first();
    }
}
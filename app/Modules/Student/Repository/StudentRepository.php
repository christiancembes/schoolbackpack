<?php

namespace App\Modules\Student\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Student\Models\Student;

class StudentRepository extends MasterRepository implements StudentRepositoryInterface
{
    public function model()
    {
        return Student::class;
    }

    public function getByUserId($userId)
    {
        return $this->makeModel()->where('user_id', $userId)->firstOrFail();
    }
}
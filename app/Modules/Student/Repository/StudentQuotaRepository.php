<?php

namespace App\Modules\Student\Repository;

use App\Exceptions\Student\StudentCapacity;
use App\Foundations\MasterRepository;
use App\Modules\Student\Models\StudentQuota;

class StudentQuotaRepository extends MasterRepository implements StudentQuotaRepositoryInterface
{
    public function model()
    {
        return StudentQuota::class;
    }

    public function checkCapacity($schoolPeriodId)
    {
        $studentQuota = $this->where('school_period_id', $schoolPeriodId)->first();

        if ($studentQuota->getRemainingCapacity() > 0) {
            return true;
        } else {
            throw new StudentCapacity();
        }
    }
}
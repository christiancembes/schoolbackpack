<?php

namespace App\Modules\Student;

use App\Modules\Student\Models\Student;
use App\Modules\Student\Models\StudentParent;
use App\Modules\Student\Repository\StudentQuotaRepository;
use App\Modules\Student\Repository\StudentRepository;
use App\Modules\User\User;

class StudentService
{
    public function __construct(StudentRepository $studentRepository, StudentQuotaService $studentQuotaService)
    {
        $this->studentRepository = $studentRepository;
        $this->studentQuotaService = $studentQuotaService;
    }

    public function register($data, $user)
    {
        $studentData = array_merge($data, [
            'user_id' => $user->getId()
        ]);

        $student = Student::create($studentData);

        $studentParentData = array_merge($data['parent'], [
            'user_id' => $user->getId(),
            'student_id' => $student->getId()
        ]);

        $studentParent = StudentParent::create($studentParentData);

        return $student;
    }

    public function cancelRegistration($studentId)
    {
        $this->studentRepository->updateById($studentId, [
            'status' => Student::CANCEL_REGISTRATION_PAYMENT
        ]);

        $student = $this->studentRepository->getById($studentId);

        $this->studentQuotaService->increase($student);
    }

    public function getDetail($id)
    {
        return Student::find($id);
    }

    public function getDetailByUser($userId)
    {
        return $this->studentRepository->getByUserId($userId);
    }
}
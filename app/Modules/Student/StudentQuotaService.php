<?php

namespace App\Modules\Student;

use App\Modules\Payment\Models\PaymentTransaction;
use App\Modules\Student\Models\Student;
use App\Modules\Student\Repository\StudentQuotaRepository;

class StudentQuotaService
{
    protected $studentQuotaRepository;

    public function __construct(StudentQuotaRepository $studentQuotaRepository)
    {
        $this->studentQuotaRepository = $studentQuotaRepository;
    }

    public function decrease(Student $student, PaymentTransaction $paymentTransaction)
    {
        $periodId = $student->getSchoolPeriod()->getId();

        if ($paymentTransaction->getStatus() === PaymentTransaction::SUCCESS) {
            $studentQuota = $this->studentQuotaRepository->where('school_period_id', $periodId)->first();

            $this->studentQuotaRepository->updateById($studentQuota->getId(), [
                'remaining_capacity' => $studentQuota->getRemainingCapacity() - 1
            ]);
        }
    }

    public function increase(Student $student)
    {
        $periodId = $student->getSchoolPeriod()->getId();

        $studentQuota = $this->studentQuotaRepository->where('school_period_id', $periodId)->first();

        $this->studentQuotaRepository->updateById($studentQuota->getId(), [
            'remaining_capacity' => $studentQuota->getRemainingCapacity() + 1
        ]);
    }
}
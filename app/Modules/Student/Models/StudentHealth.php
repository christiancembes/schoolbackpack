<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class StudentHealth extends MasterModel implements StudentHealthInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'tbc',
        'malaria',
        'chotipa',
        'cacar',
        'others',
        'physical_abnormalities',
        'student_id'
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSOR
    |--------------------------------------------------------------------------
    */

    public function getTbc()
    {
        return $this->tbc;

    }
    public function getMalaria()
    {
        return $this->malaria;

    }
    public function getChotipa()
    {
        return $this->chotipa;

    }
    public function getCacar()
    {
        return $this->cacar;

    }
    public function getOthers()
    {
        return $this->others;

    }
    public function getPhysicalAbnormalities()
    {
        return $this->physical_abnormalities;

    }
}
<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveStudent\HaveStudentModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Generals\Models\SchoolPeriod;
use App\Modules\Generals\Models\Subject;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class StudentReportScore extends MasterModel implements StudentReportScoreInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'score',
        'predicate',
        'notes',
        'student_report_id',
        'subject_id'
    ];

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function studentReport()
    {
        return $this->belongsTo(StudentReport::class);
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function getStudentReport()
    {
        return $this->studentReport;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function getPredicate()
    {
        return $this->predicate;
    }

    public function getNotes()
    {
        return $this->notes;
    }
}
<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Location\Models\City;
use App\Modules\Location\Models\District;
use App\Modules\Location\Models\Province;
use App\Modules\Location\Models\Village;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class StudentAddress extends MasterModel implements StudentAddressInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'address',
        'postal_code',
        'phone',
        'live_with',
        'student_id',
        'province_id',
        'city_id',
        'district_id',
        'village_id',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function village()
    {
        return $this->belongsTo(Village::class);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getAddress()
    {
        return $this->address;
    }

    public function getPostalCode()
    {
        return $this->postal_code;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getLiveWith()
    {
        return $this->live_with;
    }

    public function getStudent()
    {
        return $this->student;
    }

    public function getProvince()
    {
        return $this->province;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getDistrict()
    {
        return $this->district;
    }

    public function getVillage()
    {
        return $this->village;
    }

}
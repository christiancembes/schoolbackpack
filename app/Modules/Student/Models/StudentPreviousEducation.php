<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Location\Models\City;
use App\Modules\Location\Models\District;
use App\Modules\Location\Models\Province;
use App\Modules\Location\Models\Village;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class StudentPreviousEducation extends MasterModel implements StudentPreviousEducationInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'school_origin',
        'school_address',
        'phone',
        'head_master_name',
        'nisn',
        'school_status',
        'education_system',
        'period',
        'student_id',
        'province_id',
        'city_id',
        'district_id',
        'village_id',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function village()
    {
        return $this->belongsTo(Village::class);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getSchoolOrigin()
    {
        return $this->school_origin;
    }

    public function getSchoolAddress()
    {
        return $this->school_address;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getHeadMasterName()
    {
        return $this->head_master_name;
    }

    public function getNisn()
    {
        return $this->nisn;
    }

    public function getSchoolStatus()
    {
        return $this->school_status;
    }

    public function getEducationSystem()
    {
        return $this->education_system;
    }

    public function getPeriod()
    {
        return $this->period;
    }

    public function getStudent()
    {
        return $this->student;
    }

    public function getProvince()
    {
        return $this->province;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getDistrict()
    {
        return $this->district;
    }

    public function getVillage()
    {
        return $this->village;
    }

}
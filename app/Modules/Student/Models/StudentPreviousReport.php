<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Generals\Models\Grade;
use App\Modules\Generals\Models\Subject;

class StudentPreviousReport extends MasterModel implements StudentPreviousReportInterface
{
    use HaveTimestampModelTrait;

    protected $fillable = [
        'first_period_score',
        'second_period_score',
        'notes',
        'student_id',
        'subject_id',
        'grade_id',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getFirstPeriodScore()
    {
        return $this->first_period_score;
    }

    public function getSecondPeriodScore()
    {
        return $this->second_period_score;
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function getStudent()
    {
        return $this->student;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function getGrade()
    {
        return $this->grade;
    }

}
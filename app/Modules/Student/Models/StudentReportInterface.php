<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveStudent\HaveStudentModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface StudentReportInterface extends MasterModelInterface, HaveStudentModel, HaveTimestampModel
{
    public function getTerms();

    public function getStudent();

    public function getSchoolPeriod();

}
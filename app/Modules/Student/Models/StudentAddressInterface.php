<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface StudentAddressInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getAddress();

    public function getPostalCode();

    public function getPhone();

    public function getLiveWith();

    public function getStudent();

    public function getProvince();

    public function getCity();

    public function getDistrict();

    public function getVillage();

}
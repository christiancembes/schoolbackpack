<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveStudent\HaveStudentModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Generals\Models\SchoolPeriod;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class StudentQuota extends MasterModel implements StudentQuotaInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'capacity',
        'remaining_capacity',
        'school_period_id',
    ];

    public function schoolPeriod()
    {
        return $this->belongsTo(SchoolPeriod::class);
    }

    public function getCapacity()
    {
        return $this->capacity;
    }

    public function getRemainingCapacity()
    {
        return $this->remaining_capacity;
    }

    public function getSchoolPeriod()
    {
        return $this->schoolPeriod;
    }
}
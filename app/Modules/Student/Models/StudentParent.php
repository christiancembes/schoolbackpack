<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\Base\HaveUser\HaveUserModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Location\Models\Country;

class StudentParent extends MasterModel implements StudentParentInterface
{
    use HaveUserModelTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'type',
        'name',
        'place_of_birth',
        'date_of_birth',
        'profession',
        'last_education',
        'religion',
        'mobile',
        'email',
        'company',
        'office_address',
        'income',
        'student_id',
        'country_id',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSOR
    |--------------------------------------------------------------------------
    */

    public function getType()
    {
        return $this->type;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPlaceOfBirth()
    {
        return $this->place_of_birth;
    }

    public function getDateOfBirth()
    {
        return $this->date_of_birth;
    }

    public function getProfession()
    {
        return $this->profession;
    }

    public function getLastEducation()
    {
        return $this->last_education;
    }

    public function getReligion()
    {
        return $this->religion;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function getOfficeAddress()
    {
        return $this->office_address;
    }

    public function getIncome()
    {
        return $this->income;
    }

    public function getStudent()
    {
        return $this->student;
    }

    public function getCountry()
    {
        return $this->country;
    }

}
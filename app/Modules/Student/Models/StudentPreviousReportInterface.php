<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface StudentPreviousReportInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getFirstPeriodScore();

    public function getSecondPeriodScore();

    public function getNotes();

    public function getStudent();

    public function getSubject();

    public function getGrade();

}
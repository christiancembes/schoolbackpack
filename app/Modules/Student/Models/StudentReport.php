<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveStudent\HaveStudentModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Generals\Models\SchoolPeriod;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class StudentReport extends MasterModel implements StudentReportInterface
{
    use CrudTrait, HaveStudentModelTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'terms',
        'parent_note',
        'guardian_note',
        'absence',
        'achievement',
        'extracurricular',
        'student_id',
        'school_period_id',
    ];

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function schoolPeriod()
    {
        return $this->belongsTo(SchoolPeriod::class);
    }

    public function studentReportScores()
    {
        return $this->hasMany(StudentReportScore::class);
    }

    public function getTerms()
    {
        return $this->terms;
    }

    public function getParentNote()
    {
        return $this->parent_note;
    }

    public function getGuardianNote()
    {
        return $this->guardian_note;
    }

    public function getAbsence()
    {
        return $this->absence;
    }

    public function getAchievement()
    {
        return $this->achievement;
    }

    public function getExtracurricular()
    {
        return $this->extracurricular;
    }


    public function getStudent()
    {
        return $this->student;
    }

    public function getSchoolPeriod()
    {
        return $this->schoolPeriod;
    }

    public function getStudentReportScores()
    {
        return $this->studentReportScores;
    }

    public function setStudent(Student $student)
    {
        // TODO: Implement setStudent() method.
    }
}
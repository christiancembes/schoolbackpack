<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveStudent\HaveStudentModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface StudentQuotaInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getCapacity();

    public function getRemainingCapacity();

    public function getSchoolPeriod();

}
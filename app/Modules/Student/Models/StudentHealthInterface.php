<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface StudentHealthInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getTbc();

    public function getMalaria();

    public function getChotipa();

    public function getCacar();

    public function getOthers();

    public function getPhysicalAbnormalities();

}
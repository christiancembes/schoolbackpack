<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface StudentPreviousEducationInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getSchoolOrigin();

    public function getSchoolAddress();

    public function getPhone();

    public function getHeadMasterName();

    public function getNisn();

    public function getSchoolStatus();

    public function getEducationSystem();

    public function getPeriod();

    public function getStudent();

    public function getProvince();

    public function getCity();

    public function getDistrict();

    public function getVillage();

}
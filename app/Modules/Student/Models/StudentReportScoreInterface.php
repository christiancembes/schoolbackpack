<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveStudent\HaveStudentModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface StudentReportScoreInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getSubject();

    public function getStudentReport();

    public function getScore();

    public function getNotes();
}
<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveName\HaveNameModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\Base\HaveUser\HaveUserModel;
use App\Foundations\MasterModelInterface;

interface StudentInterface extends MasterModelInterface, HaveUserModel, HaveNameModel, HaveTimestampModel
{
    const INACTIVE = 0;
    const ACTIVE = 1;
    const WAITING_REGISTRATION_PAYMENT = 2;
    const WAITING_BUILDING_PAYMENT = 3;
    const WAITING_ACTIVATION = 4;
    const CANCEL_REGISTRATION_PAYMENT = 5;
    const CANCEL_BUILDING_PAYMENT = 6;
    const SCHOOL_MOVE = 7;
    const LOST = 8;
    const PASS_AWAY = 9;

    const WAITING_PAYMENT = [
        self::WAITING_REGISTRATION_PAYMENT, self::WAITING_BUILDING_PAYMENT
    ];

    const ALL_CANCEL = [
        self::CANCEL_REGISTRATION_PAYMENT, self::CANCEL_BUILDING_PAYMENT
    ];

    public function getNis();

    public function getNisn();

    public function getFullName();

    public function getName();

    public function getGender();

    public function getPlaceOfBirth();

    public function getDateOfBirth();

    public function getGoal();

    public function getHobby();

    public function getNik();

    public function getOrderInFamily();

    public function getNumbersOfSiblings();

    public function getNumbersOfStepFamilies();

    public function getNumbersOfAdoptedFamilies();

    public function getWeight();

    public function getHeight();

    public function getBloodType();

    public function getMotherLanguage();

    public function getNameOfSibling();

    public function getStatus();

    public function getCountry();

    public function getStudentCategory();

    public function getStudentType();

    public function getGrade();

    public function getGradeClass();

    public function getGradeOfSibling();

    public function getFacility();

    public function getMajor();

    public function getMarketer();

    public function getAddress();

    public function getParents();

    public function getHealth();

    public function getPreviousEducation();

    public function getPreviousReports();

    public function getSchoolPeriod();

}
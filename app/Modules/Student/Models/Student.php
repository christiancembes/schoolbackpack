<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveName\HaveNameModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\Base\HaveUser\HaveUserModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Generals\Models\Facility;
use App\Modules\Generals\Models\Grade;
use App\Modules\Generals\Models\GradeClass;
use App\Modules\Generals\Models\Major;
use App\Modules\Generals\Models\Marketer;
use App\Modules\Generals\Models\SchoolPeriod;
use App\Modules\Generals\Models\StudentCategory;
use App\Modules\Generals\Models\StudentType;
use App\Modules\Location\Models\Country;
use App\Modules\User\User;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Student extends MasterModel implements StudentInterface
{
    use CrudTrait, HaveUserModelTrait, HaveNameModelTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'nis',
        'nisn',
        'full_name',
        'name',
        'gender',
        'place_of_birth',
        'date_of_birth',
        'goal',
        'hobby',
        'nik',
        'order_in_family',
        'numbers_of_siblings',
        'numbers_of_step_families',
        'numbers_of_adopted_families',
        'weight',
        'height',
        'blood_type',
        'mother_language',
        'name_of_sibling',
        'status',
        'user_id',
        'country_id',
        'student_category_id',
        'student_type_id',
        'grade_id',
        'grade_class_id',
        'grade_of_sibling_id',
        'facility_id',
        'major_id',
        'school_period_id',
        'marketer_id',
    ];

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeWhereActive($query)
    {
        return $query->where('status', Student::ACTIVE);
    }

    public function scopeWhereWaiting($query)
    {
        return $query->where('status', 4);
    }

    public function scopeWhereWaitingPayment($query)
    {
        return $query->whereIn('status', Student::WAITING_PAYMENT);
    }

    public function scopeWhereCancel($query)
    {
        return $query->whereIn('status', Student::ALL_CANCEL);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function studentCategory()
    {
        return $this->belongsTo(StudentCategory::class);
    }

    public function student_category()
    {
        return $this->belongsTo(StudentCategory::class);
    }

    public function studentType()
    {
        return $this->belongsTo(StudentType::class);
    }

    public function student_type()
    {
        return $this->belongsTo(StudentType::class);
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    public function gradeClass()
    {
        return $this->belongsTo(GradeClass::class);
    }

    public function grade_class()
    {
        return $this->belongsTo(GradeClass::class);
    }

    public function gradeOfSibling()
    {
        return $this->belongsTo(Grade::class, 'grade_of_sibling_id', 'id');
    }

    public function facility()
    {
        return $this->belongsTo(Facility::class);
    }

    public function major()
    {
        return $this->belongsTo(Major::class);
    }

    public function marketer()
    {
        return $this->belongsTo(Marketer::class);
    }

    public function schoolPeriod()
    {
        return $this->belongsTo(SchoolPeriod::class);
    }

    public function school_period()
    {
        return $this->belongsTo(SchoolPeriod::class);
    }

    public function address()
    {
        return $this->hasOne(StudentAddress::class);
    }

    public function parents()
    {
        return $this->hasMany(StudentParent::class);
    }

    public function health()
    {
        return $this->hasOne(StudentHealth::class);
    }

    public function previousEducation()
    {
        return $this->hasOne(StudentPreviousEducation::class);
    }

    public function previous_education()
    {
        return $this->hasOne(StudentPreviousEducation::class);
    }

    public function previousReports()
    {
        return $this->hasMany(StudentPreviousReport::class);
    }

    public function previous_reports()
    {
        return $this->hasMany(StudentPreviousReport::class);
    }

    public function reports()
    {
        return $this->hasMany(StudentReport::class);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getNis()
    {
        return $this->nis;
    }

    public function getNisn()
    {
        return $this->nisn;
    }

    public function getFullName()
    {
        return $this->full_name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getPlaceOfBirth()
    {
        return $this->place_of_birth;
    }

    public function getDateOfBirth()
    {
        return $this->date_of_birth;
    }

    public function getGoal()
    {
        return $this->goal;
    }

    public function getHobby()
    {
        return $this->hobby;
    }

    public function getNik()
    {
        return $this->nik;
    }

    public function getOrderInFamily()
    {
        return $this->order_in_family;
    }

    public function getNumbersOfSiblings()
    {
        return $this->numbers_of_siblings;
    }

    public function getNumbersOfStepFamilies()
    {
        return $this->numbers_of_step_families;
    }

    public function getNumbersOfAdoptedFamilies()
    {
        return $this->numbers_of_adopted_families;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getBloodType()
    {
        return $this->blood_type;
    }

    public function getMotherLanguage()
    {
        return $this->mother_language;
    }

    public function getNameOfSibling()
    {
        return $this->name_of_sibling;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getStudentCategory()
    {
        return $this->studentCategory;
    }

    public function getStudentType()
    {
        return $this->studentType;
    }

    public function getGrade()
    {
        return $this->grade;
    }

    public function getGradeClass()
    {
        return $this->gradeClass;
    }

    public function getGradeOfSibling()
    {
        return $this->gradeOfSibling;
    }

    public function getFacility()
    {
        return $this->facility;
    }

    public function getMajor()
    {
        return $this->major;
    }

    public function getMarketer()
    {
        return $this->marketer;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getParents()
    {
        return $this->parents;
    }

    public function getHealth()
    {
        return $this->health;
    }

    public function getPreviousEducation()
    {
        return $this->previousEducation;
    }

    public function getPreviousReports()
    {
        return $this->previousReports;
    }

    public function getSchoolPeriod()
    {
        return $this->schoolPeriod;
    }

    public function getStatusStr()
    {
        if ($this->status === 0) {
            return 'Inactive';
        }
        else if ($this->status === 1) {
            return 'Aktif';
        }
        else if ($this->status === 2) {
            return 'Menunggu Pembayaran Registrasi';
        }
        else if ($this->status === 3) {
            return 'Menunggu Pembayaran Gedung';
        }
        else if ($this->status === 4) {
            return 'Menunggu Aktifasi';
        }
        else if ($this->status === 5) {
            return 'Batal Pembayaran Registrasi';
        }
        else if ($this->status === 6) {
            return 'Batal Pembayaran Gedung';
        }
        else if ($this->status === 7) {
            return 'Mutasi';
        }
        else if ($this->status === 8) {
            return 'Hilang';
        }
        else if ($this->status === 9) {
            return 'Meninggal';
        }
    }

    public function getReports()
    {
        return $this->reports;
    }
}
<?php

namespace App\Modules\Student\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\Base\HaveUser\HaveUserModel;
use App\Foundations\MasterModelInterface;

interface StudentParentInterface extends MasterModelInterface, HaveUserModel, HaveTimestampModel
{
    public function getType();

    public function getName();

    public function getPlaceOfBirth();

    public function getDateOfBirth();

    public function getProfession();

    public function getLastEducation();

    public function getReligion();

    public function getMobile();

    public function getEmail();

    public function getCompany();

    public function getOfficeAddress();

    public function getIncome();

    public function getStudent();

    public function getCountry();

}
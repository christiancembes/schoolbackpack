<?php

namespace App\Modules\Student;

use App\Modules\Dormitory\Repository\Report\MonthlyReportRepository;
use App\Modules\Dormitory\Repository\Report\TermReportRepository;
use App\Modules\Student\Repository\StudentReportRepository;

class StudentReportService
{
    protected $studentReportRepository;

    protected $monthlyReportRepository;

    protected $termReportRepository;

    public function __construct(StudentReportRepository $studentReportRepository, MonthlyReportRepository $monthlyReportRepository, TermReportRepository $termReportRepository)
    {
        $this->studentReportRepository = $studentReportRepository;
        $this->monthlyReportRepository = $monthlyReportRepository;
        $this->termReportRepository = $termReportRepository;
    }

    public function getAll($attributes, $user)
    {
        return $this->studentReportRepository->getReportByUser($user->id);
    }

    public function getById($id, $attributes, $user)
    {
        return $this->studentReportRepository->getReportByIdByUser($id, $user->id);
    }

    public function getDormitoryMonthlyAll($attributes, $user)
    {
        return $this->monthlyReportRepository->getReportByUser($user->id);
    }

    public function getDormitoryMonthlyById($id, $attributes, $user)
    {
        return $this->monthlyReportRepository->getReportByIdByUser($id, $user->id);
    }

    public function getDormitoryTermAll($attributes, $user)
    {
        return $this->termReportRepository->getReportByUser($user->id);
    }

    public function getDormitoryTermById($id, $attributes, $user)
    {
        return $this->termReportRepository->getReportByIdByUser($id, $user->id);
    }


    public function sendById($id)
    {
        $report = $this->studentReportRepository->getById($id);

//        $user->notify(new PaymentRegistration($user, $mailData));
    }
}
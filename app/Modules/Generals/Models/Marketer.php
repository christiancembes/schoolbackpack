<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\Base\HaveUser\HaveUserModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Marketer extends MasterModel implements MarketerInterface
{
    use CrudTrait, HaveNameModelTrait, HaveTimestampModelTrait, HaveUserModelTrait;

    protected $fillable = [
        'name',
        'balance',
        'nip'
    ];

    public function marketerFeeHistory()
    {
        return $this->hasMany(MarketerFeeHistory::class);
    }

    public function getMarketerFeeHistory()
    {
        return $this->marketerFeeHistory;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function getBalanceMoney()
    {
        return 'Rp. ' . number_format($this->balance);
    }

    public function getNip()
    {
        return $this->nip;
    }

}
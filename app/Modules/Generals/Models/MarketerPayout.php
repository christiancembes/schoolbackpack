<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class MarketerPayout extends MasterModel implements MarketerPayoutInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'amount',
        'status',
        'marketer_id',
    ];

    public function marketer()
    {
        return $this->belongsTo(Marketer::class);
    }

    public function getMarketer()
    {
        return $this->marketer;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getAmountMoney()
    {
        return 'Rp. '.number_format($this->amount);
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getMarketerId()
    {
        return $this->marketer_id;
    }

    public function getStatusStr()
    {
        if ($this->status == 0) return 'Menunggu Konfirmasi';
        else if ($this->status == 1) return 'Berhasil';
    }

}
<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Employee extends MasterModel implements EmployeeInterface
{
    use CrudTrait, HaveTimestampModelTrait, HaveNameModelTrait;

    protected $fillable = [
        'nip',
        'name',
        'position',
        'status'
    ];

    public function getNip()
    {
        return $this->nip;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function getStatus()
    {
        return $this->status;
    }

}
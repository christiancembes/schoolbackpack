<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Subject extends MasterModel implements SubjectInterface
{
    use CrudTrait, HaveNameModelTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'name',
        'group'
    ];

    public function getGroup()
    {
        return $this->group;
    }
}
<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface GradeInterface extends MasterModelInterface, HaveNameModel, HaveTimestampModel
{

}
<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class SchoolFee extends MasterModel implements SchoolFeeInterface
{
    use CrudTrait, SoftDeletes, HaveTimestampModelTrait;

    protected $table = 'school_fees';

    protected $fillable = [
        'type',
        'name',
        'amount',
        'description',
        'payment_system',
        'school_period_id',
        'major_id',
    ];

    /*
     * RELATIONS
     */

    public function schoolPeriod()
    {
        return $this->belongsTo(SchoolPeriod::class);
    }

    public function major()
    {
        return $this->belongsTo(Major::class);
    }

    /*
     * ACCESSOR
     */

    public function getType()
    {
        return $this->type;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getPaymentSystem()
    {
        return $this->payment_system;
    }

    public function getSchoolPeriod()
    {
        return $this->schoolPeriod;
    }

    public function getMajor()
    {
        return $this->major;
    }

    /*
     * MUTATOR
     */

    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = Str::slug($value);
    }

}
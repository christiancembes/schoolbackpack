<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface EmployeeInterface extends MasterModelInterface, HaveTimestampModel, HaveNameModel
{
    public function getNip();

    public function getName();

    public function getPosition();

    public function getStatus();

}
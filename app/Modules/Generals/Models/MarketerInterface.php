<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\Base\HaveUser\HaveUserModel;
use App\Foundations\MasterModelInterface;

interface MarketerInterface extends MasterModelInterface, HaveNameModel, HaveTimestampModel, HaveUserModel
{
    public function getMarketerFeeHistory();

    public function getName();

    public function getBalance();

    public function getNip();

}
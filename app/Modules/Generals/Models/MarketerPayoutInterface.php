<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface MarketerPayoutInterface extends MasterModelInterface, HaveTimestampModel
{
    const PENDING = 0;
    const COMPLETE = 1;

    public function getAmount();

    public function getStatus();

    public function getMarketerId();

}
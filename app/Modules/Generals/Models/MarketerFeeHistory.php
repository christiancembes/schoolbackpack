<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\Base\HaveUser\HaveUserModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class MarketerFeeHistory extends MasterModel implements MarketerFeeHistoryInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'description',
        'amount',
        'marketer_id'
    ];

    public function marketer()
    {
        return $this->belongsTo(Marketer::class);
    }

    public function getMarketer()
    {
        return $this->marketer;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getAmountMoney()
    {
        return 'Rp. ' . number_format($this->amount);
    }

}
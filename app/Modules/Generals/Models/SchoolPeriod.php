<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Student\Models\StudentQuota;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class SchoolPeriod extends MasterModel implements SchoolPeriodInterface
{
    use CrudTrait, HaveNameModelTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'name',
        'active'
    ];

    public function studentQuota()
    {
        return $this->hasOne(StudentQuota::class);
    }
}
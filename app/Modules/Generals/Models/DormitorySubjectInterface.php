<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface DormitorySubjectInterface extends MasterModelInterface, HaveNameModel, HaveTimestampModel
{
    public function getGroup();
}
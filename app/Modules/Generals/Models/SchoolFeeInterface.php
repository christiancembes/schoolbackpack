<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface SchoolFeeInterface extends MasterModelInterface, HaveTimestampModel
{
    const REGISTRATION_FEE = 'registration-fee';

    public function getType();

    public function getName();

    public function getAmount();

    public function getDescription();

    public function getPaymentSystem();

    public function getSchoolPeriod();

    public function getMajor();

    public function setTypeAttribute($value);

}
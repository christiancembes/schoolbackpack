<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\Base\HaveUser\HaveUserModel;
use App\Foundations\MasterModelInterface;

interface MarketerFeeHistoryInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getMarketer();

    public function getDescription();

    public function getAmount();

}
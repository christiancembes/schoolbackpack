<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface StudentTypeInterface extends MasterModelInterface, HaveNameModel, HaveTimestampModel
{
    const FULLDAY = 1;
    const BOARDING = 2;
}
<?php

namespace App\Modules\Generals\Models;

use App\Foundations\Base\HaveName\HaveNameModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class GradeClass extends MasterModel implements GradeInterface
{
    use CrudTrait, HaveNameModelTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'name'
    ];

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }
}
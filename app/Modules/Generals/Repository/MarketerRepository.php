<?php

namespace App\Modules\Generals\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Generals\Models\Marketer;

class MarketerRepository extends MasterRepository
{
    public function model()
    {
        return Marketer::class;
    }
}
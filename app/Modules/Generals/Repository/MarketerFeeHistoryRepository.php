<?php

namespace App\Modules\Generals\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Generals\Models\MarketerFeeHistory;

class MarketerFeeHistoryRepository extends MasterRepository implements MarketerFeeHistoryRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function model()
    {
        return MarketerFeeHistory::class;
    }
}
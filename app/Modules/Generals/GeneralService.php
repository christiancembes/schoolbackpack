<?php

namespace App\Modules\Generals;

use App\Modules\Generals\Models\Facility;
use App\Modules\Generals\Models\Grade;
use App\Modules\Generals\Models\GradeClass;
use App\Modules\Generals\Models\Major;
use App\Modules\Generals\Models\Marketer;
use App\Modules\Generals\Models\SchoolFee;
use App\Modules\Generals\Models\SchoolPeriod;
use App\Modules\Generals\Models\StudentCategory;
use App\Modules\Generals\Models\StudentType;
use App\Modules\Generals\Models\Subject;

class GeneralService
{
    /**
     * @return SchoolPeriod[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getSchoolPeriod()
    {
        return SchoolPeriod::all();
    }

    /**
     * @return SchoolPeriod[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getSchoolFee()
    {
        return SchoolFee::all();
    }

    /**
     * @return Facility[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getFacility()
    {
        return Facility::all();
    }

    /**
     * @return Grade[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getGrade()
    {
        return Grade::all();
    }

    public function getGradeClass($gradeId)
    {
        return GradeClass::where('grade_id', $gradeId)->get();
    }

    /**
     * @return Major[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getMajor()
    {
        return Major::all();
    }

    /**
     * @return Marketer[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getMarketer()
    {
        return Marketer::all();
    }

    /**
     * @return StudentCategory[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getStudentCategory()
    {
        return StudentCategory::all();
    }

    /**
     * @return StudentType[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getStudentType()
    {
        return StudentType::all();
    }

    /**
     * @return Subject[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getSubject()
    {
        return Subject::all();
    }
}
<?php

namespace App\Modules\Generals;

use App\Modules\Generals\Models\Marketer;
use App\Modules\Generals\Models\MarketerPayout;
use App\Modules\Generals\Repository\MarketerFeeHistoryRepository;
use App\Modules\Generals\Repository\MarketerRepository;
use App\Modules\Notification\InternalNotificationCenterService;
use App\Modules\Notification\NotificationCenterService;
use App\Modules\Quotation\Models\Quotation;

class MarketerService
{
    protected $marketerRepository;

    protected $marketerFeeHistoryRepository;

    protected $internalNotificationCenterService;

    protected $notificationCenterService;

    public function __construct(MarketerRepository $marketerRepository, MarketerFeeHistoryRepository $marketerFeeHistoryRepository, InternalNotificationCenterService $internalNotificationCenterService, NotificationCenterService $notificationCenterService)
    {
        $this->marketerRepository = $marketerRepository;
        $this->marketerFeeHistoryRepository = $marketerFeeHistoryRepository;
        $this->internalNotificationCenterService = $internalNotificationCenterService;
        $this->notificationCenterService = $notificationCenterService;
    }

    public function redeemFee(Quotation $quotation)
    {
        $student = $quotation->getStudent();

        if ($student->getMarketer()) {
            $marketer = $student->getMarketer();

            if ($quotation->getPayedAmount() >= config('core.marketer_fee_trigger')) {

                $this->marketerFeeHistoryRepository->create([
                    'description' => 'Fee Registration',
                    'amount' => config('core.marketer_fee'),
                    'marketer_id' => $marketer->getId()
                ]);

                $this->marketerRepository->updateById($marketer->getId(), [
                    'balance' => $marketer->getBalance() + config('core.marketer_fee')
                ]);
            }
        }

    }

    public function requestPayout($data)
    {
        $data['status'] = MarketerPayout::PENDING;

        $marketerPayout = MarketerPayout::create($data);

        $marketer = Marketer::where('id', $data['marketer_id'])->firstOrFail();

        $this->internalNotificationCenterService->marketerPayoutNotificationToFinance($marketer, $marketerPayout);

        return $marketerPayout;
    }

    public function confirmPayout($id)
    {
        $marketerPayout = MarketerPayout::where('id', $id)->firstOrFail();

        $marketer = Marketer::where('id', $marketerPayout->getMarketer()->getId())->firstOrFail();

        Marketer::where('id', $marketerPayout->getMarketer()->getId())->update([
            'balance' => $marketer->getBalance() - $marketerPayout->getAmount()
        ]);

        $this->notificationCenterService->marketerConfirmPayoutNotification($marketer, $marketerPayout);

        return MarketerPayout::where('id', $id)->update([
            'status' => MarketerPayout::COMPLETE
        ]);
    }
}
<?php

namespace App\Modules\Payment\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface PaymentConfirmationInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getCode();

    public function getAmount();

    public function getSenderName();

    public function getSenderBankAccount();

    public function getTransferAt();

    public function getCompletedAt();

}
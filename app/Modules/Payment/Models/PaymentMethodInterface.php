<?php

namespace App\Modules\Payment\Models;

use App\Foundations\Base\HaveName\HaveNameModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface PaymentMethodInterface extends MasterModelInterface, HaveNameModel, HaveTimestampModel
{
    public function getSlug();

    public function getBank();

    public function getAccountNo();

    public function paymentGateway();

}
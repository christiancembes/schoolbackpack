<?php

namespace App\Modules\Payment\Models;

use App\Foundations\Base\HaveCode\HaveCodeModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentTransaction extends MasterModel implements PaymentTransactionInterface
{
    use CrudTrait, SoftDeletes, HaveCodeModelTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'code',
        'description',
        'total_amount',
        'status',
        'completed_at',
        'expired_at',
        'payment_method_id',
        'student_id',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function payment_method()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function confirmation()
    {
        return $this->hasOne(PaymentConfirmation::class, 'code', 'code');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    public function scopeHasSuccess($query)
    {
        return $query->where('status', PaymentTransaction::SUCCESS);
    }

    public function scopeWherePending($query)
    {
        return $query->where('status', PaymentTransaction::PENDING);
    }

    public function scopeRegistrationTransaction($query)
    {
        return $query->where('code', 'LIKE', env('REGISTRATION_PREFIX') . '-' . '%');
    }

    public function scopeQuotationTransaction($query)
    {
        return $query->where('code', 'LIKE', env('QUOTATION_SCHEDULE_PREFIX') . '-' . '%');
    }

    public function scopeExactAmount($query, $amount)
    {
        return $query->where('total_amount', $amount);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getDescription()
    {
        return $this->description;
    }

    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    public function getTotalAmountMoney()
    {
        return 'Rp. '.number_format($this->total_amount, 2);
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getCompletedAt()
    {
        return $this->completed_at;
    }

    public function getExpiredAt()
    {
        return $this->expired_at;
    }

    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    public function getStudent()
    {
        return $this->student;
    }

    public function getStatusStr()
    {
        if ($this->status === 0) {
            return trans('payment.waiting');
        }
        else if ($this->status === 1) {
            return trans('payment.success');
        }
        else if ($this->status === 2) {
            return trans('payment.expired');
        }
        else if ($this->status === 3) {
            return trans('payment.cancel');
        }
        else if ($this->status === 4) {
            return trans('payment.waiting_confirmation');
        }
    }

}
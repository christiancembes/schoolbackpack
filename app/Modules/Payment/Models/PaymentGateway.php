<?php

namespace App\Modules\Payment\Models;

use App\Foundations\Base\HaveName\HaveNameModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentGateway extends MasterModel implements PaymentGatewayInterface
{
    use CrudTrait, SoftDeletes, HaveNameModelTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'name',
        'slug',
    ];

    public function getSlug()
    {
        return $this->slug;
    }
}
<?php

namespace App\Modules\Payment\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class PaymentConfirmation extends MasterModel implements PaymentConfirmationInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'code',
        'amount',
        'sender_name',
        'sender_bank_account',
        'proof_of_payment',
        'transfer_at',
        'completed_at'
    ];

    /*
     * SCOPE
     */

    public function scopeHasSuccess($query)
    {
        return $query->whereNotNull('completed_at');
    }

    /*
     * ACCESSOR
     */

    public function getCode()
    {
        return $this->code;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getSenderName()
    {
        return $this->sender_name;
    }

    public function getSenderBankAccount()
    {
        return $this->sender_bank_account;
    }

    public function getTransferAt()
    {
        return $this->transfer_at;
    }

    public function getCompletedAt()
    {
        return $this->completed_at;
    }

}
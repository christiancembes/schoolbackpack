<?php

namespace App\Modules\Payment\Models;

use App\Foundations\Base\HaveName\HaveNameModelTrait;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends MasterModel implements PaymentMethodInterface
{
    use CrudTrait, SoftDeletes, HaveNameModelTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'name',
        'slug',
        'bank',
        'account_no',
        'payment_gateway_id',
    ];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function paymentGateway()
    {
        return $this->belongsTo(PaymentGateway::class);
    }

    public function payment_gateway()
    {
        return $this->belongsTo(PaymentGateway::class);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getSlug()
    {
        return $this->slug;
    }

    public function getBank()
    {
        return $this->bank;
    }

    public function getAccountNo()
    {
        return $this->account_no;
    }

    public function getPaymentGateway()
    {
        return $this->paymentGateway;
    }

}
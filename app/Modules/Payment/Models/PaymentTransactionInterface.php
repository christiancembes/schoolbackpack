<?php

namespace App\Modules\Payment\Models;

use App\Foundations\Base\HaveCode\HaveCodeModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface PaymentTransactionInterface extends MasterModelInterface, HaveCodeModel, HaveTimestampModel
{
    const PENDING = 0;
    const SUCCESS = 1;
    const EXPIRED = 2;
    const CANCELED = 3;
    const WAITING_CONFIRMATION = 4;

    public function getDescription();

    public function getTotalAmount();

    public function getStatus();

    public function getCompletedAt();

    public function getExpiredAt();

    public function getPaymentMethod();

    public function getStudent();

}
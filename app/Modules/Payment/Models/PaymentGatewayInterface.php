<?php

namespace App\Modules\Payment\Models;

use App\Foundations\Base\HaveName\HaveNameModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface PaymentGatewayInterface extends MasterModelInterface, HaveNameModel, HaveTimestampModel
{
    public function getSlug();
}
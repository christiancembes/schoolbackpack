<?php

namespace App\Modules\Payment;

use App\Modules\Payment\Repository\PaymentTransactionRepository;

class PaymentTransactionService
{
    protected $paymentTransactionRepository;

    public function __construct(
        PaymentTransactionRepository $paymentTransactionRepository
    )
    {
        $this->paymentTransactionRepository = $paymentTransactionRepository;
    }

    public function getAll($attributes, $user)
    {
        return $this->paymentTransactionRepository->getTransactionByUser($user->id);
    }

    public function getByCode($code, $attributes, $user)
    {
        return $this->paymentTransactionRepository->getTransactionByCodeByUser($code, $user->id);
    }

    public function create($data)
    {
        return $this->paymentTransactionRepository->create($data);
    }

}
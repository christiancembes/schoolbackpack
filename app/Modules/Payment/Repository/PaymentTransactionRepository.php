<?php

namespace App\Modules\Payment\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Payment\Models\PaymentTransaction;

class PaymentTransactionRepository extends MasterRepository
{
    protected $scopes = [
        'hasSuccess',
        'exactAmount'
    ];

    public function model()
    {
        return PaymentTransaction::class;
    }

    public function updateByCode($code, $data)
    {
        $this->unsetClauses();

        $model = $this->where('code', $code)->first();

        return $this->updateById($model->getId(), $data);
    }

    public function getTransactionByUser($userId)
    {
        return $this->makeModel()->whereHas('student', function($query) use ($userId) {
            $query->where('user_id', $userId);
        })->paginate();
    }

    public function getTransactionByCodeByUser($code, $userId)
    {
        return $this->makeModel()->where('code', $code)->whereHas('student', function($query) use ($userId) {
            $query->where('user_id', $userId);
        })->first();
    }

    public function getPendingTransaction($type)
    {
        if ($type == 'registration') {
            return $this->makeModel()->registrationTransaction()->wherePending()->get();
        } else {
            return $this->makeModel()->quotationTransaction()->wherePending()->get();
        }
    }

    public function checkSuccess($data)
    {
        return $this->makeModel()->where('code', $data['code'])->hasSuccess()->exists();
    }

    public function amountCheck($data)
    {
        return $this->makeModel()->where('code', $data['code'])->exactAmount($data['amount'])->exists();
    }
}
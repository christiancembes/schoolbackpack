<?php

namespace App\Modules\Payment\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Payment\Models\PaymentConfirmation;

class PaymentConfirmationRepository extends MasterRepository
{
    public function model()
    {
        return PaymentConfirmation::class;
    }

    public function updateByCode($code, $data)
    {
        $this->unsetClauses();

        $model = $this->where('code', $code)->first();

        return $this->updateById($model->getId(), $data);
    }

    public function checkSuccess($data)
    {
        return $this->makeModel()->where('code', $data['code'])->hasSuccess()->exists();
    }
}
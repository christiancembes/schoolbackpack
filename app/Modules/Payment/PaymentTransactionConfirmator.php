<?php

namespace App\Modules\Payment;

use App\Exceptions\Payment\PaymentAmountNotMatch;
use App\Exceptions\Payment\PaymentCodeNotValid;
use App\Exceptions\Payment\PaymentHasActive;
use App\Modules\Dormitory\Models\DormitoryWaitingList;
use App\Modules\Dormitory\Repository\DormitoryWaitingListRepository;
use App\Modules\Generals\MarketerService;
use App\Modules\Generals\Models\StudentType;
use App\Modules\Notification\InternalNotificationCenterService;
use App\Modules\Notification\NotificationCenterService;
use App\Modules\Payment\Models\PaymentTransaction;
use App\Modules\Payment\Repository\PaymentConfirmationRepository;
use App\Modules\Payment\Repository\PaymentTransactionRepository;
use App\Modules\Quotation\Repository\QuotationRepository;
use App\Modules\Quotation\Repository\QuotationScheduleRepository;
use App\Modules\Student\Models\Student;
use App\Modules\Student\Repository\StudentRepository;
use App\Modules\Student\StudentQuotaService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class PaymentTransactionConfirmator
{
    protected $paymentTransactionRepository;

    protected $paymentConfirmationRepository;

    protected $quotationRepository;

    protected $quotationScheduleRepository;

    protected $studentRepository;

    protected $internalNotificationCenterService;

    protected $notificationCenterService;

    protected $dormitoryWaitingListRepository;

    protected $marketerService;

    protected $studentQuotaService;

    public function __construct(
        PaymentTransactionRepository $paymentTransactionRepository,
        PaymentConfirmationRepository $paymentConfirmationRepository,
        QuotationRepository $quotationRepository,
        QuotationScheduleRepository $quotationScheduleRepository,
        StudentRepository $studentRepository,
        InternalNotificationCenterService $internalNotificationCenterService,
        NotificationCenterService $notificationCenterService,
        DormitoryWaitingListRepository $dormitoryWaitingListRepository,
        MarketerService $marketerService,
        StudentQuotaService $studentQuotaService
    )
    {
        $this->paymentTransactionRepository = $paymentTransactionRepository;
        $this->paymentConfirmationRepository = $paymentConfirmationRepository;
        $this->quotationRepository = $quotationRepository;
        $this->quotationScheduleRepository = $quotationScheduleRepository;
        $this->studentRepository = $studentRepository;
        $this->internalNotificationCenterService = $internalNotificationCenterService;
        $this->notificationCenterService = $notificationCenterService;
        $this->dormitoryWaitingListRepository = $dormitoryWaitingListRepository;
        $this->marketerService = $marketerService;
        $this->studentQuotaService = $studentQuotaService;
    }

    public function confirmation($data)
    {
        if ($this->paymentTransactionRepository->checkSuccess($data)) {
            throw new PaymentHasActive();
        } elseif (!$this->paymentTransactionRepository->amountCheck($data)) {
            throw new PaymentAmountNotMatch();
        }

        $uploadFolder = 'users';

        $image = $data['proof_of_payment'];

        $image_uploaded_path = $image->store($uploadFolder, 'public');

        $uploadedImageResponse = array(
            "image_name" => basename($image_uploaded_path),
            "image_url" => Storage::disk('public')->url($image_uploaded_path),
            "mime" => $image->getClientMimeType()
        );

        $data['proof_of_payment'] = $uploadedImageResponse['image_url'];

        $paymentTransactionData = [
            'status' => PaymentTransaction::WAITING_CONFIRMATION
        ];

        $paymentConfirmation = $this->paymentConfirmationRepository->create($data);

        $this->paymentTransactionRepository->updateByCode($data['code'], $paymentTransactionData);

        $this->internalNotificationCenterService->registrationFeeNotificationToFinance($paymentConfirmation);
    }

    public function cmsConfirmation($code)
    {
//        if ($this->paymentConfirmationRepository) {
//
//        }

        $now = Carbon::now();

        $paymentConfirmationData = [
            'completed_at' => $now
        ];

        $paymentConfirmation = $this->paymentConfirmationRepository->updateByCode($code, $paymentConfirmationData);

        $paymentTransactionData = [
            'status' => PaymentTransaction::SUCCESS,
            'completed_at' => $now
        ];

        $paymentTransaction = $this->paymentTransactionRepository->updateByCode($code, $paymentTransactionData);

        $paymentCodePrefix = explode('-', $code)[0];

        if ($paymentCodePrefix == env('REGISTRATION_PREFIX')) {
            $this->confirmRegistration($paymentTransaction, $paymentConfirmation);
        } elseif ($paymentCodePrefix == env('QUOTATION_SCHEDULE_PREFIX')) {
            $this->confirmQuotation($paymentTransaction, $paymentConfirmation);
        } else {
            throw new PaymentCodeNotValid();
        }

        return [
            'success' => true
        ];
    }

    protected function confirmQuotation($paymentTransaction, $paymentConfirmation)
    {
        $now = Carbon::now();

        $quotationSchedule = $this->quotationScheduleRepository->updateByCode($paymentTransaction->getCode(), [
            'completed_at' => $now
        ]);

        $quotation = $this->quotationRepository->getById($quotationSchedule->getQuotation()->getId());

        $quotation = $this->quotationRepository->updateById($quotation->getId(), [
            'remaining_amount' => $quotation->getRemainingAmount() - $quotationSchedule->getAmount(),
            'completed_at' => ($quotation->getRemainingAmount() - $quotationSchedule->getAmount() <= 0) ? $now : null
        ]);

        $this->notificationCenterService->paymentReceived($paymentTransaction->getStudent()->getUser(), $paymentConfirmation);

        // Redeem Marketing Fee
        $this->marketerService->redeemFee($quotation);

        // Check If Payment is Complete and activate student
        if ($quotation->getRemainingAmount() - $quotationSchedule->getAmount() <= 0) {
            $this->studentRepository->updateById($paymentTransaction->getStudent()->getId(), [
                'status' => Student::ACTIVE
            ]);

            $this->notificationCenterService->studentActivated($paymentTransaction->getStudent()->getUser());
        }
    }

    protected function confirmRegistration($paymentTransaction, $paymentConfirmation)
    {
        $studentData = [
            'status' => Student::WAITING_BUILDING_PAYMENT
        ];

        $this->studentRepository->updateById($paymentTransaction->getStudent()->getId(), $studentData);

        $this->studentQuotaService->decrease($paymentTransaction->getStudent(), $paymentTransaction);

        if ($paymentTransaction->getStudent()->getStudentType()->getId() === StudentType::BOARDING) {
            $this->dormitoryWaitingListRepository->updateByStudentId($paymentTransaction->getStudent()->getId(), [
                'status' => DormitoryWaitingList::ACTIVE
            ]);
        }

        $this->notificationCenterService->paymentReceived($paymentTransaction->getStudent()->getUser(), $paymentConfirmation);
    }
}
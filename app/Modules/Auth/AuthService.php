<?php

namespace App\Modules\Auth;

use App\Modules\User\User;
use App\Notifications\WelcomeEmail;

class AuthService
{
    public function register($validatedData, $request)
    {
        $validatedData['password'] = bcrypt($request->password);

        $user = User::create($validatedData);

        $user->assignRole('user');

        $user->notify(new WelcomeEmail());

        return response([ 'user' => $user]);
    }

    public function login($loginData)
    {
        if (!auth()->attempt($loginData)) {
            return response(['message' => 'Invalid Credentials']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response(['user' => auth()->user(), 'access_token' => $accessToken]);
    }
}

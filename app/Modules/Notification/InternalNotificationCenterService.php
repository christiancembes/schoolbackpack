<?php

namespace App\Modules\Notification;

use App\Modules\Generals\Models\Marketer;
use App\Modules\Generals\Models\MarketerPayout;
use App\Modules\Payment\Models\PaymentConfirmation;
use App\Modules\User\User;
use App\Notifications\Marketer\RequestPayout;
use App\Notifications\Payment\RegistrationFeeSubmitted;

class InternalNotificationCenterService
{
    protected function getFinance()
    {
        return User::find(1);
    }

    public function registrationFeeNotificationToFinance(PaymentConfirmation $paymentConfirmation)
    {
        $user = $this->getFinance();

        $mailData = [
            'paymentConfirmation' => $paymentConfirmation
        ];

        $user->notify(new RegistrationFeeSubmitted($mailData));
    }

    public function marketerPayoutNotificationToFinance(Marketer $marketer, MarketerPayout $marketerPayout)
    {
        $user = $this->getFinance();

        $mailData = [
            'marketer' => $marketer,
            'marketerPayout' => $marketerPayout
        ];

        $user->notify(new RequestPayout($mailData));
    }
}
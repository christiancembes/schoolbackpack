<?php

namespace App\Modules\Notification;

use App\Modules\Dormitory\Models\Dormitory;
use App\Modules\Generals\Models\Marketer;
use App\Modules\Generals\Models\MarketerPayout;
use App\Modules\Quotation\Models\Quotation;
use App\Modules\User\User;
use App\Notifications\Dormitory\Checkin;
use App\Notifications\Marketer\ConfirmPayout;
use App\Notifications\Payment\Received;
use App\Notifications\Payment\RegistrationFeeReminder;
use App\Notifications\PaymentRegistration;
use App\Notifications\Quotation\Reminder;
use App\Notifications\Quotation\Submitted;
use App\Notifications\Student\Activated;
use App\Notifications\WelcomeEmail;

class NotificationCenterService
{
    public function registration(User $user, $pass)
    {
        $mailData = [
            'pass' => $pass
        ];

        $user->notify(new WelcomeEmail($user, $mailData));
    }

    public function registrationFeePayment(User $user)
    {
        $mailData = [];

        $user->notify(new PaymentRegistration($user, $mailData));
    }

    public function registrationPaymentRemainderNotification(User $user, $paymentTransaction)
    {
        $mailData = [
            'paymentTransaction' => $paymentTransaction
        ];

        $user->notify(new RegistrationFeeReminder($user, $mailData));
    }

    public function quotationNotification(User $user, Quotation $quotation)
    {
        $mailData = [
            'quotation' => $quotation,
            'quotation_details' => $quotation->getQuotationDetails(),
            'quotation_schedules' => $quotation->getQuotationSchedules()
        ];

        $user->notify(new Submitted($user, $mailData));
    }

    public function quotationReminderNotification(User $user, Quotation $quotation)
    {
        $mailData = [
            'quotation' => $quotation,
            'quotation_details' => $quotation->getQuotationDetails(),
            'quotation_schedules' => $quotation->getQuotationSchedules()
        ];

        $user->notify(new Reminder($user, $mailData));
    }

    public function paymentReceived(User $user, $paymentConfirmation)
    {
        $mailData = [
            'payment_confirmation' => $paymentConfirmation
        ];

        $user->notify(new Received($user, $mailData));
    }

    public function studentActivated(User $user)
    {
        $mailData = [];

        $user->notify(new Activated($user));
    }

    public function dormitoryCheckIn(User $user, Dormitory $dormitory)
    {
        $mailData = [
            'dormitory' => $dormitory
        ];

        $user->notify(new Checkin($user, $mailData));
    }

    public function marketerConfirmPayoutNotification(Marketer $marketer, MarketerPayout $marketerPayout)
    {
        $mailData = [
            'marketer' => $marketer,
            'marketerPayout' => $marketerPayout
        ];

        $marketer->getUser()->notify(new ConfirmPayout($mailData));
    }
}
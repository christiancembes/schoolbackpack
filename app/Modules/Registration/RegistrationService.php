<?php

namespace App\Modules\Registration;

use App\Modules\Dormitory\Repository\DormitoryRepository;
use App\Modules\Dormitory\Repository\DormitoryWaitingListRepository;
use App\Modules\Notification\NotificationCenterService;
use App\Modules\Student\Models\Student;
use App\Modules\Student\Models\StudentAddress;
use App\Modules\Student\Models\StudentHealth;
use App\Modules\Student\Models\StudentParent;
use App\Modules\Student\Models\StudentPreviousEducation;
use App\Modules\Student\Models\StudentPreviousReport;
use App\Modules\User\User;

class RegistrationService
{
    protected $registrationPaymentService;

    protected $notificationCenterService;

    protected $dormitoryRepository;

    protected $dormitoryWaitingListRepository;

    public function __construct(
        RegistrationPaymentService $registrationPaymentService,
        NotificationCenterService $notificationCenterService,
        DormitoryRepository $dormitoryRepository,
        DormitoryWaitingListRepository $dormitoryWaitingListRepository
    )
    {
        $this->registrationPaymentService = $registrationPaymentService;
        $this->notificationCenterService = $notificationCenterService;
        $this->dormitoryRepository = $dormitoryRepository;
        $this->dormitoryWaitingListRepository = $dormitoryWaitingListRepository;
        $this->password = 'alexandria';
    }

    public function register($data)
    {
        // TODO: Add check student capacity
        $dormitoryCapacity = $this->dormitoryRepository->checkCapacity($data['gender']);

        $user = $this->registerUser($data);

        $student = $this->studentRegistration($data, $user);

        $this->studentAddress($data['address'], $student);

        $this->studentParents($data['parents'], $student);

        $this->studentHealth($data['health'], $student);

        $this->studentPreviousEducation($data['previous_education'], $student);

        if (@$data['previous_report']) {
            $this->studentPreviousReport($data['previous_report'], $student);
        }

        $payment = $this->registrationPaymentService->createPayment($student);

        if ($dormitoryCapacity) {
            if ($student->getStudentType()->getId() === 2) {
                $this->dormitoryWaitingListRepository->create([
                    'student_id' => $student->getId(),
                    'dormitory_type' => $student->getGender(),
                    'status' => 0
                ]);
            }
        }

        // Send Registration & Registration Payment Email
        $this->notificationCenterService->registration($user, $this->password);
        $this->notificationCenterService->registrationFeePayment($user, $payment);

        return $payment;
    }

    protected function studentPreviousReport($datas, Student $student)
    {
        foreach ($datas as $data) {
            $prevReportData = array_merge($data, [
                'student_id' => $student->getId()
            ]);

            StudentPreviousReport::create($prevReportData);
        }
    }

    protected function studentPreviousEducation($data, Student $student)
    {
        $prevEducationData = array_merge($data, [
            'student_id' => $student->getId()
        ]);

        StudentPreviousEducation::create($prevEducationData);
    }

    protected function studentHealth($data, Student $student)
    {
        $healthData = array_merge($data, [
            'student_id' => $student->getId()
        ]);

        StudentHealth::create($healthData);
    }

    protected function studentParents($datas, Student $student)
    {
        foreach ($datas as $data) {
            if (isset($data['father'])) {
                $parentData = array_merge($data['father'], [
                    'student_id' => $student->getId()
                ]);

                StudentParent::create($parentData);
            } elseif (isset($data['mother'])) {
                $parentData = array_merge($data['mother'], [
                    'student_id' => $student->getId()
                ]);

                StudentParent::create($parentData);
            } elseif (isset($data['wali'])) {
                $parentData = array_merge($data['wali'], [
                    'student_id' => $student->getId()
                ]);

                StudentParent::create($parentData);
            }
        }
    }

    protected function studentAddress($data, Student $student)
    {
        $addressData = array_merge($data, [
            'student_id' => $student->getId()
        ]);

        return StudentAddress::create($addressData);
    }

    protected function studentRegistration($data, User $user)
    {
        $studentData = array_merge($data, [
            'status' => Student::WAITING_REGISTRATION_PAYMENT,
            'user_id' => $user->getId()
        ]);

        return Student::create($studentData);
    }

    protected function registerUser($data)
    {
        if (isset($data['parents']['father'])) {
            $email = $data['parents']['father']['email'];
        } elseif (isset($data['parents']['mother'])) {
            $email = $data['parents']['mother']['email'];
        } elseif (isset($data['parents']['wali'])) {
            $email = $data['parents']['wali']['email'];
        }

        $userData = [
            'name' => $data['name'],
            'email' => $email,
            'password' => bcrypt($this->password)
        ];

        $user = User::create($userData);

        $user->assignRole('user');

        return $user;
    }
}
<?php

namespace App\Modules\Registration;

use App\Foundations\Traits\CodeGenerator;
use App\Modules\Generals\Models\SchoolFee;
use App\Modules\Payment\Models\PaymentMethod;
use App\Modules\Payment\Models\PaymentTransaction;
use App\Modules\Payment\PaymentTransactionService;
use App\Modules\Student\Models\Student;
use App\Notifications\PaymentRegistration;
use Illuminate\Support\Str;

class RegistrationPaymentService
{
    use CodeGenerator;

    protected $paymentTransactionService;

    public function __construct(PaymentTransactionService $paymentTransactionService)
    {
        $this->paymentTransactionService = $paymentTransactionService;
    }

    public function createPayment(Student $student)
    {
        $schooFee = SchoolFee::where('school_period_id', $student->getSchoolPeriod()->getId())
                                ->where('grade_id', $student->getGrade()->getId())
                                ->where('student_type_id', $student->getStudentType()->getId())
                                ->where('type', Str::slug(SchoolFee::REGISTRATION_FEE . ' ' . $student->getStudentType()->getName()))
                                ->firstOrFail();

        $paymentMethod = PaymentMethod::find(1);

        $paymentTransactionData = [
            'code' => $this->generateCode(7, env('REGISTRATION_PREFIX')),
            'description' => $schooFee->getName(),
            'student_id' => $student->getId(),
            'total_amount' => $schooFee->getAmount(),
            'status' => PaymentTransaction::PENDING,
            'payment_method_id' => $paymentMethod->getId(),
        ];

        return $this->paymentTransactionService->create($paymentTransactionData);
    }
}
<?php

namespace App\Modules\User;

use App\Foundations\Base\HaveId\HaveIdModel;
use App\Foundations\Base\HaveName\HaveNameModel;
use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;

interface UserInterface extends HaveIdModel, HaveNameModel, HaveTimestampModel
{
    public function getEmail();
}
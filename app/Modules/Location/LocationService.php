<?php

namespace App\Modules\Location;

use App\Modules\Location\Models\City;
use App\Modules\Location\Models\Country;
use App\Modules\Location\Models\District;
use App\Modules\Location\Models\Province;
use App\Modules\Location\Models\Village;

class LocationService
{
    /**
     * @return Country[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getCountry()
    {
        return Country::all();
    }

    /**
     * @return Province[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getProvince()
    {
        return Province::all();
    }

    /**
     * @param $provinceId
     *
     * @return mixed
     */
    public function getCity($provinceId)
    {
        return City::where('province_id', $provinceId)->get();
    }

    /**
     * @param $cityId
     *
     * @return mixed
     */
    public function getDistrict($cityId)
    {
        return District::where('city_id', $cityId)->get();
    }

    /**
     * @param $districtId
     *
     * @return mixed
     */
    public function getVillage($districtId)
    {
        return Village::where('district_id', $districtId)->get();
    }
}
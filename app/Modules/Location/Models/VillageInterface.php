<?php

namespace App\Modules\Location\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface VillageInterface extends MasterModelInterface, HaveTimestampModel
{

}
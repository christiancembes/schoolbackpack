<?php

namespace App\Modules\Location\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends MasterModel implements CountryInterface
{
    use CrudTrait, SoftDeletes, HaveTimestampModelTrait;

    protected $fillable = [
        'name'
    ];

    public function getName()
    {
        return $this->name;
    }
}

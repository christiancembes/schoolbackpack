<?php

namespace App\Modules\Quotation\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Quotation extends MasterModel implements QuotationInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'code',
        'description',
        'total_amount',
        'total_discount',
        'remaining_amount',
        'final_amount',
        'has_revised',
        'completed_at',
        'student_id'
    ];

    /**
     * RELATIONS
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    public function quotationDetails()
    {
        return $this->hasMany(QuotationDetail::class);
    }

    public function quotationSchedules()
    {
        return $this->hasMany(QuotationSchedule::class);
    }

    /**
     * SCOPE
     */

    public function scopeWherePending($query)
    {
        return $query->whereNull('completed_at');
    }

    /**
     * ACCESSORS
     */

    public function getStudent()
    {
        return $this->student;
    }

    public function getQuotationDetails()
    {
        return $this->quotationDetails;
    }

    public function getQuotationSchedules()
    {
        return $this->quotationSchedules;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getTotalAmount()
    {
        return $this->total_amount;
    }

    public function getTotalAmountMoney()
    {
        return 'Rp. ' . number_format($this->total_amount);
    }

    public function getTotalDiscount()
    {
        return $this->total_discount;
    }

    public function getTotalDiscountMoney()
    {
        return 'Rp. ' . number_format($this->total_discount);
    }

    public function getRemainingAmount()
    {
        return $this->remaining_amount;
    }

    public function getRemainingAmountMoney()
    {
        return 'Rp. ' . number_format($this->remaining_amount);
    }

    public function getFinalAmount()
    {
        return $this->final_amount;
    }

    public function getFinalAmountMoney()
    {
        return 'Rp. ' . number_format($this->final_amount);
    }

    public function getHasRevised()
    {
        return $this->has_revised;
    }

    public function getCompletedAt()
    {
        return $this->completed_at;
    }

    public function getPayedAmount()
    {
        return $this->final_amount - $this->remaining_amount;
    }

}
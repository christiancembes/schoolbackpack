<?php

namespace App\Modules\Quotation\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface QuotationDetailInterface extends MasterModelInterface, HaveTimestampModel
{
    public function quotation();

    public function schoolFee();

    public function getDescription();

    public function getAmount();

    public function getDiscount();

    public function getQuotationId();

    public function getSchoolFeeId();

}
<?php

namespace App\Modules\Quotation\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class QuotationSchedule extends MasterModel implements QuotationScheduleInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'code',
        'description',
        'amount',
        'completed_at',
        'quotation_id'
    ];

    /**
     * RELATIONS
     */

    public function quotation()
    {
        return $this->belongsTo(Quotation::class);
    }

    /**
     * ACCESSORS
     */

    public function getQuotation()
    {
        return $this->quotation;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getAmountMoney()
    {
        return 'Rp. ' . number_format($this->amount);
    }

    public function getCompletedAt()
    {
        return $this->completed_at;
    }

}
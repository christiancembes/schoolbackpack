<?php

namespace App\Modules\Quotation\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface QuotationScheduleInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getQuotation();

    public function getCode();

    public function getDescription();

    public function getAmount();

    public function getCompletedAt();

}
<?php

namespace App\Modules\Quotation\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModelTrait;
use App\Foundations\MasterModel;
use App\Modules\Generals\Models\SchoolFee;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class QuotationDetail extends MasterModel implements QuotationDetailInterface
{
    use CrudTrait, HaveTimestampModelTrait;

    protected $fillable = [
        'description',
        'amount',
        'discount',
        'quotation_id',
        'school_fee_id'
    ];

    /**
     * RELATIONS
     */

    public function quotation()
    {
        return $this->belongsTo(Quotation::class);
    }

    public function schoolFee()
    {
        return $this->belongsTo(SchoolFee::class);
    }

    /**
     * ACCESSORS
     */

    public function getSchoolFee()
    {
        return $this->schoolFee;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getAmountMoney()
    {
        return 'Rp. ' . number_format($this->amount);
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function getDiscountMoney()
    {
        return 'Rp. ' . number_format($this->discount);
    }

    public function getQuotationId()
    {
        return $this->quotation_id;
    }

    public function getSchoolFeeId()
    {
        return $this->school_fee_id;
    }

}
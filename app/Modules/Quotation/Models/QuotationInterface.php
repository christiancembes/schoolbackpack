<?php

namespace App\Modules\Quotation\Models;

use App\Foundations\Base\HaveTimestamp\HaveTimestampModel;
use App\Foundations\MasterModelInterface;

interface QuotationInterface extends MasterModelInterface, HaveTimestampModel
{
    public function getStudent();

    public function getQuotationDetails();

    public function getQuotationSchedules();

    public function getCode();

    public function getDescription();

    public function getTotalAmount();

    public function getTotalDiscount();

    public function getRemainingAmount();

    public function getFinalAmount();

    public function getCompletedAt();

}
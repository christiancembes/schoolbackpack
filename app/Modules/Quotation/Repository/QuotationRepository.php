<?php

namespace App\Modules\Quotation\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Quotation\Models\Quotation;

class QuotationRepository extends MasterRepository implements QuotationRepositoryInterface
{
    public function model()
    {
        return Quotation::class;
    }

    public function getPendingQuotation()
    {
        return $this->makeModel()->wherePending()->get();
    }

    public function getQuotationByUser($userId)
    {
        return $this->makeModel()->whereHas('student', function($query) use ($userId) {
            $query->where('user_id', $userId);
        })->paginate();
    }

    public function getQuotationByCodeByUser($code, $userId)
    {
        return $this->makeModel()->where('code', $code)->whereHas('student', function($query) use ($userId) {
            $query->where('user_id', $userId);
        })->first();
    }
}
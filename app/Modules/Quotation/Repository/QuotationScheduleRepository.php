<?php

namespace App\Modules\Quotation\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Quotation\Models\QuotationSchedule;

class QuotationScheduleRepository extends MasterRepository implements QuotationScheduleRepositoryInterface
{
    public function model()
    {
        return QuotationSchedule::class;
    }

    public function updateByCode($code, $data)
    {
        $this->unsetClauses();

        $model = $this->where('code', $code)->first();

        return $this->updateById($model->getId(), $data);
    }
}
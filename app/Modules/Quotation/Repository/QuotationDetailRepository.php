<?php

namespace App\Modules\Quotation\Repository;

use App\Foundations\MasterRepository;
use App\Modules\Quotation\Models\QuotationDetail;

class QuotationDetailRepository extends MasterRepository implements QuotationDetailRepositoryInterface
{
    public function model()
    {
        return QuotationDetail::class;
    }
}
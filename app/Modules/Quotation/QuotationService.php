<?php

namespace App\Modules\Quotation;

use App\Foundations\Traits\CodeGenerator;
use App\Modules\Notification\NotificationCenterService;
use App\Modules\Payment\Models\PaymentMethod;
use App\Modules\Payment\Models\PaymentTransaction;
use App\Modules\Payment\PaymentTransactionService;
use App\Modules\Quotation\Models\Quotation;
use App\Modules\Quotation\Models\QuotationSchedule;
use App\Modules\Quotation\Repository\QuotationDetailRepository;
use App\Modules\Quotation\Repository\QuotationRepository;
use App\Modules\Quotation\Repository\QuotationScheduleRepository;
use App\Modules\Student\Models\Student;
use App\Modules\User\User;

class QuotationService
{
    use CodeGenerator;

    protected $quotationRepository;

    protected $quotationDetailRepository;

    protected $quotationScheduleRepository;

    protected $paymentTransactionService;

    protected $notificationCenterService;

    public function __construct(
        QuotationRepository $quotationRepository,
        QuotationDetailRepository $quotationDetailRepository,
        QuotationScheduleRepository $quotationScheduleRepository,
        PaymentTransactionService $paymentTransactionService,
        NotificationCenterService $notificationCenterService
    )
    {
        $this->quotationRepository = $quotationRepository;
        $this->quotationDetailRepository = $quotationDetailRepository;
        $this->quotationScheduleRepository = $quotationScheduleRepository;
        $this->paymentTransactionService = $paymentTransactionService;
        $this->notificationCenterService = $notificationCenterService;
    }

    public function getAll($attributes, User $user)
    {
        return $this->quotationRepository->getQuotationByUser($user->id);
    }

    public function getByCode($code, $attributes, User $user)
    {
        return $this->quotationRepository->getQuotationByCodeByUser($code, $user->id);
    }

    public function sendQuotation($quotationId)
    {
        $quotation = $this->quotationRepository->getById($quotationId);

        $quotation = $this->quotationRepository->updateById($quotationId, [
            'code' => $this->generateCode(7, env('QUOTATION_PREFIX')),
            'has_revised' => $quotation->getHasRevised() + 1
        ]);

        $user = $quotation->getStudent()->getUser();

        if ($quotation->getQuotationSchedules()->count() > 0) {
            foreach ($quotation->getQuotationSchedules() as $quotationSchedule) {
                $quotationSchedule = $this->quotationScheduleRepository->updateById($quotationSchedule->getId(), [
                    'code' => $this->generateCode(7, env('QUOTATION_SCHEDULE_PREFIX'))
                ]);

                $this->createPaymentSchdule($quotation->getStudent(), $quotationSchedule);
            }
        } else {
            $this->createPayment($quotation->getStudent(), $quotation);
        }

        $quotation = $this->quotationRepository->getById($quotationId);

        $this->notificationCenterService->quotationNotification($user, $quotation);
    }

    protected function createPaymentSchdule(Student $student, QuotationSchedule $quotationSchedule)
    {
        $paymentMethod = PaymentMethod::find(1);

        $paymentTransactionData = [
            'code' => $quotationSchedule->getCode(),
            'description' => $quotationSchedule->getDescription(),
            'student_id' => $student->getId(),
            'total_amount' => $quotationSchedule->getAmount(),
            'status' => PaymentTransaction::PENDING,
            'payment_method_id' => $paymentMethod->getId(),
        ];

        return $this->paymentTransactionService->create($paymentTransactionData);
    }

    protected function createPayment(Student $student, Quotation $quotation)
    {
        $paymentMethod = PaymentMethod::find(1);

        $paymentTransactionData = [
            'code' => $quotation->getCode(),
            'description' => 'Development Fee',
            'student_id' => $student->getId(),
            'total_amount' => $quotation->getFinalAmount(),
            'status' => PaymentTransaction::PENDING,
            'payment_method_id' => $paymentMethod->getId(),
        ];

        return $this->paymentTransactionService->create($paymentTransactionData);
    }
}
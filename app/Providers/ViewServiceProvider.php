<?php

namespace App\Providers;

use App\Http\View\Composers\DashboardComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    public function boot()
    {
        View::composer('backpack::dashboard', DashboardComposer::class);
    }
}
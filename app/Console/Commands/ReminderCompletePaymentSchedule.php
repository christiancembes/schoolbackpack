<?php

namespace App\Console\Commands;

use App\Modules\Notification\NotificationCenterService;
use App\Modules\Payment\Repository\PaymentTransactionRepository;
use App\Modules\Quotation\Repository\QuotationRepository;
use Illuminate\Console\Command;

class ReminderCompletePaymentSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:complete-payment-schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email Reminder for Quotations Payment';

    private $quotationRepository;

    private $notificationCenterService;

    /**
     * Create a new command instance.
     *
     * @param  QuotationRepository  $quotationRepository
     * @param  NotificationCenterService  $notificationCenterService
     */
    public function __construct(QuotationRepository $quotationRepository, NotificationCenterService $notificationCenterService)
    {
        parent::__construct();
        $this->quotationRepository = $quotationRepository;
        $this->notificationCenterService = $notificationCenterService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $pendingQuotations = $this->quotationRepository->getPendingQuotation();

        foreach ($pendingQuotations as $pendingQuotation) {
            $user = $pendingQuotation->getStudent()->getUser();

            $this->notificationCenterService->quotationReminderNotification($user, $pendingQuotation);
        }
    }
}

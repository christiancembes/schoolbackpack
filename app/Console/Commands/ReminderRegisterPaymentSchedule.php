<?php

namespace App\Console\Commands;

use App\Modules\Notification\NotificationCenterService;
use App\Modules\Payment\Repository\PaymentTransactionRepository;
use Illuminate\Console\Command;

class ReminderRegisterPaymentSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:register-payment-schedule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email Reminder for Registration Payment';

    private $paymentTransactionRepository;

    private $notificationCenterService;

    /**
     * Create a new command instance.
     *
     * @param  PaymentTransactionRepository  $paymentTransactionRepository
     * @param  NotificationCenterService  $notificationCenterService
     */
    public function __construct(PaymentTransactionRepository $paymentTransactionRepository, NotificationCenterService $notificationCenterService)
    {
        parent::__construct();
        $this->paymentTransactionRepository = $paymentTransactionRepository;
        $this->notificationCenterService = $notificationCenterService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $pendingTransactions = $this->paymentTransactionRepository->getPendingTransaction('registration');

        foreach ($pendingTransactions as $pendingTransaction) {
            $user = $pendingTransaction->getStudent()->getUser();

            $this->notificationCenterService->registrationPaymentRemainderNotification($user, $pendingTransaction);
        }
    }
}

<?php

namespace App\Notifications\Marketer;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ConfirmPayout extends Notification implements ShouldQueue
{
    use Queueable;

    protected $mailData;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (env('APP_ENV') === 'production') {
            $subject = 'Request Payout Berhasil';
        } else {
            $subject = '[' . env('APP_ENV') . '] - Request Payout Berhasil';
        }

        return (new MailMessage)
            ->subject($subject)
            ->markdown('mail.marketer.confirm-payout', [
                'mailData' => $this->mailData
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

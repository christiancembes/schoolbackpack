<?php

namespace App\Notifications\Dormitory;

use App\Modules\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Checkin extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user;

    protected $mailData;

    /**
     * Create a new notification instance.
     *
     * @param  User  $user
     * @param $mailData
     */
    public function __construct(User $user, $mailData)
    {
        $this->user = $user;
        $this->mailData = $mailData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (env('APP_ENV') === 'production') {
            $subject = 'Detail Asrama';
        } else {
            $subject = '[' . env('APP_ENV') . '] - Detail Asrama';
        }

        return (new MailMessage)
            ->subject($subject)
            ->markdown('mail.dormitory.checkin', [
                'user' => $this->user,
                'mailData' => $this->mailData
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

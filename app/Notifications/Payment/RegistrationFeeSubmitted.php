<?php

namespace App\Notifications\Payment;

use App\Models\Admin\Payment\PaymentConfirmation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RegistrationFeeSubmitted extends Notification implements ShouldQueue
{
    use Queueable;

    protected $mailData;

    /**
     * Create a new notification instance.
     *
     */
    public function __construct($mailData)
    {
        $this->mailData = $mailData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (env('APP_ENV') === 'production') {
            $subject = 'Konfirmasi Pembayaran';
        } else {
            $subject = '[' . env('APP_ENV') . '] - Konfirmasi Pembayaran';
        }

        return (new MailMessage)
            ->subject($subject)
            ->markdown('mail.payment.registration-fee-submitted', [
                'mailData' => $this->mailData
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

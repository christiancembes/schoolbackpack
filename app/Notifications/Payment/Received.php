<?php

namespace App\Notifications\Payment;

use App\Modules\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Received extends Notification implements ShouldQueue
{
    use Queueable;

    protected $user;

    protected $mailData;

    /**
     * Create a new notification instance.
     *
     * @param  User  $user
     * @param $mailData
     */
    public function __construct(User $user, $mailData)
    {
        $this->user = $user;
        $this->mailData = $mailData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if (env('APP_ENV') === 'production') {
            $subject = 'Pembayaran Telah Kami Terima';
        } else {
            $subject = '[' . env('APP_ENV') . '] - Pembayaran Telah Kami Terima';
        }

        return (new MailMessage)
            ->subject($subject)
            ->markdown('mail.payment.received', [
                'user' => $this->user,
                'mailData' => $this->mailData
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class QuotationDetailRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $quotationDetails = json_decode($value, true);

        foreach ($quotationDetails as $quotationDetail) {
            if ($quotationDetail['school_fee_id'] === null) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Biaya sekolah wajib diisi.';
    }
}

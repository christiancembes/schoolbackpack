<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class StudentReportTermScoreRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $scores = json_decode($value, true);

        foreach ($scores as $score) {
            if ($score['dormitory_subject_id'] === null || $score['score'] === "") {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Nilai mata pelajaran wajib diisi.';
    }
}

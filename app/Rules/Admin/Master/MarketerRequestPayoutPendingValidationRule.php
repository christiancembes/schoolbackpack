<?php

namespace App\Rules\Admin\Master;

use App\Modules\Generals\Models\MarketerPayout;
use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\HttpFoundation\ParameterBag;

class MarketerRequestPayoutPendingValidationRule implements Rule
{
    /**
     * @var array
     */
    protected $requestData;

    /**
     * MarketerRequestPayoutPendingValidationRule constructor.
     *
     * @param  ParameterBag  $request
     */
    public function __construct(ParameterBag $request)
    {
        $this->requestData = $request->all();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !MarketerPayout::where('marketer_id', $this->requestData['marketer_id'])
            ->where('status', MarketerPayout::PENDING)
            ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Terdapat request payout yang masih menunggu.';
    }
}

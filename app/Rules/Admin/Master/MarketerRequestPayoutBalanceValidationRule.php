<?php

namespace App\Rules\Admin\Master;

use App\Modules\Generals\Models\Marketer;
use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\HttpFoundation\ParameterBag;

class MarketerRequestPayoutBalanceValidationRule implements Rule
{
    /**
     * @var array
     */
    protected $requestData;

    /**
     * MarketerRequestPayoutBalanceValidationRule constructor.
     *
     * @param  ParameterBag  $request
     */
    public function __construct(ParameterBag $request)
    {
        $this->requestData = $request->all();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $marketer = Marketer::where('id', $this->requestData['marketer_id'])->firstOrFail();

        if ((int)$marketer->getBalance() < (int)$value) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Saldo Tidak Mencukupi.';
    }
}

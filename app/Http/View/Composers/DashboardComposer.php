<?php

namespace App\Http\View\Composers;

use App\Modules\Dormitory\Models\Dormitory;
use App\Modules\Dormitory\Models\DormitoryWaitingList;
use App\Modules\Generals\Models\SchoolPeriod;
use App\Modules\Student\Models\Student;
use App\Modules\Student\Models\StudentQuota;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class DashboardComposer
{
    public function __construct()
    {
    }

    public function compose(View $view)
    {
        $schoolPeriod = SchoolPeriod::where('active', 1)->firstOrFail();
        $activeStudentCount = Student::whereActive()->count();
        $waitingStudentCount = Student::whereWaiting()->count();
        $waitingPaymentStudentCount = Student::where('status',
            Student::WAITING_REGISTRATION_PAYMENT)->count();
        $waitingFullPaymentStudentCount = Student::where('status',
            Student::WAITING_BUILDING_PAYMENT)->count();
        $dormitory = Dormitory::select(
            DB::raw('SUM(capacity) as total_capacity'),
            DB::raw('SUM(remaining_capacity) as total_remaining_capacity')
        )->first()->toArray();
//        $dormitoryBooking = DormitoryWaitingList::where
        $studentQuota = StudentQuota::where('school_period_id', $schoolPeriod->getId())->first()->toArray();

        $view->with('schoolPeriod', $schoolPeriod);
        $view->with('activeStudentCount', $activeStudentCount);
        $view->with('waitingStudentCount', $waitingStudentCount);
        $view->with('waitingPaymentStudentCount', $waitingPaymentStudentCount);
        $view->with('waitingFullPaymentStudentCount', $waitingFullPaymentStudentCount);
        $view->with('dormitory', $dormitory);
        $view->with('studentQuota', $studentQuota);
    }
}
<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Requests\Api\Payment\PaymentConfirmationRequest;
use App\Modules\Payment\PaymentTransactionConfirmator;
use App\Modules\Payment\PaymentTransactionService;
use App\Transformers\Payment\PaymentTransactionTransformer;
use Illuminate\Http\Request;

class PaymentController
{
    protected $paymentTransactionService;

    protected $paymentTransactionConfirmator;

    public function __construct(PaymentTransactionService $paymentTransactionService, PaymentTransactionConfirmator $paymentTransactionConfirmator)
    {
        $this->paymentTransactionService = $paymentTransactionService;
        $this->paymentTransactionConfirmator = $paymentTransactionConfirmator;
    }

    public function getAll(Request $request)
    {
        $res = $this->paymentTransactionService->getAll($request->all(), $request->user());

        return fractal($res, new PaymentTransactionTransformer())->respond();
    }

    public function getByCode(Request $request, $code)
    {
        $res = $this->paymentTransactionService->getByCode($code, $request->all(), $request->user());

        return fractal($res, new PaymentTransactionTransformer())->respond();
    }

    public function confirmation(PaymentConfirmationRequest $request)
    {
        $data = $request->all();

        $this->paymentTransactionConfirmator->confirmation($data);
    }
}
<?php

namespace App\Http\Controllers\Api\General;

use App\Http\Controllers\Controller;
use App\Modules\Generals\GeneralService;
use App\Transformers\General\FacilityTransformer;
use App\Transformers\General\GradeClassTransformer;
use App\Transformers\General\GradeTransformer;
use App\Transformers\General\MajorTransformer;
use App\Transformers\General\MarketerTransformer;
use App\Transformers\General\SchoolFeeTransformer;
use App\Transformers\General\SchoolPeriodTransformer;
use App\Transformers\General\StudentCategoryTransformer;
use App\Transformers\General\StudentTypeTransformer;
use App\Transformers\General\SubjectTransformer;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    protected $generalService;

    public function __construct(GeneralService $generalService)
    {
        $this->generalService = $generalService;
    }

    public function schoolPeriod()
    {
        $res = $this->generalService->getSchoolPeriod();

        return fractal($res, new SchoolPeriodTransformer())->respond();
    }

    public function schoolFee()
    {
        $res = $this->generalService->getSchoolFee();

        return fractal($res, new SchoolFeeTransformer())->respond();
    }

    public function facility()
    {
        $res = $this->generalService->getFacility();

        return fractal($res, new FacilityTransformer())->respond();
    }

    public function grade()
    {
        $res = $this->generalService->getGrade();

        return fractal($res, new GradeTransformer())->respond();
    }

    public function gradeClasses($gradeId)
    {
        $res = $this->generalService->getGradeClass($gradeId);

        return fractal($res, new GradeClassTransformer())->respond();
    }

    public function major()
    {
        $res = $this->generalService->getMajor();

        return fractal($res, new MajorTransformer())->respond();
    }

    public function marketer()
    {
        $res = $this->generalService->getMarketer();

        return fractal($res, new MarketerTransformer())->respond();
    }

    public function studentCategory()
    {
        $res = $this->generalService->getStudentCategory();

        return fractal($res, new StudentCategoryTransformer())->respond();
    }

    public function studentType()
    {
        $res = $this->generalService->getStudentType();

        return fractal($res, new StudentTypeTransformer())->respond();
    }

    public function subject()
    {
        $res = $this->generalService->getSubject();

        return fractal($res, new SubjectTransformer())->respond();
    }
}
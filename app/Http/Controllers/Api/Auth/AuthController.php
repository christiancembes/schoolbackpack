<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Modules\Auth\AuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function register(RegisterRequest $request)
    {
        $validatedData = $request->all();
        
        return $this->authService->register($validatedData, $request);
    }

    public function login(LoginRequest $request)
    {
        $loginData = $request->all();

        return $this->authService->login($loginData);
    }

    public function logout()
    {
        if (auth()->check()) {
            auth()->user()->token()->revoke();
        }
    }
}

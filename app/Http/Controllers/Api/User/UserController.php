<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Modules\Student\StudentService;
use App\Transformers\Student\StudentTransformer;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $studentService;

    public function __construct(StudentService $studentService)
    {
        $this->studentService = $studentService;
    }

    public function me(Request $request)
    {
        return $request->user();
    }

    public function meStudent(Request $request)
    {
        $res = $this->studentService->getDetailByUser($request->user()->getId());

        return fractal($res, new StudentTransformer())->respond();
    }
}
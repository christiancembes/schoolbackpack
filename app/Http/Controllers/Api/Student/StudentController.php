<?php

namespace App\Http\Controllers\Api\Student;

use App\Http\Controllers\Controller;
use App\Modules\Student\StudentService;
use App\Transformers\Student\StudentTransformer;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    protected $studentService;

    public function __construct(StudentService $studentService)
    {
        $this->studentService = $studentService;
    }

    public function getDetail(Request $request, $id)
    {
        $res = $this->studentService->getDetail($id);

        return fractal($res, new StudentTransformer())->respond();
    }
}
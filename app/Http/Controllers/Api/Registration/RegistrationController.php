<?php

namespace App\Http\Controllers\Api\Registration;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Registration\PreRegistration1Request;
use App\Http\Requests\Api\Registration\PreRegistration2Request;
use App\Http\Requests\Api\Registration\PreRegistration3Request;
use App\Http\Requests\Api\Registration\PreRegistration5Request;
use App\Http\Requests\Api\Registration\RegistrationRequest;
use App\Modules\Registration\RegistrationService;
use App\Transformers\Payment\PaymentTransactionTransformer;
use App\Transformers\Student\StudentTransformer;
use Illuminate\Http\Request;
use Spatie\Fractal\Fractal;

class RegistrationController extends Controller
{
    protected $registrationService;

    public function __construct(RegistrationService $registrationService)
    {
        $this->registrationService = $registrationService;
    }

    public function preRegister1(PreRegistration1Request $request)
    {
        return true;
    }

    public function preRegister2(PreRegistration2Request $request)
    {
        return true;
    }

    public function preRegister3(PreRegistration3Request $request)
    {
        return true;
    }

    public function preRegister5(PreRegistration5Request $request)
    {
        return true;
    }

    public function register(RegistrationRequest $request)
    {
        $data = $request->all();

        $res = $this->registrationService->register($data);

        return fractal($res, new PaymentTransactionTransformer())->respond();
    }
}
<?php

namespace App\Http\Controllers\Api\Quotation;

use App\Http\Controllers\Controller;
use App\Modules\Quotation\QuotationService;
use App\Transformers\Quotation\QuotationTransformer;
use Illuminate\Http\Request;

class QuotationController extends Controller
{
    protected $quotationService;

    public function __construct(QuotationService $quotationService)
    {
        $this->quotationService = $quotationService;
    }

    public function getAll(Request $request)
    {
        $res = $this->quotationService->getAll($request->all(), $request->user());

        return fractal($res, new QuotationTransformer())->respond();
    }

    public function getByCode(Request $request, $code)
    {
        $res = $this->quotationService->getByCode($code, $request->all(), $request->user());

        return fractal($res, new QuotationTransformer())->respond();
    }
}
<?php

namespace App\Http\Controllers\Api\Location;

use App\Http\Controllers\Controller;
use App\Modules\Location\LocationService;
use App\Transformers\Location\CityTransformer;
use App\Transformers\Location\CountryTransformer;
use App\Transformers\Location\DistrictTransformer;
use App\Transformers\Location\ProvinceTransformer;
use App\Transformers\Location\VillageTransformer;

class LocationController extends Controller
{
    protected $locationService;

    public function __construct(LocationService $locationService)
    {
        $this->locationService = $locationService;
    }

    public function country()
    {
        $res = $this->locationService->getCountry();

        return fractal($res, new CountryTransformer())->respond();
    }

    public function province()
    {
        $res = $this->locationService->getProvince();

        return fractal($res, new ProvinceTransformer())->respond();
    }

    public function city($provinceId)
    {
        $res = $this->locationService->getCity($provinceId);

        return fractal($res, new CityTransformer())->respond();
    }

    public function district($cityId)
    {
        $res = $this->locationService->getDistrict($cityId);

        return fractal($res, new DistrictTransformer())->respond();
    }

    public function village($districtId)
    {
        $res = $this->locationService->getVillage($districtId);

        return fractal($res, new VillageTransformer())->respond();
    }
}
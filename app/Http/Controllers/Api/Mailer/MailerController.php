<?php

namespace App\Http\Controllers\Api\Mailer;

use App\Http\Controllers\Controller;
use App\Notifications\WelcomeEmail;
use Illuminate\Http\Request;

class MailerController extends Controller
{
    public function send(Request $request)
    {
        $data = $request->all();
        $user = $request->user();

        if ($data['type'] == 'welcome-email') {
            $mailData = $data['data'];
            $user->notify(new WelcomeEmail($user, $mailData));
        }
    }
}
<?php

namespace App\Http\Controllers\Api\Report;

use App\Http\Controllers\Controller;
use App\Modules\Student\StudentReportService;
use App\Transformers\Dormitory\TermReportTransformer;
use App\Transformers\Dormitory\MonthlyReportTransformer;
use App\Transformers\Student\StudentReportTransformer;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    protected $studentReportService;

    public function __construct(StudentReportService $studentReportService)
    {
        $this->studentReportService = $studentReportService;
    }

    public function getAll(Request $request)
    {
        $res = $this->studentReportService->getAll($request->all(), $request->user());

        return fractal($res, new StudentReportTransformer())->respond();
    }

    public function getById(Request $request, $id)
    {
        $res = $this->studentReportService->getById($id, $request->all(), $request->user());

        return fractal($res, new StudentReportTransformer())->respond();
    }

    public function getDormitoryMonthlyAll(Request $request)
    {
        $res = $this->studentReportService->getDormitoryMonthlyAll($request->all(), $request->user());

        return fractal($res, new MonthlyReportTransformer())->respond();
    }

    public function getDormitoryMonthlyById(Request $request, $id)
    {
        $res = $this->studentReportService->getDormitoryMonthlyById($id, $request->all(), $request->user());

        return fractal($res, new MonthlyReportTransformer())->respond();
    }

    public function getDormitoryTermAll(Request $request)
    {
        $res = $this->studentReportService->getDormitoryTermAll($request->all(), $request->user());

        return fractal($res, new TermReportTransformer())->respond();
    }

    public function getDormitoryTermById(Request $request, $id)
    {
        $res = $this->studentReportService->getDormitoryTermById($id, $request->all(), $request->user());

        return fractal($res, new TermReportTransformer())->respond();
    }


}
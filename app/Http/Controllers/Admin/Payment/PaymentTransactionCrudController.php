<?php

namespace App\Http\Controllers\Admin\Payment;

use App\Http\Controllers\Admin\Payment\Traits\PaymentTransactionFilter;
use App\Http\Requests\Admin\Payment\PaymentTransactionRequest;
use App\Modules\Payment\Models\PaymentTransaction;
use App\Modules\Payment\PaymentTransactionConfirmator;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class PaymentTransactionCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PaymentTransactionCrudController extends CrudController
{
    use ListOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use PaymentTransactionFilter;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(PaymentTransaction::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/payment/payment-transaction');
        CRUD::setEntityNameStrings('Daftar Pembayaran', 'Daftar Pembayaran');
    }

    public function confirmation(Request $request, $id, PaymentTransactionConfirmator $paymentTransactionConfirmator)
    {
        return $paymentTransactionConfirmator->cmsConfirmation($id);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->filter($this->crud);

        $this->crud->column('code')->label('Kode Pembayaran');
        $this->crud->column('description')->label('Deskripsi');
        $this->crud->column('total_amount')->label('Total Pembayaran')->prefix('Rp. ')->type('number');
        $this->crud->addColumn([
            'name' => 'status',
            'type' => 'model_function',
            'function_name' => 'getStatusStr'
        ]);
        $this->crud->column('completed_at')->label('Tanggal Selesai');
        $this->crud->column('student.full_name')->label('Siswa');
        $this->crud->column('payment_method.name')->label('Metode Pembayaran');
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->crud->addButtonFromView('line', 'confirmation', 'payment/confirmation', 'end');

        $this->crud->column('code')->label('Kode Pembayaran');
        $this->crud->column('description')->label('Deskripsi');
        $this->crud->column('total_amount')->label('Total Pembayaran')->prefix('Rp. ')->type('number');
        $this->crud->addColumn([
            'name' => 'status',
            'type' => 'model_function',
            'function_name' => 'getStatusStr'
        ]);
        $this->crud->column('completed_at')->label('Tanggal Selesai');
        $this->crud->column('student.name')->label('Siswa');
        $this->crud->column('payment_method.name')->label('Metode Pembayaran');

        $this->crud->column('confirmation.sender_name')->label('Nama Pengirim');
        $this->crud->column('confirmation.sender_bank_account')->label('No Rekening Pengirim');
        $this->crud->column('confirmation.amount')->label('Jumlah Transfer')->prefix('Rp. ')->type('number');
        $this->crud->column('confirmation.transfer_at')->label('Tanggal Transfer');
        $this->crud->addColumn([
            'name' => 'confirmation.proof_of_payment',
            'type' => 'image',
            'label' => 'Bukti Transfer'
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PaymentTransactionRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }
}

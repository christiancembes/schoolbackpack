<?php

namespace App\Http\Controllers\Admin\Payment;

use App\Http\Requests\Admin\Payment\PaymentConfirmationRequest;
use App\Modules\Payment\Models\PaymentConfirmation;
use App\Modules\Payment\PaymentTransactionConfirmator;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class PaymentConfirmationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PaymentConfirmationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    // use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(PaymentConfirmation::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/payment/payment-confirmation');
        CRUD::setEntityNameStrings('Payment Confirmation', 'Payment Confirmations');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    protected function setupShowOperation()
    {
        $this->crud->addButtonFromView('line', 'confirmation', 'payment/confirmation', 'end');

        $this->crud->setShowContentClass('col-md-12');

        $this->crud->set('show.setFromDb', true);

        $this->crud->addColumn([
            'name' => 'proof_of_payment',
            'type' => 'image'
        ]);
        $this->crud->column('code');
        $this->crud->column('amount');
        $this->crud->column('sender_name');
        $this->crud->column('sender_bank_account');
        $this->crud->column('transfer_at');
        $this->crud->column('completed_at');
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PaymentConfirmationRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function confirmation(Request $request, $id, PaymentTransactionConfirmator $paymentTransactionConfirmator)
    {
        return $paymentTransactionConfirmator->cmsConfirmation($id);
    }
}

<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Requests\MarketerPayoutRequest;
use App\Modules\Generals\Models\Marketer;
use App\Modules\Generals\Models\MarketerPayout;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class MarketerPayoutCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class MarketerPayoutCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(MarketerPayout::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/master/marketer-payout');
        CRUD::setEntityNameStrings('Request Payout', 'Request Payout');

        if (auth()->user()->hasRole('super-admin')) {
            $this->crud->allowAccess(['list', 'update', 'create', 'delete']);
        } else {
            if (auth()->user()->can('marketer')) {
                $marketer = Marketer::where('user_id', auth()->user()->getId())->firstOrFail();
                $this->crud->allowAccess(['list']);
                $this->crud->denyAccess(['update', 'create', 'delete']);
                $this->crud->addClause('where', 'marketer_id', $marketer->getId());
            }
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->crud->column('amount')->label('Jumlah')->prefix('Rp. ')->type('number');
        $this->crud->column('marketer.name')->label('Nama Marketer');
        $this->crud->addColumn([
            'name' => 'status',
            'type' => 'model_function',
            'function_name' => 'getStatusStr'
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->crud->column('amount')->label('Jumlah')->prefix('Rp. ')->type('number');
        $this->crud->column('marketer.name')->label('Nama Marketer');
        $this->crud->addColumn([
            'name' => 'status',
            'type' => 'model_function',
            'function_name' => 'getStatusStr'
        ]);
        $this->crud->addButtonFromView('line', 'request_payout', 'marketer/confirm-payout', 'begining');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(MarketerPayoutRequest::class);

        CRUD::setFromDb();
    }
}

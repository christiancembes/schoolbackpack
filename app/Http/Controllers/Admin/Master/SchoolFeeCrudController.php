<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Requests\Api\Admin\Master\SchoolFeeRequest;
use App\Modules\Generals\Models\SchoolFee;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class SchoolFeeCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SchoolFeeCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(SchoolFee::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/master/school-fee');
        CRUD::setEntityNameStrings('Biaya Sekolah', 'Biaya Sekolah');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->set('show.setFromDb', false);

        CRUD::column('name')->label('Name');
        CRUD::column('amount')->label('Jumlah');
        CRUD::column('description')->label('Deskripsi');
        CRUD::column('payment_system')->label('Sistem Pembayaran');
        CRUD::column('school_period.name')->label('Periode');
        CRUD::column('grade.name')->label('Tingkat');
        CRUD::column('student_type.name')->label('Tipe Siswa');
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        CRUD::column('name')->label('Name');
        CRUD::column('amount')->label('Jumlah');
        CRUD::column('description')->label('Deskripsi');
        CRUD::column('payment_system')->label('Sistem Pembayaran');
        CRUD::column('school_period.name')->label('Periode');
        CRUD::column('grade.name')->label('Tingkat');
        CRUD::column('student_type.name')->label('Tipe Siswa');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->crud->set('show.setFromDb', false);

        CRUD::field('type')->label('Type');
        CRUD::field('name')->label('Name');
        CRUD::field('amount')->label('Jumlah');
        CRUD::field('description')->label('Deskripsi');
        CRUD::field('payment_system')->label('Sistem Pembayaran');
        CRUD::field('school_period')->label('Periode');
        CRUD::field('grade')->label('Tingkat');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SchoolFeeRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function schoolFeeByUser(Request $request)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $options = SchoolFee::query();

        // if no category has been selected, show no options
        if (!$form['student_id']) {
            return [];
        } else {
            $student = Student::find($form['student_id']);

            if ($student['school_period_id']) {
                $options = $options->where('school_period_id', $student['school_period_id']);
            }

            if ($student['grade_id']) {
                $options = $options->where('grade_id', $student['grade_id']);
            }

            if ($student['student_type_id']) {
                $options = $options->where('student_type_id', $student['student_type_id']);
            }
        }

        if ($search_term) {
            $results = $options->where('name', 'LIKE', '%'.$search_term.'%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        return $results;
    }
}

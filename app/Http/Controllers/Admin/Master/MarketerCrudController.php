<?php

namespace App\Http\Controllers\Admin\Master;

use App\Foundations\Traits\ApiResponser;
use App\Http\Controllers\Admin\Master\Operations\MarketerShowOperation;
use App\Http\Requests\Admin\Master\MarketerConfirmPayout;
use App\Http\Requests\Admin\Master\MarketerRequestPayout;
use App\Http\Requests\Api\Admin\Master\MarketerRequest;
use App\Modules\Generals\MarketerService;
use App\Modules\Generals\Models\Marketer;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Prologue\Alerts\Facades\Alert;

/**
 * Class MarketerCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class MarketerCrudController extends CrudController
{
    use ApiResponser;
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use MarketerShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Marketer::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/master/marketer');
        CRUD::setEntityNameStrings('Marketer', 'Marketer');

        if (auth()->user()->hasRole('super-admin')) {
            $this->crud->allowAccess(['list', 'update', 'create', 'delete']);
        } else {
            if (auth()->user()->can('marketer')) {
                $this->crud->allowAccess(['list']);
                $this->crud->denyAccess(['update', 'create', 'delete']);
                $this->crud->addClause('where', 'user_id', auth()->user()->getId());
            }
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->crud->column('name')->label('Nama');
        $this->crud->column('nip')->label('NIP');
        $this->crud->column('balance')->label('Saldo')->prefix('Rp. ')->type('number');
        $this->crud->column('user.email')->label('Email');
    }

    protected function setupShowOperation()
    {
        $this->crud->setShowContentClass('col-md-12');
        $this->crud->setShowView('crud::marketer.show');
        $this->crud->set('show.setFromDb', false);

        $this->crud->column('name')->label('Nama');
        $this->crud->column('nip')->label('NIP');
        $this->crud->column('balance')->label('Saldo')->prefix('Rp. ')->type('number');
        $this->crud->column('user.email')->label('Email');

        CRUD::addButtonFromView('line', 'request_payout', 'marketer/request-payout', 'begining');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(MarketerRequest::class);

        CRUD::setFromDb(); // fields
    }

    public function requestPayout(MarketerRequestPayout $request, MarketerService $marketerService)
    {
        $data = $request->all();

        $marketerService->requestPayout($data);

        return $this->successResponse([
            'status' => 'success'
        ]);
    }

    public function confirmPayout(MarketerConfirmPayout $request, $id, MarketerService $marketerService)
    {
        $marketerService->confirmPayout($id);

        return $this->successResponse([
            'status' => 'success'
        ]);
    }
}

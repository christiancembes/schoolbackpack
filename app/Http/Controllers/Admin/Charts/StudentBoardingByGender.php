<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Highcharts\Chart;
use Illuminate\Support\Facades\DB;

class StudentBoardingByGender extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        $students = Student::select('gender', DB::raw('count(*) as total'))->where('student_type_id', 2)->whereActive()->groupBy('gender')->get();

        $values = [];
        $labels = [];

        foreach ($students as $student) {
            array_push($values, $student->total);
            
            if ($student->gender === 'L')
                array_push($labels, 'Laki-Laki');
            elseif ($student->gender === 'P')
                array_push($labels, 'Perempuan');
        }

        $this->chart->dataset('Siswa Boarding L/P', 'pie', $values)
            ->color([
                'rgb(70, 127, 208)',
                'rgb(66, 186, 150)',
            ]);

        // OPTIONAL
        $this->chart->displayAxes(false);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($labels);
    }
}
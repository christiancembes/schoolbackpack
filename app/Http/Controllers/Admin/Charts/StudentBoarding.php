<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Highcharts\Chart;
use Illuminate\Support\Facades\DB;

class StudentBoarding extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        $students = Student::select('student_type_id', DB::raw('count(*) as total'))->whereActive()->groupBy('student_type_id')->get();

        $values = [];
        $labels = [];

        foreach ($students as $student) {
            array_push($values, $student->total);

            if ($student->student_type_id === 1)
                array_push($labels, 'Fullday');
            elseif ($student->student_type_id === 2)
                array_push($labels, 'Boarding');
        }

        $this->chart->dataset('Siswa Fullday/Boarding', 'pie', $values)
            ->color([
                'rgb(70, 127, 208)',
                'rgb(66, 186, 150)',
            ]);

        // OPTIONAL
        $this->chart->displayAxes(false);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels($labels);
    }
}
<?php

namespace App\Http\Controllers\Admin\Quotation\Operations;

use App\Foundations\Traits\CodeGenerator;
use App\Modules\Generals\Models\SchoolFee;
use App\Modules\Quotation\Models\QuotationDetail;
use App\Modules\Quotation\Models\QuotationSchedule;
use Illuminate\Support\Facades\Route;

trait QuotationCreateOperation
{
    use CodeGenerator;

    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment    Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupCreateRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/create', [
            'as'        => $routeName.'.create',
            'uses'      => $controller.'@create',
            'operation' => 'create',
        ]);

        Route::post($segment, [
            'as'        => $routeName.'.store',
            'uses'      => $controller.'@store',
            'operation' => 'create',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupCreateDefaults()
    {
        $this->crud->allowAccess('create');

        $this->crud->operation('create', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
            $this->crud->setupDefaultSaveActions();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('top', 'create', 'view', 'crud::buttons.create');
        });
    }

    /**
     * Show the form for creating inserting a new row.
     *
     * @return Response
     */
    public function create()
    {
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->crud->getSaveAction();
        $this->data['title'] = $this->crud->getTitle() ?? trans('backpack::crud.add').' '.$this->crud->entity_name;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getCreateView(), $this->data);
    }

    /**
     * Store a newly created resource in the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        $quotationDetails = $request->all()['quotation_details'];
        $quotationDetails = json_decode($quotationDetails, true);

        $totalAmount = 0;

        foreach ($quotationDetails as $quotationDetail) {
            $schoolFee = SchoolFee::find($quotationDetail['school_fee_id']);
            $totalAmount += $schoolFee->amount;
        }

        $quotationData = $this->crud->getStrippedSaveRequest();
        $quotationData['code'] = $this->generateCode(7, 'Q');
        $quotationData['total_amount'] = $totalAmount;
        $quotationData['remaining_amount'] = $totalAmount;
        $quotationData['final_amount'] = $totalAmount;

        if ($quotationData['quotation_schedule'] < 1) {
            $quotationData['quotation_schedule'] = 1;
        }

        $item = $this->crud->create($quotationData);
        $this->data['entry'] = $this->crud->entry = $item;

        if ($quotationData['quotation_schedule']) {
            $totalScheduleAmount = 0;
            $amountPerSchedule = ceil($totalAmount / $quotationData['quotation_schedule']);

            for ($i = 1; $i <= $quotationData['quotation_schedule']; $i++) {
                $totalScheduleAmount += $amountPerSchedule;

                if ($totalScheduleAmount > $totalAmount) {
                    $amountPerSchedule = $amountPerSchedule - ($totalScheduleAmount - $totalAmount);
                }

                QuotationSchedule::create([
                    'code' => $this->generateCode(7, 'QS'),
                    'description' => 'Pembayaran ke ' . $i,
                    'amount' => $amountPerSchedule,
                    'quotation_id' => $item->getKey()
                ]);
            }
        }

        foreach ($quotationDetails as $quotationDetail) {
            $schoolFee = SchoolFee::find($quotationDetail['school_fee_id']);
            $totalAmount += $schoolFee->amount;
            QuotationDetail::create([
                'description' => $schoolFee->name,
                'amount' => $schoolFee->amount,
                'discount' => 0,
                'quotation_id' => $item->getKey(),
                'school_fee_id' => $schoolFee->id
            ]);
        }

        // show a success message
        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}
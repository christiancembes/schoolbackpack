<?php

namespace App\Http\Controllers\Admin\Quotation\Operations;

use App\Modules\Quotation\Models\Quotation;
use App\Modules\Quotation\Models\QuotationDetail;
use App\Modules\Quotation\Models\QuotationSchedule;
use Illuminate\Support\Facades\Route;

trait QuotationUpdateOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $name       Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName  Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupUpdateRoutes($segment, $routeName, $controller)
    {
        Route::get($segment.'/{id}/edit', [
            'as'        => $routeName.'.edit',
            'uses'      => $controller.'@edit',
            'operation' => 'update',
        ]);

        Route::put($segment.'/{id}', [
            'as'        => $routeName.'.update',
            'uses'      => $controller.'@update',
            'operation' => 'update',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupUpdateDefaults()
    {
        $this->crud->allowAccess('update');

        $this->crud->operation('update', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();

            if ($this->crud->getModel()->translationEnabled()) {
                $this->crud->addField([
                    'name' => 'locale',
                    'type' => 'hidden',
                    'value' => request()->input('locale') ?? app()->getLocale(),
                ]);
            }

            $this->crud->setupDefaultSaveActions();
        });

        $this->crud->operation(['list', 'show'], function () {
            $this->crud->addButton('line', 'update', 'view', 'crud::buttons.update', 'end');
        });
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');
        // get entry ID from Request (makes sure its the last ID for nested resources)
        $id = $this->crud->getCurrentEntryId() ?? $id;
        $this->crud->setOperationSetting('fields', $this->crud->getUpdateFields());
        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->crud->getSaveAction();
        $this->data['title'] = $this->crud->getTitle() ?? trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    /**
     * Update the specified resource in the database.
     *
     * @return Response
     */
    public function update()
    {
        $this->crud->hasAccessOrFail('update');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        $quotationData = $request->all();
        $quotationDetails = $quotationData['quotation_details'];

        if (isset($quotationData['quotation_schedules'])) {
            $quotationSchedules = $quotationData['quotation_schedules'];
        }

        $totalAmount = 0;
        $totalDiscount = 0;

        foreach ($quotationDetails as $quotationDetail) {
            $totalAmount += intval($quotationDetail['amount']);
            $totalDiscount += intval($quotationDetail['discount']);

            QuotationDetail::where('id', $quotationDetail['id'])->update([
                'amount' => intval($quotationDetail['amount']),
                'discount' => intval($quotationDetail['discount']),
                'description' => $quotationDetail['description']
            ]);
        }

        if (isset($quotationData['quotation_schedules'])) {
            foreach ($quotationSchedules as $key => $quotationSchedule) {
                QuotationSchedule::where('id', $key)->update([
                    'amount' => $quotationSchedule
                ]);
            }
        }

        $quotation = Quotation::where('id', $quotationData['id'])->update([
            'description' => $quotationData['description'],
            'total_amount' => $totalAmount,
            'total_discount' => $totalDiscount,
            'final_amount' => $totalAmount - $totalDiscount,
            'remaining_amount' => $totalAmount - $totalDiscount
        ]);

        $item = Quotation::find($quotationData['id']);
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }
}
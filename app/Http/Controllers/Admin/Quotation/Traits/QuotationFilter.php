<?php

namespace App\Http\Controllers\Admin\Quotation\Traits;

trait QuotationFilter
{
    public function filter($crud)
    {
        $crud->addFilter([
            'name'  => 'code',
            'type'  => 'text',
            'label' => 'Kode Tagihan'
        ], false, function ($value) use ($crud) { // if the filter is active
            $crud->addClause('where', 'code', 'LIKE', "%$value%");
        });

        $crud->addFilter([
            'name'  => 'total_amount',
            'type'  => 'text',
            'label' => 'Total Pembayaran'
        ], false, function ($value) use ($crud) { // if the filter is active
            $crud->addClause('where', 'total_amount', 'LIKE', "%$value%");
        });

        $crud->addFilter([
            'name'  => 'full_name',
            'type'  => 'text',
            'label' => 'Nama'
        ], false, function ($value) use ($crud) { // if the filter is active
            $this->crud->query->whereHas('student', function($query) use ($value) {
                $query->where('full_name', 'LIKE', "%$value%");
            });
        });
    }
}
<?php

namespace App\Http\Controllers\Admin\Quotation;

use App\Http\Controllers\Admin\Quotation\Operations\QuotationCreateOperation;
use App\Http\Controllers\Admin\Quotation\Operations\QuotationShowOperation;
use App\Http\Controllers\Admin\Quotation\Operations\QuotationUpdateOperation;
use App\Http\Controllers\Admin\Quotation\Traits\QuotationFilter;
use App\Http\Requests\Admin\Quotation\QuotationRequest;
use App\Http\Requests\Admin\Quotation\QuotationUpdateRequest;
use App\Modules\Generals\Models\SchoolFee;
use App\Modules\Quotation\Models\Quotation;
use App\Modules\Quotation\Models\QuotationDetail;
use App\Modules\Quotation\Models\QuotationSchedule;
use App\Modules\Quotation\QuotationService;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class QuotationCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class QuotationCrudController extends CrudController
{
    use ListOperation;
    use DeleteOperation;
    use QuotationCreateOperation;
    use QuotationUpdateOperation;
    use QuotationShowOperation;
    use QuotationFilter;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Quotation::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/quotation/quotation');
        CRUD::setEntityNameStrings('Daftar Tagihan', 'Daftar Tagihan');

//        $this->crud->setCreateView('crud::quotation.create');
//        $this->crud->setEditView('crud::quotation.edit');
    }

    public function send(Request $request, $id, QuotationService $quotationService)
    {
        return $quotationService->sendQuotation($id);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::with(['student']);

        $this->filter($this->crud);

        CRUD::column('code')->label('Quotation Code');
        CRUD::column('description')->label('Keterangan');
        CRUD::column('total_amount')->label('Total')->prefix('Rp. ')->type('number');
        CRUD::column('total_discount')->label('Total Diskon')->prefix('Rp. ')->type('number');
        CRUD::column('remaining_amount')->label('Sisa Pembayaran')->prefix('Rp. ')->type('number');
        CRUD::column('completed_at')->label('Tanggal Selesai Pembayaran');
        CRUD::column('has_revised')->label('Revisi Ke');
        CRUD::column('student.full_name')->label('Siswa');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    protected function setupShowOperation()
    {
        $this->crud->setShowContentClass('col-md-12');
        $this->crud->setShowView('crud::quotation.show');
        $this->crud->set('show.setFromDb', false);

        CRUD::column('code')->label('Quotation Code');
        CRUD::column('description')->label('Keterangan');
        CRUD::column('total_amount')->label('Total')->prefix('Rp. ')->type('number');
        CRUD::column('total_discount')->label('Total Diskon')->prefix('Rp. ')->type('number');
        CRUD::column('remaining_amount')->label('Sisa Pembayaran')->prefix('Rp. ')->type('number');
        CRUD::column('completed_at')->label('Tanggal Selesai Pembayaran');
        CRUD::column('has_revised')->label('Revisi Ke');
        CRUD::column('student.full_name')->label('Siswa');

        CRUD::addButtonFromView('line', 'send_quotation', 'quotation/send-quotation', 'begining');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setOperationSetting('contentClass', 'col-md-12 bold-labels');
        CRUD::setValidation(QuotationRequest::class);

        $this->crud->field('description')->label('Deskripsi');
        $this->crud->addField([
            'label' => "Siswa",
            'type' => "select2_from_ajax",
            'name' => 'student_id',
            'attribute' => 'full_name',
            'model' => Student::class,
            'data_source' => backpack_url('student/api/student'),
            'placeholder' => 'Pilih Siswa',
            'method' => 'POST',
            'minimum_input_length' => 3,
        ]);
        CRUD::addField([
            'name' => "quotation_schedule",
            'label' => 'Jumlah Cicilan (Opsional)',
            'value' => 1
        ]);
        $this->crud->addField([
            'name' => 'quotation_details',
            'label' => 'Detil Tagihan',
            'type' => 'repeatable',
            'fields' => [
                [
                    'label' => "Biaya Sekolah",
                    'type' => "select2_from_ajax",
                    'fake' => true,
                    'name' => 'school_fee_id',
                    'attribute' => 'name',
                    'model' => SchoolFee::class,
                    'data_source' => backpack_url('master/api/school-fee/user'),
                    'minimum_input_length' => 0,
                    'placeholder' => 'Pilih Biaya Sekolah',
                    'dependencies' => ['student'],
                    'method' => 'POST',
                    'include_all_form_fields' => true
                ]
            ],

            'new_item_label' => 'Tambah Detil Tagihan'
        ]);

        $this->crud->setSaveAction('save_and_edit');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setOperationSetting('contentClass', 'col-md-12 bold-labels');
        CRUD::setValidation(QuotationUpdateRequest::class);

        $this->crud->setEditView('crud::quotation.edit');

        CRUD::field('code')->label('Kode Pembayaran')->tab('Tagihan')->attributes(['readonly' => 'readonly']);
        CRUD::field('description')->label('Deskripsi')->tab('Tagihan');
        CRUD::addField([
            'label' => "Siswa",
            'type' => "select2_from_ajax",
            'name' => 'student',
            'attribute' => 'full_name',
            'model' => Student::class,
            'data_source' => backpack_url('student/api/student'),
            'placeholder' => 'Pilih Siswa',
            'method' => 'POST',
            'minimum_input_length' => 3,
            'tab' => 'Tagihan'
        ]);
        CRUD::addField([
            'name' => 'total_amount',
            'tab' => 'Tagihan',
            'prefix' => 'Rp.',
            'attributes' => [
                'readonly' => 'readonly'
            ]
        ]);
        CRUD::addField([
            'name' => 'total_discount',
            'tab' => 'Tagihan',
            'prefix' => 'Rp.',
            'attributes' => [
                'readonly' => 'readonly'
            ]
        ]);
        CRUD::addField([
            'name' => 'remaining_amount',
            'tab' => 'Tagihan',
            'prefix' => 'Rp.',
            'attributes' => [
                'readonly' => 'readonly'
            ]
        ]);

        $quotationSchedules = QuotationSchedule::where('quotation_id', $this->crud->getCurrentEntryId())->get();

        foreach ($quotationSchedules as $key => $quotationSchedule) {
            CRUD::addField([
                'name' => "quotation_schedules[$quotationSchedule->id]",
                'label' => $quotationSchedule->description,
                'prefix' => 'Rp.',
                'value' => $quotationSchedule->amount,
                'tab' => "Detil Cicilan"
            ]);
        }

        $quotationDetails = QuotationDetail::where('quotation_id', $this->crud->getCurrentEntryId())->get();

        foreach ($quotationDetails as $key => $quotationDetail) {
            CRUD::addField([
                'name' => "quotation_details[$key][id]",
                'type' => 'hidden',
                'value' => $quotationDetail->id,
                'tab' => "Detil $quotationDetail->description"
            ]);
            CRUD::addField([
                'name' => "quotation_details[$key][description]",
                'label' => 'Deskripsi',
                'value' => $quotationDetail->description,
                'tab' => "Detil $quotationDetail->description"
            ]);
            CRUD::addField([
                'name' => "quotation_details[$key][amount]",
                'label' => 'Jumlah',
                'prefix' => 'Rp.',
                'type' => 'number',
                'value' => $quotationDetail->amount,
                'tab' => "Detil $quotationDetail->description"
            ]);
            CRUD::addField([
                'name' => "quotation_details[$key][discount]",
                'label' => 'Diskon',
                'prefix' => 'Rp.',
                'type' => 'number',
                'value' => $quotationDetail->discount,
                'tab' => "Detil $quotationDetail->description"
            ]);
        }
    }

}

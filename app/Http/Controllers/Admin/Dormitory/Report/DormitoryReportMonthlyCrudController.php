<?php

namespace App\Http\Controllers\Admin\Dormitory\Report;

use App\Http\Controllers\Admin\Dormitory\Report\Operations\MonthlyReportCreateOperation;
use App\Http\Controllers\Admin\Dormitory\Report\Operations\MonthlyReportShowOperation;
use App\Http\Controllers\Admin\Dormitory\Report\Operations\MonthlyReportUpdateOperation;
use App\Http\Requests\Admin\Dormitory\Report\DormitoryReportMonthlyRequest;
use App\Modules\Dormitory\Models\Report\MonthlyReport;
use App\Modules\Dormitory\Models\Report\MonthlyReportScore;
use App\Modules\Generals\Models\DormitorySubject;
use App\Modules\Generals\Models\SchoolPeriod;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DormitoryReportMonthlyCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DormitoryReportMonthlyCrudController extends CrudController
{
    use ListOperation;
    use MonthlyReportCreateOperation;
    use MonthlyReportUpdateOperation;
    use DeleteOperation;
    use MonthlyReportShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(MonthlyReport::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/dormitory/report/monthly');
        CRUD::setEntityNameStrings('Laporan Asrama Bulanan', 'Laporan Asrama Bulanan');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->crud->column('student.full_name')->label('Nama Siswa');
        $this->crud->column('schoolPeriod.name')->label('Periode');
        $this->crud->addColumn([
            'name' => 'status',
            'type' => 'model_function',
            'function_name' => 'getMonthStr'
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->setShowContentClass('col-md-12');
        $this->crud->setShowView('crud::dormitory.report.monthly.show');
        $this->crud->set('show.setFromDb', false);

        $this->crud->column('student.full_name')->label('Nama Siswa');
        $this->crud->column('schoolPeriod.name')->label('Periode');
        $this->crud->addColumn([
            'name' => 'status',
            'type' => 'model_function',
            'function_name' => 'getMonthStr'
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation(true);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation($isUpdate = false)
    {
        CRUD::setValidation(DormitoryReportMonthlyRequest::class);

        $this->crud->set('show.setFromDb', false);

        $this->crud->addField([
            'tab' => 'Score Report',
            'label' => "Siswa",
            'type' => "select2_from_ajax",
            'name' => 'student_id',
            'attribute' => 'full_name',
            'model' => Student::class,
            'data_source' => backpack_url('student/api/student/active'),
            'placeholder' => 'Pilih Siswa',
            'method' => 'POST',
            'minimum_input_length' => 3,
        ]);
        $this->crud->addField([
            'tab' => 'Score Report',
            'label' => "Periode",
            'type' => "select2",
            'name' => 'school_period_id',
            'attribute' => 'name',
            'model' => SchoolPeriod::class,
            'placeholder' => 'Pilih Periode',
            'wrapper' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'tab' => 'Score Report',
            'name' => 'month',
            'label' => "Bulan",
            'type' => 'select_from_array',
            'wrapper' => ['class' => 'form-group col-md-6'],
            'options' => [
                1 => 'Januari',
                2 => 'Februari',
                3 => 'Maret',
                4 => 'April',
                5 => 'Mei',
                6 => 'Juni',
                7 => 'Juli',
                8 => 'Agustus',
                9 => 'September',
                10 => 'Oktober',
                11 => 'November',
                12 => 'Desember',
            ],
        ]);

        if ($isUpdate) {
            $reportScores = MonthlyReportScore::where('dormitory_student_monthly_report_id', $this->crud->getCurrentEntryId())->get()->toArray();

            $this->crud->addField([
                'tab' => 'Score Report',
                'name' => 'student_report_monthly_scores',
                'label' => 'Mata Pelajaran',
                'type' => 'repeatable',
                'fields' => [
                    [
                        'label' => "id",
                        'type' => "hidden",
                        'name' => 'id'
                    ],
                    [
                        'label' => "Mata Pelajaran",
                        'type' => "select2",
                        'name' => 'dormitory_subject_id',
                        'attribute' => 'name',
                        'model' => DormitorySubject::class,
                        'placeholder' => 'Pilih Mata Pelajaran',
                        'wrapper' => ['class' => 'form-group col-md-6'],
                    ],
                    [
                        'label' => "Nilai",
                        'type' => "number",
                        'fake' => true,
                        'name' => 'score',
                        'placeholder' => 'Nilai',
                        'wrapper' => ['class' => 'form-group col-md-6'],
                    ]
                ],
                'value' => json_encode($reportScores),

                'new_item_label' => 'Tambah Mata Pelajaran'
            ]);
        }
        else {
            $this->crud->addField([
                'tab' => 'Score Report',
                'name' => 'student_report_monthly_scores',
                'label' => 'Mata Pelajaran',
                'type' => 'repeatable',
                'fields' => [
                    [
                        'label' => "Mata Pelajaran",
                        'type' => "select2",
                        'name' => 'dormitory_subject_id',
                        'attribute' => 'name',
                        'model' => DormitorySubject::class,
                        'placeholder' => 'Pilih Mata Pelajaran',
                        'wrapper' => ['class' => 'form-group col-md-6'],
                    ],
                    [
                        'label' => "Nilai",
                        'type' => "number",
                        'fake' => true,
                        'name' => 'score',
                        'placeholder' => 'Nilai',
                        'wrapper' => ['class' => 'form-group col-md-6'],
                    ]
                ],
                'value' => json_encode($this->getSubjectList()),

                'new_item_label' => 'Tambah Mata Pelajaran'
            ]);
        }

        $this->crud->addField([
            'tab' => 'Assessment Report',
            'name' => 'academic',
            'label' => 'Academic',
            'type' => 'repeatable',
            'init_rows' => 0,
            'min_rows' => 0,
            'max_rows' => 0,
            'fields' => [
                [
                    'label' => "Boarding Academic Activity",
                    'type' => "textarea",
                    'name' => 'boarding_academic_activity',
                ],[
                    'label' => "Memorizing",
                    'type' => "textarea",
                    'name' => 'memorizing',
                ],[
                    'label' => "Note",
                    'type' => "textarea",
                    'name' => 'note',
                ],
            ]
        ]);
        $this->crud->addField([
            'tab' => 'Assessment Report',
            'name' => 'behavior',
            'label' => 'Behavior',
            'type' => 'repeatable',
            'fields' => [
                [
                    'label' => "Worship",
                    'type' => "textarea",
                    'name' => 'worship',
                ],[
                    'label' => "Personality",
                    'type' => "textarea",
                    'name' => 'personality',
                ],[
                    'label' => "Note",
                    'type' => "textarea",
                    'name' => 'note',
                ],
            ]
        ]);
        $this->crud->addField([
            'tab' => 'Assessment Report',
            'name' => 'life_skill',
            'label' => 'Life Skill',
            'type' => 'repeatable',
            'fields' => [
                [
                    'label' => "Ability",
                    'type' => "textarea",
                    'name' => 'ability',
                ],[
                    'label' => "Note",
                    'type' => "textarea",
                    'name' => 'note',
                ],
            ]
        ]);
    }

    protected function getSubjectList()
    {
        $studentReportScores = [];

        $subjects = DormitorySubject::all();

        foreach ($subjects as $subject) {
            array_push($studentReportScores, [
                'dormitory_subject_id' => $subject->getId(),
            ]);
        }

        return $studentReportScores;
    }
}

<?php

namespace App\Http\Controllers\Admin\Dormitory;

use App\Http\Controllers\Admin\Dormitory\Operations\DormitoryShowOperation;
use App\Http\Controllers\Admin\Dormitory\Traits\DormitoryFilter;
use App\Http\Requests\Admin\Dormitory\DormitoryRequest;
use App\Modules\Dormitory\Models\Dormitory;
use App\Modules\Student\Repository\StudentRepository;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class DormitoryCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DormitoryCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use DormitoryShowOperation;
    use DormitoryFilter;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Dormitory::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/dormitory/dormitory');
        CRUD::setEntityNameStrings('Daftar Asrama', 'Daftar Asrama');
    }

    public function getAvailableDormitory(Request $request, StudentRepository $studentRepository)
    {
        $search_term = $request->input('q');
        $form = collect($request->input('form'))->pluck('value', 'name');

        $student = $studentRepository->getById($form['student_id']);

        $options = Dormitory::query();

        if ($search_term) {
            $results = $options->where('type', $student->getGender())->where('remaining_capacity', '>=',
                1)->paginate(1000);
        } else {
            $results = $options->where('type', $student->getGender())->paginate(1000);
        }

        return $results;
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->filter($this->crud);

        CRUD::column('name')->label('Nomor Asrama');
        CRUD::column('capacity')->label('Kapasitas');
        CRUD::column('remaining_capacity')->label('Sisa');
        CRUD::column('building')->label('Gedung');
        CRUD::column('floor')->label('Lantai');
        CRUD::column('type')->label('Tipe');
    }

    protected function setupShowOperation()
    {
        $this->crud->setShowView('crud::dormitory.show');

        CRUD::column('name')->label('Nomor Asrama');
        CRUD::column('capacity')->label('Kapasitas');
        CRUD::column('remaining_capacity')->label('Sisa');
        CRUD::column('building')->label('Gedung');
        CRUD::column('floor')->label('Lantai');
        CRUD::column('type')->label('Tipe');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(DormitoryRequest::class);

        CRUD::field('name')->label('Nomor Asrama');
        CRUD::field('capacity')->label('Kapasitas')->type('number');
        CRUD::field('remaining_capacity')->label('Sisa')->type('number');
        CRUD::field('building')->label('Gedung');
        CRUD::field('floor')->label('Lantai');
        CRUD::addField([
            'name' => 'type',
            'label' => "Tipe",
            'type' => 'select_from_array',
            'options' => ['L' => 'Laki-Laki', 'P' => 'Perempuan'],
            'allows_null' => false,
            'default' => 'one',
        ]);
    }
}

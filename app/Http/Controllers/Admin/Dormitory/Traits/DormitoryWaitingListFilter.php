<?php

namespace App\Http\Controllers\Admin\Dormitory\Traits;

trait DormitoryWaitingListFilter
{
    public function filter($crud)
    {
        $crud->addFilter([
            'name'  => 'full_name',
            'type'  => 'text',
            'label' => 'Nama'
        ], false, function ($value) use ($crud) { // if the filter is active
            $this->crud->query->whereHas('student', function($query) use ($value) {
                $query->where('full_name', 'LIKE', "%$value%");
            });
        });
    }
}
<?php

namespace App\Http\Controllers\Admin\Dormitory\Traits;

trait DormitoryFilter
{
    public function filter($crud)
    {
        $crud->addFilter([
            'name'  => 'name',
            'type'  => 'text',
            'label' => 'Nama'
        ], false, function ($value) use ($crud) { // if the filter is active
            $crud->addClause('where', 'name', 'LIKE', "%$value%");
        });

        $crud->addFilter([
            'name'  => 'type',
            'type'  => 'select2',
            'label' => 'Tipe'
        ], function () {
            return [
                'L' => 'Laki-Laki',
                'P' => 'Perempuan',
            ];
        }, function ($value) use ($crud) { // if the filter is active
            $crud->addClause('where', 'type', $value);
        });
    }
}
<?php

namespace App\Http\Controllers\Admin\Dormitory;

use App\Http\Controllers\Admin\Dormitory\Operations\DormitoryWaitingListUpdateOperation;
use App\Http\Controllers\Admin\Dormitory\Traits\DormitoryWaitingListFilter;
use App\Http\Requests\Admin\Dormitory\DormitoryWaitingListRequest;
use App\Modules\Dormitory\Models\Dormitory;
use App\Modules\Dormitory\Models\DormitoryWaitingList;
use App\Modules\Student\Models\Student;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DormitoryWaitingListCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DormitoryWaitingListCrudController extends CrudController
{
    use ListOperation;
    use DormitoryWaitingListUpdateOperation;
    use DeleteOperation;
    use DormitoryWaitingListFilter;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(DormitoryWaitingList::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/dormitory/dormitory-waiting-list');
        CRUD::setEntityNameStrings('Daftar Tunggu Asrama', 'Daftar Tunggu Asrama');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->setListView('crud::dormitory-waiting.list');

        $this->filter($this->crud);

        $this->crud->column('student.full_name')->label(trans('student.student'));
        $this->crud->column('student.gender')->label(trans('student.gender'));
        $this->crud->addColumn([
            'name' => 'status',
            'type' => 'model_function',
            'function_name' => 'getStatusStr'
        ]);
        $this->crud->addClause('whereNull', 'completed_at');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setOperationSetting('contentClass', 'col-md-8 bold-labels');
        CRUD::setValidation(DormitoryWaitingListRequest::class);

        $this->crud->addField([
            'label' => trans('student.student'),
            'type' => "select2_from_ajax",
            'name' => 'student_id',
            'attribute' => 'full_name',
            'model' => Student::class,
            'data_source' => backpack_url('student/api/student'),
            'placeholder' => 'Pilih Siswa',
            'method' => 'POST',
            'minimum_input_length' => 3,
        ]);
        $this->crud->addField([
            'label' => trans('student.gender'),
            'type' => 'text',
            'name' => 'student.gender',
            'attributes' => [
                'disabled' => 'disabled'
            ]
        ]);
        $this->crud->addField([
            'label' => "Asrama",
            'type' => "select2_from_ajax",
            'name' => 'dormitory_id',
            'attribute' => 'name',
            'model' => Dormitory::class,
            'data_source' => backpack_url('dormitory/api/available-dormitory'),
            'placeholder' => 'Pilih Kamar Asrama',
            'method' => 'POST',
            'include_all_form_fields' => true,
            'minimum_input_length' => 1,
        ]);
    }
}

<?php

namespace App\Http\Controllers\Admin\Student;

use App\Http\Controllers\Admin\Student\Operations\StudentShowOperation;
use App\Http\Controllers\Admin\Student\Traits\UserFilter;
use App\Http\Requests\Api\Admin\Student\StudentRequest;
use App\Modules\Location\Models\City;
use App\Modules\Location\Models\District;
use App\Modules\Location\Models\Province;
use App\Modules\Location\Models\Village;
use App\Modules\Student\Models\Student;
use App\Modules\Student\Models\StudentParent;
use App\Modules\Student\StudentService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class StudentCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class StudentCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use StudentShowOperation;
    use UserFilter;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Student::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/student/student');
        CRUD::setEntityNameStrings('Siswa Aktif', 'Siswa Aktif');
    }

    public function getStudent(Request $request)
    {
        $search_term = $request->input('q');

        $options = Student::query();

        if ($search_term) {
            $results = $options->where('full_name', 'LIKE', '%'.$search_term.'%')->paginate(10);
        } else {
            $results = $options->paginate(10);
        }

        return $results;
    }

    public function getStudentActive(Request $request)
    {
        $search_term = $request->input('q');

        $options = Student::query();

        if ($search_term) {
            $results = $options->whereActive()->where('full_name', 'LIKE', '%'.$search_term.'%')->paginate(10);
        } else {
            $results = $options->whereActive()->paginate(10);
        }

        return $results;
    }

    public function cancelRegistration(Request $request, $id, StudentService $studentService)
    {
        $studentService->cancelRegistration($id);
    }

    public function waitingActivation()
    {
        $this->setupListOperation();

        $this->crud->hasAccessOrFail('list');

        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? mb_ucfirst($this->crud->entity_name_plural);

        return view($this->crud->getListView(), $this->data);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation($status = null)
    {
        $this->crud->set('show.setFromDb', false);

        $this->filter($this->crud);

        $request = $status;

        if (!isset($request['status'])) {
            $this->crud->addClause('where', 'status', 1);
        }

        if (@$request['status'] === 2) {
            $this->crud->denyAccess('delete');
            $this->crud->denyAccess('update');
            $this->crud->addButtonFromView('line', 'confirmation', 'student/cancel-registration', 'beginning');
        }

        $this->crud->column('nis')->label(trans('student.nis'));
        $this->crud->column('nisn')->label(trans('student.nisn'));
        $this->crud->column('full_name')->label(trans('student.full_name'));
        $this->crud->column('gender')->label(trans('student.gender'));
        $this->crud->column('place_of_birth')->label(trans('student.place_of_birth'));
        $this->crud->column('date_of_birth')->label(trans('student.date_of_birth'));
        $this->crud->column('grade.name')->label(trans('student.grade.name'));
        $this->crud->column('gradeClass.name')->label(trans('student.gradeClass.name'));
        $this->crud->column('major.name')->label(trans('student.major.name'));
        $this->crud->column('school_period.name')->label(trans('student.schoolPeriod.name'));
        $this->crud->addColumn([
            'name' => 'status',
            'type' => 'model_function',
            'function_name' => 'getStatusStr'
        ]);
    }

    protected function setupShowOperation()
    {
        $this->crud->setShowContentClass('col-md-12');
        $this->crud->setShowView('crud::student.show');
        $this->crud->set('show.setFromDb', false);

        $this->crud->column('nis')->label(trans('student.nis'));
        $this->crud->column('nisn')->label(trans('student.nisn'));
        $this->crud->column('full_name')->label(trans('student.full_name'));
        $this->crud->column('name')->label(trans('student.name'));
        $this->crud->column('gender')->label(trans('student.gender'));
        $this->crud->column('place_of_birth')->label(trans('student.place_of_birth'));
        $this->crud->column('date_of_birth')->label(trans('student.date_of_birth'));
        $this->crud->column('goal')->label(trans('student.goal'));
        $this->crud->column('hobby')->label(trans('student.hobby'));
        $this->crud->column('nik')->label(trans('student.nik'));
        $this->crud->column('order_in_family')->label(trans('student.order_in_family'));
        $this->crud->column('numbers_of_siblings')->label(trans('student.numbers_of_siblings'));
        $this->crud->column('numbers_of_step_families')->label(trans('student.numbers_of_step_families'));
        $this->crud->column('numbers_of_adopted_families')->label(trans('student.numbers_of_adopted_families'));
        $this->crud->column('weight')->label(trans('student.weight'));
        $this->crud->column('height')->label(trans('student.height'));
        $this->crud->column('blood_type')->label(trans('student.blood_type'));
        $this->crud->column('mother_language')->label(trans('student.mother_language'));
        $this->crud->column('name_of_sibling')->label(trans('student.name_of_sibling'));
        $this->crud->column('gradeOfSibling.name')->label(trans('student.gradeOfSibling.name'));
        $this->crud->addColumn([
            'name' => 'status',
            'type' => 'model_function',
            'function_name' => 'getStatusStr'
        ]);
        $this->crud->column('user.name')->label(trans('student.user.name'));
        $this->crud->column('country.name')->label(trans('student.country.name'));
        $this->crud->column('studentCategory.name')->label(trans('student.studentCategory.name'));
        $this->crud->column('studentType.name')->label(trans('student.studentType.name'));
        $this->crud->column('grade.name')->label(trans('student.grade.name'));
        $this->crud->column('gradeClass.name')->label(trans('student.gradeClass.name'));
        $this->crud->column('facility.name')->label(trans('student.facility.name'));
        $this->crud->column('major.name')->label(trans('student.major.name'));
        $this->crud->column('marketer.name')->label(trans('student.marketer.name'));
        $this->crud->column('school_period.name')->label(trans('student.schoolPeriod.name'));
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(StudentRequest::class);

        $this->crud->set('show.setFromDb', false);

        $this->crud->field('nis')->label(trans('student.nis'))->tab('Siswa');
        $this->crud->field('nisn')->label(trans('student.nisn'))->tab('Siswa');
        $this->crud->field('full_name')->label(trans('student.full_name'))->tab('Siswa');
        $this->crud->field('name')->label(trans('student.name'))->tab('Siswa');
        $this->crud->field('gender')->label(trans('student.gender'))->tab('Siswa');
        $this->crud->field('place_of_birth')->label(trans('student.place_of_birth'))->tab('Siswa');
        $this->crud->field('date_of_birth')->label(trans('student.date_of_birth'))->tab('Siswa');
        $this->crud->field('goal')->label(trans('student.goal'))->tab('Siswa');
        $this->crud->field('hobby')->label(trans('student.hobby'))->tab('Siswa');
        $this->crud->field('nik')->label(trans('student.nik'))->tab('Siswa');
        $this->crud->field('order_in_family')->label(trans('student.order_in_family'))->tab('Siswa');
        $this->crud->field('numbers_of_siblings')->label(trans('student.numbers_of_siblings'))->tab('Siswa');
        $this->crud->field('numbers_of_step_families')->label(trans('student.numbers_of_step_families'))->tab('Siswa');
        $this->crud->field('numbers_of_adopted_families')->label(trans('student.numbers_of_adopted_families'))->tab('Siswa');
        $this->crud->field('weight')->label(trans('student.weight'))->tab('Siswa');
        $this->crud->field('height')->label(trans('student.height'))->tab('Siswa');
        $this->crud->field('blood_type')->label(trans('student.blood_type'))->tab('Siswa');
        $this->crud->field('mother_language')->label(trans('student.mother_language'))->tab('Siswa');
        $this->crud->field('name_of_sibling')->label(trans('student.name_of_sibling'))->tab('Siswa');
        $this->crud->field('grade_of_sibling_id')->label(trans('student.gradeOfSibling.name'))->tab('Siswa');

        $this->crud->addField([
            'name' => 'status',
            'label' => "Status",
            'type' => 'select_from_array',
            'tab' => 'Siswa',
            'options' => [
                0 => 'Tidak Aktif',
                1 => 'Aktif',
                2 => 'Menunggu Pembayaran Pendaftaran',
                3 => 'Menunggu Pembayaran Gedung',
                4 => 'Menunggu Aktivasi Finance',
                5 => 'Batal Pembayaran Registrasi',
                6 => 'Batal Pembayaran Gedung',
                7 => 'Mutasi',
                8 => 'Hilang',
                9 => 'Meninggal',
            ],
        ]);

        $this->crud->field('user_id')->label(trans('student.user.name'))->tab('Siswa');
        $this->crud->field('country_id')->label(trans('student.country.name'))->tab('Siswa');
        $this->crud->field('student_category_id')->label(trans('student.studentCategory.name'))->tab('Siswa');
        $this->crud->field('student_type_id')->label(trans('student.studentType.name'))->tab('Siswa');
        $this->crud->field('grade_id')->label(trans('student.grade.name'))->tab('Siswa');
        $this->crud->field('grade_class_id')->label(trans('student.gradeClass.name'))->tab('Siswa');
        $this->crud->field('facility_id')->label(trans('student.facility.name'))->tab('Siswa');
        $this->crud->field('major_id')->label(trans('student.major.name'))->tab('Siswa');
        $this->crud->field('marketer_id')->label(trans('student.marketer.name'))->tab('Siswa');
        $this->crud->field('school_period_id')->label(trans('student.schoolPeriod.name'))->tab('Siswa');

        $this->crud->addFields($this->addressFields());
        $this->crud->addFields($this->parentFields());
        $this->crud->addFields($this->healthFields());
        $this->crud->addFields($this->prevSchoolFields());
    }

    protected function addressFields()
    {
        return [
            [
                'name' => 'address.address',
                'label' => 'Alamat',
                'tab' => 'Alamat'
            ],
            [
                'name' => 'address.postal_code',
                'label' => 'Kode Pos',
                'tab' => 'Alamat'
            ],
            [
                'name' => 'address.phone',
                'label' => 'No Telp',
                'tab' => 'Alamat'
            ],
            [
                'name' => 'address.live_with',
                'label' => 'Tinggal Bersama',
                'tab' => 'Alamat'
            ],
            [
                'label' => "Provinsi",
                'type' => "select2_from_ajax",
                'name' => 'address.province',
                'attribute' => 'name',
                'model' => Province::class,
                'data_source' => backpack_url('general/api/province'),
                'placeholder' => 'Pilih Provinsi',
                'method' => 'POST',
                'minimum_input_length' => 3,
                'tab' => 'Alamat'
            ],
            [
                'label' => "Kota",
                'type' => "select2_from_ajax",
                'name' => 'address.city',
                'attribute' => 'name',
                'model' => City::class,
                'data_source' => backpack_url('general/api/city'),
                'placeholder' => 'Pilih Kota',
                'method' => 'POST',
                'minimum_input_length' => 3,
                'tab' => 'Alamat'
            ],
            [
                'label' => "Kecamatan",
                'type' => "select2_from_ajax",
                'name' => 'address.district',
                'attribute' => 'name',
                'model' => District::class,
                'data_source' => backpack_url('general/api/district'),
                'placeholder' => 'Pilih Kecamatan',
                'method' => 'POST',
                'minimum_input_length' => 3,
                'tab' => 'Alamat'
            ],
            [
                'label' => "Desa",
                'type' => "select2_from_ajax",
                'name' => 'address.village',
                'attribute' => 'name',
                'model' => Village::class,
                'data_source' => backpack_url('general/api/village'),
                'placeholder' => 'Pilih Desa',
                'method' => 'POST',
                'minimum_input_length' => 3,
                'tab' => 'Alamat'
            ],
        ];
    }
// batas
    protected function parentFields()
    {
        $parents = StudentParent::where('student_id', $this->crud->getCurrentEntryId())->get();

        $fields = [];

        foreach ($parents as $key => $parent) {
            $_fields = [
                [
                    'name' => "student_parent[$key][name]",
                    'value' => $parent->name,
                    'label' => 'Nama '.ucfirst($parent->type),
                    'tab' => 'Orang Tua'
                ],
                [
                    'name' => "student_parent[$key][place_of_birth]",
                    'value' => $parent->place_of_birth,
                    'label' => 'Tempat Lahir '.ucfirst($parent->type),
                    'tab' => 'Orang Tua'
                ],
                [
                    'name' => "student_parent[$key][date_of_birth]",
                    'value' => $parent->date_of_birth,
                    'label' => 'Tanggal Lahir '.ucfirst($parent->type),
                    'tab' => 'Orang Tua'
                ],
                [
                    'name' => "student_parent[$key][profession]",
                    'value' => $parent->profession,
                    'label' => 'Pekerjaan '.ucfirst($parent->type),
                    'tab' => 'Orang Tua'
                ],
                [
                    'name' => "student_parent[$key][last_education]",
                    'value' => $parent->last_education,
                    'label' => 'Pendidikan Terakhir '.ucfirst($parent->type),
                    'tab' => 'Orang Tua'
                ],
                [
                    'name' => "student_parent[$key][religion]",
                    'value' => $parent->religion,
                    'label' => 'Agama '.ucfirst($parent->type),
                    'tab' => 'Orang Tua'
                ],
                [
                    'name' => "student_parent[$key][mobile]",
                    'value' => $parent->mobile,
                    'label' => 'No Handphone '.ucfirst($parent->type),
                    'tab' => 'Orang Tua'
                ],
                [
                    'name' => "student_parent[$key][email]",
                    'value' => $parent->email,
                    'label' => 'Email '.ucfirst($parent->type),
                    'tab' => 'Orang Tua'
                ],
                [
                    'name' => "student_parent[$key][company]",
                    'value' => $parent->company,
                    'label' => 'Tempat Bekerja '.ucfirst($parent->type),
                    'tab' => 'Orang Tua'
                ],
                [
                    'name' => "student_parent[$key][office_address]",
                    'value' => $parent->office_address,
                    'label' => 'Alamat Tempat Kerja '.ucfirst($parent->type),
                    'tab' => 'Orang Tua'
                ],
                [
                    'name' => "student_parent[$key][income]",
                    'value' => $parent->income,
                    'label' => 'Penghasilan '.ucfirst($parent->type),
                    'tab' => 'Orang Tua'
                ]
            ];

            $fields = array_merge($fields, $_fields);
        }

        return $fields;
    }

    protected function healthFields()
    {
        return [
            [
                'name' => 'health.tbc',
                'label' => 'TBC',
                'tab' => 'Kesehatan'
            ],
            [
                'name' => 'health.malaria',
                'label' => 'Malaria',
                'tab' => 'Kesehatan'
            ],
            [
                'name' => 'health.chotipa',
                'label' => 'Chotipa',
                'tab' => 'Kesehatan'
            ],
            [
                'name' => 'health.cacar',
                'label' => 'Cacar',
                'tab' => 'Kesehatan'
            ],
            [
                'name' => 'health.others',
                'label' => 'Lain-Lain',
                'tab' => 'Kesehatan'
            ],
            [
                'name' => 'health.physical_abnormalities',
                'label' => 'Kelainan Jasmani',
                'tab' => 'Kesehatan'
            ]
        ];
    }

    protected function prevSchoolFields()
    {
        return [
            [
                'name' => 'previous_education.school_origin',
                'label' => 'Sekolah Asal',
                'tab' => 'Sekolah Asal'
            ],
            [
                'name' => 'previous_education.school_address',
                'label' => 'Alamat Sekolah Asal',
                'tab' => 'Sekolah Asal'
            ],
            [
                'name' => 'previous_education.phone',
                'label' => 'Telp Sekolah Asal',
                'tab' => 'Sekolah Asal'
            ],
            [
                'name' => 'previous_education.head_master_name',
                'label' => 'Kepala Sekolah Sekolah Asal',
                'tab' => 'Sekolah Asal'
            ],
            [
                'name' => 'previous_education.nisn',
                'label' => 'NISN',
                'tab' => 'Sekolah Asal'
            ],
            [
                'name' => 'previous_education.school_status',
                'label' => 'Status Sekolah',
                'tab' => 'Sekolah Asal'
            ],
            [
                'name' => 'previous_education.education_system',
                'label' => 'Sistem Pembelajaran',
                'tab' => 'Sekolah Asal'
            ],
            [
                'name' => 'previous_education.period',
                'label' => 'Lama Sekolah',
                'tab' => 'Sekolah Asal'
            ],
            [
                'label' => "Provinsi",
                'type' => "select2_from_ajax",
                'name' => 'previous_education.province',
                'attribute' => 'name',
                'model' => Province::class,
                'data_source' => backpack_url('general/api/province'),
                'placeholder' => 'Pilih Provinsi',
                'method' => 'POST',
                'minimum_input_length' => 3,
                'tab' => 'Sekolah Asal'
            ],
            [
                'label' => "Kota",
                'type' => "select2_from_ajax",
                'name' => 'previous_education.city',
                'attribute' => 'name',
                'model' => City::class,
                'data_source' => backpack_url('general/api/city'),
                'placeholder' => 'Pilih Kota',
                'method' => 'POST',
                'minimum_input_length' => 3,
                'tab' => 'Sekolah Asal'
            ],
            [
                'label' => "Kecamatan",
                'type' => "select2_from_ajax",
                'name' => 'previous_education.district',
                'attribute' => 'name',
                'model' => District::class,
                'data_source' => backpack_url('general/api/district'),
                'placeholder' => 'Pilih Kecamatan',
                'method' => 'POST',
                'minimum_input_length' => 3,
                'tab' => 'Sekolah Asal'
            ],
            [
                'label' => "Desa",
                'type' => "select2_from_ajax",
                'name' => 'previous_education.village',
                'attribute' => 'name',
                'model' => Village::class,
                'data_source' => backpack_url('general/api/village'),
                'placeholder' => 'Pilih Desa',
                'method' => 'POST',
                'minimum_input_length' => 3,
                'tab' => 'Sekolah Asal'
            ]
        ];
    }
}

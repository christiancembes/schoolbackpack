<?php

namespace App\Http\Controllers\Admin\Student\Traits;

use App\Modules\Generals\Models\Grade;
use App\Modules\Generals\Models\SchoolPeriod;

trait ReportFilter
{
    public function filter($crud)
    {
        $schoolPeriodOptions = SchoolPeriod::pluck('name', 'id')->toArray();

        $crud->addFilter([
            'name'  => 'full_name',
            'type'  => 'text',
            'label' => 'Nama'
        ], false, function ($value) use ($crud) { // if the filter is active
            $this->crud->query->whereHas('student', function($query) use ($value) {
                $query->where('full_name', 'LIKE', "%$value%");
            });
        });

        $crud->addFilter([
            'name'  => 'school_period',
            'type'  => 'select2',
            'label' => 'Periode'
        ], function () use ($schoolPeriodOptions) {
            return $schoolPeriodOptions;
        }, function ($value) use ($crud) { // if the filter is active
            $crud->addClause('where', 'school_period_id', $value);
        });
    }
}
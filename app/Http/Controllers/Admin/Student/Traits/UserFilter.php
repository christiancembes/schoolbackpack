<?php

namespace App\Http\Controllers\Admin\Student\Traits;

use App\Modules\Generals\Models\Grade;
use App\Modules\Generals\Models\SchoolPeriod;

trait UserFilter
{
    public function filter($crud)
    {
        $gradeOptions = Grade::pluck('name', 'id')->toArray();

        $schoolPeriodOptions = SchoolPeriod::pluck('name', 'id')->toArray();

        $crud->addFilter([
            'name'  => 'nis',
            'type'  => 'text',
            'label' => 'NIS'
        ], false, function ($value) use ($crud) { // if the filter is active
            $crud->addClause('where', 'nis', 'LIKE', "%$value%");
        });

        $crud->addFilter([
            'name'  => 'nisn',
            'type'  => 'text',
            'label' => 'NISN'
        ], false, function ($value) use ($crud) { // if the filter is active
            $crud->addClause('where', 'nisn', 'LIKE', "%$value%");
        });

        $crud->addFilter([
            'name'  => 'full_name',
            'type'  => 'text',
            'label' => 'Nama'
        ], false, function ($value) use ($crud) { // if the filter is active
            $crud->addClause('where', 'full_name', 'LIKE', "%$value%");
        });

        $crud->addFilter([
            'name'  => 'grade',
            'type'  => 'select2',
            'label' => 'Tingkat'
        ], function () use ($gradeOptions) {
            return $gradeOptions;
        }, function ($value) use ($crud) { // if the filter is active
            $crud->addClause('where', 'grade_id', $value);
        });

        $crud->addFilter([
            'name'  => 'status',
            'type'  => 'select2_multiple',
            'label' => 'Status'
        ], function () {
            return [
                0 => 'Tidak Aktif',
                1 => 'Aktif',
                2 => 'Menunggu Pembayaran Pendaftaran',
                3 => 'Menunggu Pembayaran Gedung',
                4 => 'Menunggu Aktivasi Finance',
                5 => 'Batal Pembayaran Registrasi',
                6 => 'Batal Pembayaran Gedung',
                7 => 'Mutasi',
                8 => 'Hilang',
                9 => 'Meninggal',
            ];
        }, function ($values) use ($crud) { // if the filter is active
            foreach (json_decode($values) as $key => $value) {
                $this->crud->addClause('orWhere', 'status', $value);
            }
        });

        $crud->addFilter([
            'name'  => 'school_period',
            'type'  => 'select2',
            'label' => 'Periode'
        ], function () use ($schoolPeriodOptions) {
            return $schoolPeriodOptions;
        }, function ($value) use ($crud) { // if the filter is active
            $crud->addClause('where', 'school_period_id', $value);
        });
    }
}
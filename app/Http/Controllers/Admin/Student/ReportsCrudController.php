<?php

namespace App\Http\Controllers\Admin\Student;

use App\Http\Controllers\Admin\Student\Operations\ReportCreateOperation;
use App\Http\Controllers\Admin\Student\Operations\ReportShowOperation;
use App\Http\Controllers\Admin\Student\Operations\ReportUpdateOperation;
use App\Http\Controllers\Admin\Student\Traits\ReportFilter;
use App\Http\Requests\ReportsRequest;
use App\Modules\Generals\Models\SchoolPeriod;
use App\Modules\Generals\Models\Subject;
use App\Modules\Student\Models\Student;
use App\Modules\Student\Models\StudentReport;
use App\Modules\Student\Models\StudentReportScore;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ReportsCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ReportsCrudController extends CrudController
{
    use ListOperation;
    use ReportCreateOperation;
    use ReportUpdateOperation;
    use DeleteOperation;
    use ReportShowOperation;
    use ReportFilter;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(StudentReport::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/student/reports');
        CRUD::setEntityNameStrings('Report', 'Report');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->filter($this->crud);

        $this->crud->column('student.full_name')->label('Nama');
        $this->crud->column('schoolPeriod.name')->label('Periode');
        $this->crud->column('terms')->label('Terms');
    }

    protected function setupShowOperation()
    {
        $this->crud->setShowContentClass('col-md-12');
        $this->crud->setShowView('crud::report.show');
        $this->crud->set('show.setFromDb', false);

        $this->crud->column('student.full_name')->label('Nama');
        $this->crud->column('schoolPeriod.name')->label('Periode');
        $this->crud->column('terms')->label('Terms');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation(true);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation($isUpdate = false)
    {
        CRUD::setValidation(ReportsRequest::class);

        $this->crud->set('show.setFromDb', false);

        $this->crud->addField([
            'tab' => 'Score Report',
            'label' => "Siswa",
            'type' => "select2_from_ajax",
            'name' => 'student_id',
            'attribute' => 'full_name',
            'model' => Student::class,
            'data_source' => backpack_url('student/api/student/active'),
            'placeholder' => 'Pilih Siswa',
            'method' => 'POST',
            'minimum_input_length' => 3,
        ]);
        $this->crud->addField([
            'tab' => 'Score Report',
            'label' => "Periode",
            'type' => "select2",
            'name' => 'school_period_id',
            'attribute' => 'name',
            'model' => SchoolPeriod::class,
            'placeholder' => 'Pilih Periode',
            'wrapper' => ['class' => 'form-group col-md-6'],
        ]);
        $this->crud->addField([
            'tab' => 'Score Report',
            'label' => "Semester",
            'name' => 'terms',
            'type' => 'select2_from_array',
            'wrapper' => ['class' => 'form-group col-md-6'],
            'options' => [
                1 => 'Semester 1',
                2 => 'Semester 2',
            ],
        ]);

        if ($isUpdate) {
            $reportScores = StudentReportScore::where('student_report_id', $this->crud->getCurrentEntryId())->get()->toArray();

            $this->crud->addField([
                'tab' => 'Score Report',
                'name' => 'student_report_scores',
                'label' => 'Mata Pelajaran',
                'type' => 'repeatable',
                'fields' => [
                    [
                        'label' => "id",
                        'type' => "hidden",
                        'name' => 'id'
                    ],
                    [
                        'label' => "Mata Pelajaran",
                        'type' => "select2",
                        'name' => 'subject_id',
                        'attribute' => 'name',
                        'model' => Subject::class,
                        'placeholder' => 'Pilih Mata Pelajaran',
                        'wrapper' => ['class' => 'form-group col-md-6']
                    ],
                    [
                        'label' => "Nilai",
                        'type' => "number",
                        'fake' => true,
                        'name' => 'score',
                        'placeholder' => 'Nilai Siswa',
                        'wrapper' => ['class' => 'form-group col-md-6']
                    ],
                    [
                        'label' => "Catatan",
                        'type' => "textarea",
                        'fake' => true,
                        'name' => 'notes',
                        'placeholder' => 'Catatan Untuk Siswa'
                    ]
                ],
                'value' => json_encode($reportScores),

                'new_item_label' => 'Tambah Mata Pelajaran'
            ]);
        }
        else {
            $this->crud->addField([
                'tab' => 'Score Report',
                'name' => 'student_report_scores',
                'label' => 'Mata Pelajaran',
                'type' => 'repeatable',
                'fields' => [
                    [
                        'label' => "Mata Pelajaran",
                        'type' => "select2",
                        'name' => 'subject_id',
                        'attribute' => 'name',
                        'model' => Subject::class,
                        'placeholder' => 'Pilih Mata Pelajaran',
                        'wrapper' => ['class' => 'form-group col-md-6'],
                    ],
                    [
                        'label' => "Nilai",
                        'type' => "number",
                        'fake' => true,
                        'name' => 'score',
                        'placeholder' => 'Nilai Siswa',
                        'wrapper' => ['class' => 'form-group col-md-6'],
                    ],
                    [
                        'label' => "Catatan",
                        'type' => "textarea",
                        'fake' => true,
                        'name' => 'notes',
                        'placeholder' => 'Catatan Untuk Siswa'
                    ]
                ],

                'new_item_label' => 'Tambah Mata Pelajaran'
            ]);
        }

        $this->crud->addField([
            'tab' => 'Lain-Lain',
            'name' => 'extracurricular',
            'label' => 'Ekstra Kurikuler',
            'type' => 'table',
            'columns' => [
                'activity'  => 'Kegiatan',
                'value'  => 'Nilai',
                'description' => 'Deskripsi'
            ],
            'max' => 10,
            'min' => 1
        ]);

        $this->crud->addField([
            'tab' => 'Lain-Lain',
            'name' => 'achievement',
            'label' => 'Prestasi',
            'type' => 'table',
            'columns' => [
                'activity'  => 'Jenis Kegiatan',
                'note'  => 'Keterangan'
            ],
            'max' => 10,
            'min' => 1
        ]);

        $this->crud->addField([
            'tab' => 'Lain-Lain',
            'name' => 'absence',
            'label' => 'Ketidakhadiran',
            'type' => 'repeatable',
            'fields' => [
                [
                    'label' => "Sakit",
                    'type' => "number",
                    'fake' => true,
                    'name' => 'sick',
                    'placeholder' => 'Sakit',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],[
                    'label' => "Ijin",
                    'type' => "number",
                    'fake' => true,
                    'name' => 'permit',
                    'placeholder' => 'Ijin',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],[
                    'label' => "Alfa",
                    'type' => "number",
                    'fake' => true,
                    'name' => 'absense}',
                    'placeholder' => 'Alfa',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ]
        ]);

        $this->crud->addField([
            'tab' => 'Lain-Lain',
            'label' => 'Catatan Wali Kelas',
            'name' => 'guardian_note',
            'type' => 'textarea',
        ]);

        $this->crud->addField([
            'tab' => 'Lain-Lain',
            'label' => 'Catatan Orang Tua',
            'name' => 'parent_note',
            'type' => 'textarea',
        ]);
    }

    public function sendByReport($id)
    {

    }
}

<?php

namespace App\Http\Controllers\Admin\Student;

use App\Http\Controllers\Admin\Student\Operations\StudentShowOperation;
use App\Http\Controllers\Admin\Student\Traits\UserFilter;
use App\Http\Requests\Api\Admin\Student\StudentRequest;
use App\Modules\Location\Models\City;
use App\Modules\Location\Models\District;
use App\Modules\Location\Models\Province;
use App\Modules\Location\Models\Village;
use App\Modules\Student\Models\Student;
use App\Modules\Student\Models\StudentParent;
use App\Modules\Student\StudentService;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class StudentCrudController
 *
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class StudentMutationCrudController extends CrudController
{
    use ListOperation;
    use ShowOperation;
    use UserFilter;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(Student::class);
        CRUD::setRoute(config('backpack.base.route_prefix').'/student/mutation');
        CRUD::setEntityNameStrings('Siswa Mutasi', 'Siswa Mutasi');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->filter($this->crud);

        $this->crud->addClause('where', 'status', Student::SCHOOL_MOVE);

        $this->crud->column('nis')->label(trans('student.nis'));
        $this->crud->column('nisn')->label(trans('student.nisn'));
        $this->crud->column('full_name')->label(trans('student.full_name'));
        $this->crud->column('gender')->label(trans('student.gender'));
        $this->crud->column('place_of_birth')->label(trans('student.place_of_birth'));
        $this->crud->column('date_of_birth')->label(trans('student.date_of_birth'));
        $this->crud->column('grade.name')->label(trans('student.grade.name'));
        $this->crud->column('gradeClass.name')->label(trans('student.gradeClass.name'));
        $this->crud->column('major.name')->label(trans('student.major.name'));
        $this->crud->column('school_period.name')->label(trans('student.schoolPeriod.name'));
        $this->crud->addColumn([
            'name' => 'status',
            'type' => 'model_function',
            'function_name' => 'getStatusStr'
        ]);
    }
}

<?php

namespace App\Http\Requests\Admin\Master;

use App\Rules\Admin\Master\MarketerRequestPayoutBalanceValidationRule;
use App\Rules\Admin\Master\MarketerRequestPayoutPendingValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class MarketerRequestPayout extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => ['required', new MarketerRequestPayoutBalanceValidationRule($this->request), new MarketerRequestPayoutPendingValidationRule($this->request)]
        ];
    }

    public function messages()
    {
        return [
            //
        ];
    }
}

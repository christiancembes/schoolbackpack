<?php

namespace App\Http\Requests\Admin\Dormitory\Report;

use App\Http\Requests\Request;
use App\Rules\StudentReportTermScoreRule;
use Illuminate\Foundation\Http\FormRequest;

class DormitoryReportTermRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id' => 'required',
            'school_period_id' => 'required',
            'term' => 'required',
            'student_report_term_scores' => ['required', new StudentReportTermScoreRule]
            // 'name' => 'required|min:5|max:255'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'student_id' => 'Siswa',
            'school_period_id' => 'Periode',
            'term' => 'Semester',
            'student_report_term_scores' => 'Mata Pelajaran'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}

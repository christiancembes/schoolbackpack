<?php

namespace App\Http\Requests\Api\Registration;

use Illuminate\Foundation\Http\FormRequest;

class PreRegistration5Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'previous_education.school_origin' => 'required',
            'previous_education.school_address' => 'required',
            'previous_education.phone' => 'required',
            'previous_education.head_master_name' => 'required',
            'previous_education.nisn' => 'required',
            'previous_education.school_status' => 'required',
            'previous_education.education_system' => 'required',
            'previous_education.period' => 'required',
            'previous_education.province_id' => 'required|integer',
            'previous_education.city_id' => 'required|integer',
            'previous_education.district_id' => 'required|integer',
            'previous_education.village_id' => 'required|integer',
        ];
    }
}

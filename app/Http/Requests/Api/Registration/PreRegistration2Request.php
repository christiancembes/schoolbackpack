<?php

namespace App\Http\Requests\Api\Registration;

use Illuminate\Foundation\Http\FormRequest;

class PreRegistration2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address.address' => 'required',
            'address.postal_code' => 'required',
            'address.phone' => 'required',
            'address.live_with' => 'required',
            'address.province_id' => 'required|integer',
            'address.city_id' => 'required|integer',
            'address.district_id' => 'required|integer',
            'address.village_id' => 'required|integer',
        ];
    }
}

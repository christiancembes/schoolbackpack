<?php

namespace App\Http\Requests\Api\Registration;

use Illuminate\Foundation\Http\FormRequest;

class PreRegistration1Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|required|unique:users',
            'nisn' => 'required|unique:students',
            'full_name' => 'required',
            'name' => 'required',
            'gender' => 'required',
            'place_of_birth' => 'required',
            'date_of_birth' => 'required',
            'goal' => 'required',
            'hobby' => 'required',
            'nik' => 'required',
            'order_in_family' => 'required|integer',
            'numbers_of_siblings' => 'required|integer',
            'numbers_of_step_families' => 'required|integer',
            'numbers_of_adopted_families' => 'required|integer',
            'weight' => 'required|integer',
            'height' => 'required|integer',
            'blood_type' => 'required',
            'mother_language' => 'required',
            'country_id' => 'required|integer',
            'student_category_id' => 'required|integer',
            'student_type_id' => 'required|integer',
            'school_period_id' => 'required|integer',
            'grade_id' => 'required|integer',
            'grade_class_id' => 'required|integer',
        ];
    }
}

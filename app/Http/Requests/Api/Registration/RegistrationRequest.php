<?php

namespace App\Http\Requests\Api\Registration;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|required|unique:users',
            'nisn' => 'required|unique:students|max:20',
            'full_name' => 'required',
            'name' => 'required',
            'gender' => 'required',
            'place_of_birth' => 'required',
            'date_of_birth' => 'required',
            'goal' => 'required',
            'hobby' => 'required',
            'nik' => 'required|max:16',
            'order_in_family' => 'required|integer',
            'numbers_of_siblings' => 'required|integer',
            'numbers_of_step_families' => 'required|integer',
            'numbers_of_adopted_families' => 'required|integer',
            'weight' => 'required|integer',
            'height' => 'required|integer',
            'blood_type' => 'required',
            'mother_language' => 'required',
            'country_id' => 'required|integer',
            'student_category_id' => 'required|integer',
            'student_type_id' => 'required|integer',
            'school_period_id' => 'required|integer',
            'grade_id' => 'required|integer',
            'grade_class_id' => 'required|integer',

            'address.address' => 'required',
            'address.postal_code' => 'required',
            'address.phone' => 'required',
            'address.live_with' => 'required',
            'address.province_id' => 'required|integer',
            'address.city_id' => 'required|integer',
            'address.district_id' => 'required|integer',
            'address.village_id' => 'required|integer',

            'parents.father.type' => 'required_without_all:parents.mother.type,parents.wali.type',
            'parents.father.name' => 'required_without_all:parents.mother.name,parents.wali.name',
            'parents.father.place_of_birth' => 'required_without_all:parents.mother.place_of_birth,parents.wali.place_of_birth',
            'parents.father.date_of_birth' => 'required_without_all:parents.mother.date_of_birth,parents.wali.date_of_birth',
            'parents.father.profession' => 'required_without_all:parents.mother.profession,parents.wali.profession',
            'parents.father.last_education' => 'required_without_all:parents.mother.last_education,parents.wali.last_education',
            'parents.father.religion' => 'required_without_all:parents.mother.religion,parents.wali.religion',
            'parents.father.mobile' => 'required_without_all:parents.mother.mobile,parents.wali.mobile',
            'parents.father.email' => 'required_without_all:parents.mother.email,parents.wali.email',
            'parents.father.company' => 'required_without_all:parents.mother.company,parents.wali.company',
            'parents.father.office_address' => 'required_without_all:parents.mother.office_address,parents.wali.office_address',
            'parents.father.income' => 'required_without_all:parents.mother.income,parents.wali.income',
            'parents.father.country_id' => 'required_without_all:parents.mother.country_id,parents.wali.country_id',

            'parents.mother.type' => 'required_without_all:parents.father.type,parents.wali.type',
            'parents.mother.name' => 'required_without_all:parents.father.name,parents.wali.name',
            'parents.mother.place_of_birth' => 'required_without_all:parents.father.place_of_birth,parents.wali.place_of_birth',
            'parents.mother.date_of_birth' => 'required_without_all:parents.father.date_of_birth,parents.wali.date_of_birth',
            'parents.mother.profession' => 'required_without_all:parents.father.profession,parents.wali.profession',
            'parents.mother.last_education' => 'required_without_all:parents.father.last_education,parents.wali.last_education',
            'parents.mother.religion' => 'required_without_all:parents.father.religion,parents.wali.religion',
            'parents.mother.mobile' => 'required_without_all:parents.father.mobile,parents.wali.mobile',
            'parents.mother.email' => 'required_without_all:parents.father.email,parents.wali.email',
            'parents.mother.company' => 'required_without_all:parents.father.company,parents.wali.company',
            'parents.mother.office_address' => 'required_without_all:parents.father.office_address,parents.wali.office_address',
            'parents.mother.income' => 'required_without_all:parents.father.income,parents.wali.income',
            'parents.mother.country_id' => 'required_without_all:parents.father.country_id,parents.wali.country_id',

            'parents.wali.type' => 'required_without_all:parents.mother.type,parents.father.type',
            'parents.wali.name' => 'required_without_all:parents.mother.name,parents.father.name',
            'parents.wali.place_of_birth' => 'required_without_all:parents.mother.place_of_birth,parents.father.place_of_birth',
            'parents.wali.date_of_birth' => 'required_without_all:parents.mother.date_of_birth,parents.father.date_of_birth',
            'parents.wali.profession' => 'required_without_all:parents.mother.profession,parents.father.profession',
            'parents.wali.last_education' => 'required_without_all:parents.mother.last_education,parents.father.last_education',
            'parents.wali.religion' => 'required_without_all:parents.mother.religion,parents.father.religion',
            'parents.wali.mobile' => 'required_without_all:parents.mother.mobile,parents.father.mobile',
            'parents.wali.email' => 'required_without_all:parents.mother.email,parents.father.email',
            'parents.wali.company' => 'required_without_all:parents.mother.company,parents.father.company',
            'parents.wali.office_address' => 'required_without_all:parents.mother.office_address,parents.father.office_address',
            'parents.wali.income' => 'required_without_all:parents.mother.income,parents.father.income',
            'parents.wali.country_id' => 'required_without_all:parents.mother.country_id,parents.father.country_id',

            'previous_education.school_origin' => 'required',
            'previous_education.school_address' => 'required',
            'previous_education.phone' => 'required',
            'previous_education.head_master_name' => 'required',
            'previous_education.nisn' => 'required',
            'previous_education.school_status' => 'required',
            'previous_education.education_system' => 'required',
            'previous_education.period' => 'required',
            'previous_education.province_id' => 'required|integer',
            'previous_education.city_id' => 'required|integer',
            'previous_education.district_id' => 'required|integer',
            'previous_education.village_id' => 'required|integer',
        ];
    }
}

<?php

namespace App\Exceptions\Student;

use App\Foundations\Traits\ApiResponser;
use Exception;

class StudentNotActive extends Exception
{
    use ApiResponser;

    public function render()
    {
        if (request()->ajax()) {
            return $this->errorResponse('Student Not Active', 403);
        } else {
            abort(403, 'Student Not Active');
        }
    }
}
<?php

namespace App\Exceptions\Dormitory;

use App\Foundations\Traits\ApiResponser;
use Exception;

class DormitoryCapacity extends Exception
{
    use ApiResponser;

    public function render()
    {
        if (request()->ajax()) {
            return $this->errorResponse('Not enough capacity', 403);
        } else {
            abort(403, 'Not enough capacity');
        }
    }
}
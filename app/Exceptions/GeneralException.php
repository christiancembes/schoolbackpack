<?php

namespace App\Exceptions;

use App\Foundations\Traits\ApiResponser;
use Exception;

class GeneralException extends Exception
{
    use ApiResponser;

    public function render()
    {
        if (request()->ajax()) {
            return $this->errorResponse($this->message, 422);
        } else {
            abort(403, $this->message);
        }
    }
}
<?php

namespace App\Exceptions\Payment;

use App\Foundations\Traits\ApiResponser;
use Exception;

class PaymentHasActive extends Exception
{
    use ApiResponser;

    public function render()
    {
        if (request()->ajax()) {
            return $this->errorResponse('Payment has been completed', 403);
        } else {
            abort(403, 'Payment has been completed');
        }
    }
}
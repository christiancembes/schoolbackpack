<?php

namespace App\Exceptions\Payment;

use App\Foundations\Traits\ApiResponser;
use Exception;

class PaymentCodeNotValid extends Exception
{
    use ApiResponser;

    public function render()
    {
        if (request()->ajax()) {
            return $this->errorResponse('Payment code not valid', 403);
        } else {
            abort(403, 'Payment code not valid');
        }
    }
}
<?php

namespace App\Exceptions\Payment;

use App\Foundations\Traits\ApiResponser;
use Exception;

class PaymentAmountNotMatch extends Exception
{
    use ApiResponser;

    public function render()
    {
        if (request()->ajax()) {
            return $this->errorResponse('Payment amount not match', 403);
        } else {
            abort(403, 'Payment amount not match');
        }

    }
}
<?php

namespace App\Foundations;

use App\Foundations\Base\HaveId\HaveIdModel;

interface MasterDeletableModelInterface extends HaveIdModel
{

}
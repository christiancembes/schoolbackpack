<?php

namespace App\Foundations\Traits;

trait CodeGenerator
{
    protected function generateCode($length = 7, $prefix = null)
    {
        $randstr = '';

        srand((float) microtime(TRUE) * 1000000);

        $chars = array(
            '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
            'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        );

        for ($rand = 0; $rand <= $length; $rand++) {
            $random = rand(0, count($chars) - 1);
            $randstr .= $chars[$random];
        }

        if ($prefix) {
            return $prefix . '-' . $randstr;
        }

        return $randstr;
    }
}
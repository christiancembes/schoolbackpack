<?php

namespace App\Foundations\Traits;

trait GradeConversion
{
    public function gradeConversion($score)
    {
        if ($score <= 100 && $score >= 80) {
            return 'A';
        }
        else if ($score <= 79 && $score >= 65) {
            return 'B';
        }
        else if ($score <= 64 && $score >= 55) {
            return 'C';
        }
        else if ($score <= 55) {
            return 'D';
        }
    }
}

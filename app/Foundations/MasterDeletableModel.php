<?php

namespace App\Foundations;

use App\Foundations\Base\HaveId\HaveIdModelTrait;
use App\Foundations\Traits\CanGetTableNameStatically;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterDeletableModel extends Model
{
    use HaveIdModelTrait, CanGetTableNameStatically;
}

<?php

namespace App\Foundations\Base\HaveUser;

use App\Modules\User\User;

trait HaveUserModelTrait
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function setUser(User $user)
    {
        return $this->user()->associate($user);
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getUserId()
    {
        return $this->user_id;
    }
}

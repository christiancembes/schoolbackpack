<?php

namespace App\Foundations\Base\HaveUser;

use App\Modules\User\User;

interface HaveUserModel
{
    public function setUser(User $user);

    public function getUser();

    public function getUserId();
}

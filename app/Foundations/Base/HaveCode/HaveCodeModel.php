<?php

namespace App\Foundations\Base\HaveCode;

interface HaveCodeModel
{
    public function getCode();
}
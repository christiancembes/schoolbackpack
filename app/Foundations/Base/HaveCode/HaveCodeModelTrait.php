<?php

namespace App\Foundations\Base\HaveCode;

trait HaveCodeModelTrait
{
    public function getCode()
    {
        return $this->code;
    }
}
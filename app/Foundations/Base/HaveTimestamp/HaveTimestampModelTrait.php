<?php

namespace App\Foundations\Base\HaveTimestamp;

trait HaveTimestampModelTrait
{
    /*
     * SCOPE
     */

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $date
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeWhereCreatedAtDate($query, $date)
    {
        return $query->whereDate('created_at', '=', $date);
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $direction
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeOrderByCreatedAt($query, $direction = 'asc')
    {
        return $query->orderBy('created_at', $direction);
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $dateTime
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeWhereCreatedAtAfter($query, $dateTime)
    {
        return $query->whereDate('created_at', '>=', $dateTime);
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $dateTime
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeWhereCreatedAtBefore($query, $dateTime)
    {
        return $query->whereDate('created_at', '<=', $dateTime);
    }

    /*
     * SETTER GETTER
     */

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
}

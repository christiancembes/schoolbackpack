<?php

namespace App\Foundations\Base\HaveTimestamp;

interface HaveTimestampModel
{
    /**
     * Get created at.
     *
     * @return \Carbon\Carbon
     */
    public function getCreatedAt();

    /**
     * Get updated at.
     *
     * @return string date($format) if not null
     */
    public function getUpdatedAt();
}

<?php

namespace App\Foundations\Base\HaveId;

interface HaveIdModel
{
    /**
     * Get id of the model.
     *
     * @return int
     */
    public function getId();
}

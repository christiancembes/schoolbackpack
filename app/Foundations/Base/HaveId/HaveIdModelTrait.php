<?php

namespace App\Foundations\Base\HaveId;

trait HaveIdModelTrait
{
    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param array|int                          $id
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeWhereId($query, $id)
    {
        if (method_exists($this, 'scopeWhereExactOrInId')) {
            return $query->whereExactOrInId($id);
        }

        $column = $this->getKeyName();

        if (is_array($id)) {
            return $query->whereIn($column, $id);
        }

        return $query->where($column, $id);
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param array|int                          $id
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeWhereNotId($query, $id)
    {
        if (method_exists($this, 'scopeWhereNotExactOrInId')) {
            return $query->whereNotExactOrInId($id);
        }

        $column = $this->getKeyName();

        if (is_array($id)) {
            return $query->whereNotIn($column, $id);
        }

        return $query->where($column, '!=', $id);
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $direction
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeOrderById($query, $direction = 'asc')
    {
        return $query->orderBy($this->getKeyName(), $direction);
    }

    /**
     * Id mutator.
     *
     * @param string $id
     * @return int
     */
    public function getIdAttribute($id)
    {
        if (is_null($id)) {
            return null;
        }

        return intval($id);
    }

    public function getId()
    {
        return $this->getKey();
    }
}

<?php

namespace App\Foundations\Base\HaveStudent;

use App\Modules\Student\Models\Student;

trait HaveStudentModelTrait
{
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }

    public function setUser(Student $student)
    {
        return $this->student()->associate($student);
    }

    public function getStudent()
    {
        return $this->student;
    }

    public function getStudentId()
    {
        return $this->student_id;
    }
}

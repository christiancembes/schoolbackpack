<?php

namespace App\Foundations\Base\HaveStudent;

use App\Modules\Student\Models\Student;

interface HaveStudentModel
{
    public function setStudent(Student $student);

    public function getStudent();

    public function getStudentId();
}

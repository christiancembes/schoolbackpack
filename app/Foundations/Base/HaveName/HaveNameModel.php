<?php

namespace App\Foundations\Base\HaveName;

interface HaveNameModel
{
    /**
     * Get name of the model.
     *
     * @return string
     */
    public function getName();
}

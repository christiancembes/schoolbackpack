<?php

namespace App\Foundations\Base\HaveName;

trait HaveNameModelTrait
{
    /**
     * Where name like.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $name
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeWhereNameLike($query, $name)
    {
        return $query->whereInfix($this->getHaveNameNameColumnName(), $name);
    }

    /**
     * Or where name like.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $name
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeOrWhereNameLike($query, $name)
    {
        return $query->orWhereInfix($this->getHaveNameNameColumnName(), $name);
    }

    /**
     * Where names like.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param array                              $names
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeWhereNamesLike($query, $names)
    {
        return $query->where(function ($query) use ($names) {
            foreach ($names as $name) {
                $query->orWhereNameLike($name);
            }
        });
    }

    /**
     * Where name.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $name
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeWhereName($query, $name)
    {
        return $query->where($this->getHaveNameNameColumnName(), $name);
    }

    /**
     * Where name prefix.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $prefix
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeWhereNamePrefix($query, $prefix)
    {
        return $query->wherePrefix($this->getHaveNameNameColumnName(), $prefix);
    }

    /**
     * Or where name prefix.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $prefix
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeOrWhereNamePrefix($query, $prefix)
    {
        return $query->orWherePrefix($this->getHaveNameNameColumnName(), $prefix);
    }

    /**
     * Where name has word starts with.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $name
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeWhereNameWordStartWith($query, $name)
    {
        return $query->orWhereNamePrefix($name)->orWhere(function($query) use ($name) {
            return $query->whereNameLike(' ' . $name);
        });
    }

    /**
     * Order by name.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param string                             $direction
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeOrderByName($query, $direction = 'asc')
    {
        return $query->orderBy($this->getHaveNameNameColumnName(), $direction);
    }

    /**
     * Mutator for name.
     *
     * @param string $name
     */
    public function setNameAttribute($name)
    {
        $this->attributes[$this->getHaveNameNameColumnName()] = trim(preg_replace('/\s+/', ' ', $name));
    }

    /**
     * Get the column name for name.
     *
     * @return string
     */
    protected function getHaveNameNameColumnName()
    {
        return $this->haveNameNameColumnName ?? 'name';
    }

    /**
     * Get name of the model.
     *
     * @return string
     */
    public function getName()
    {
        return $this->{$this->getHaveNameNameColumnName()};
    }
}

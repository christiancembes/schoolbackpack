import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  facilities: [],
  grades: [],
  gradeClasses: [],
  categories: [],
  types: [],
  periods: [],
  marketers: []
}

// getters
export const getters = {
  facilities: state => state.facilities,
  grades: state => state.grades,
  gradeClasses: state => state.gradeClasses,
  categories: state => state.categories,
  types: state => state.types,
  periods: state => state.periods,
  marketers: state => state.marketers,
}

// mutations
export const mutations = {
  [types.FETCH_GENERAL_FACILITIES] (state, { facilities }) {
    state.facilities = facilities
  },
  [types.FETCH_GENERAL_GRADES] (state, { grades }) {
    state.grades = grades
  },
  [types.FETCH_GENERAL_GRADE_CLASSES] (state, { gradeClasses }) {
    state.gradeClasses = gradeClasses
  },
  [types.FETCH_GENERAL_CATEGORIES] (state, { categories }) {
    state.categories = categories
  },
  [types.FETCH_GENERAL_TYPES] (state, { types }) {
    state.types = types
  },
  [types.FETCH_GENERAL_PERIODS] (state, { periods }) {
    state.periods = periods
  },
  [types.FETCH_GENERAL_MARKETERS] (state, { marketers }) {
    state.marketers = marketers
  },
}

// actions
export const actions = {
  async fetchFacilities ({ commit }) {
    try {
      const { data } = await axios.get('/api/v1/facilities')

      commit(types.FETCH_GENERAL_FACILITIES, { facilities: data.data })
    } catch (e) {
      console.log(e)
    }
  },

  async fetchGrades ({ commit }) {
    try {
      const { data } = await axios.get('/api/v1/grades')

      commit(types.FETCH_GENERAL_GRADES, { grades: data.data })
    } catch (e) {
      console.log(e)
    }
  },

  async fetchGradeClasses ({ commit }, id) {
    try {
      const { data } = await axios.get('/api/v1/grade-classes/' + id)

      commit(types.FETCH_GENERAL_GRADE_CLASSES, { gradeClasses: data.data })
    } catch (e) {
      console.log(e)
    }
  },

  async fetchCategories ({ commit }) {
    try {
      const { data } = await axios.get('/api/v1/student-categories')

      commit(types.FETCH_GENERAL_CATEGORIES, { categories: data.data })
    } catch (e) {
      console.log(e)
    }
  },

  async fetchTypes ({ commit }) {
    try {
      const { data } = await axios.get('/api/v1/student-types')

      commit(types.FETCH_GENERAL_TYPES, { types: data.data })
    } catch (e) {
      console.log(e)
    }
  },

  async fetchPeriods ({ commit }) {
    try {
      const { data } = await axios.get('/api/v1/school-periods')

      commit(types.FETCH_GENERAL_PERIODS, { periods: data.data })
    } catch (e) {
      console.log(e)
    }
  },

  async fetchMarketers ({ commit }) {
    try {
      const { data } = await axios.get('/api/v1/marketers')

      commit(types.FETCH_GENERAL_MARKETERS, { marketers: data.data })
    } catch (e) {
      console.log(e)
    }
  },
}
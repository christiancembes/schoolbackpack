import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  payments: null
}

// getters
export const getters = {
  payments: state => state.payments,
}

// mutations
export const mutations = {
  [types.FETCH_PAYMENT_SUCCESS] (state, { payments }) {
    state.payments = payments
  },
}

// actions
export const actions = {
  async fetchPayments ({ commit }) {
    try {
      const { data } = await axios.get('/api/v1/payments')

      commit(types.FETCH_PAYMENT_SUCCESS, { payments: data.data })
    } catch (e) {

    }
  },
  async savePaymentConfirmation ({ commit }, payloads) {
    const { data } = await axios.post('/api/v1/payment-confirmation', payloads)

    return data
  },
}
import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
  countries: [],
  provinces: [],
  cities: [],
  districts: [],
  villages: []
}

// getters
export const getters = {
  countries: state => state.countries,
  provinces: state => state.provinces,
  cities: state => state.cities,
  districts: state => state.districts,
  villages: state => state.villages
}

// mutations
export const mutations = {
  [types.FETCH_LOCATION_COUNTRIES] (state, { countries }) {
    state.countries = countries
  },
  [types.FETCH_LOCATION_PROVINCES] (state, { provinces }) {
    state.provinces = provinces
  },
  [types.FETCH_LOCATION_CITIES] (state, { cities }) {
    state.cities = cities
  },
  [types.FETCH_LOCATION_DISTRICTS] (state, { districts }) {
    state.districts = districts
  },
  [types.FETCH_LOCATION_VILLAGES] (state, { villages }) {
    state.villages = villages
  },
}

// actions
export const actions = {
  async fetchCountries ({ commit }) {
    try {
      const { data } = await axios.get('/api/v1/countries')

      commit(types.FETCH_LOCATION_COUNTRIES, { countries: data.data })
    } catch (e) {
      console.log(e)
    }
  },

  async fetchProvinces ({ commit }) {
    try {
      const { data } = await axios.get('/api/v1/provinces')

      commit(types.FETCH_LOCATION_PROVINCES, { provinces: data.data })
    } catch (e) {
      console.log(e)
    }
  },

  async fetchCities ({ commit }, provinceId) {
    try {
      const { data } = await axios.get('/api/v1/cities/' + provinceId)

      commit(types.FETCH_LOCATION_CITIES, { cities: data.data })
    } catch (e) {
      console.log(e)
    }
  },

  async fetchDistricts ({ commit }, cityId) {
    try {
      const { data } = await axios.get('/api/v1/districts/' + cityId)

      commit(types.FETCH_LOCATION_DISTRICTS, { districts: data.data })
    } catch (e) {
      console.log(e)
    }
  },

  async fetchVillages ({ commit }, districtId) {
    try {
      const { data } = await axios.get('/api/v1/villages/' + districtId)

      commit(types.FETCH_LOCATION_VILLAGES, { villages: data.data })
    } catch (e) {
      console.log(e)
    }
  },
}
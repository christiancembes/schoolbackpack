<?php

return [
    'waiting' => 'Menunggu Pembayaran',
    'success' => 'Pembayaran Berhasil',
    'expired' => 'Pembayaran Kadaluarsa',
    'cancel' => 'Pembayaran Batal',
    'waiting_confirmation' => 'Menunggu Konfirmasi'

];

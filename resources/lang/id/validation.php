<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':Attribute harus diterima.',
    'active_url'           => ':Attribute bukan URL yang valid.',
    'after'                => ':Attribute harus berisi tanggal setelah :date.',
    'after_or_equal'       => ':Attribute harus berisi tanggal setelah atau sama dengan :date.',
    'alpha'                => ':Attribute hanya boleh berisi huruf.',
    'alpha_dash'           => ':Attribute hanya boleh berisi huruf, angka, strip, dan garis bawah.',
    'alpha_num'            => ':Attribute hanya boleh berisi huruf dan angka.',
    'array'                => ':Attribute harus berisi sebuah array.',
    'before'               => ':Attribute harus berisi tanggal sebelum :date.',
    'before_or_equal'      => ':Attribute harus berisi tanggal sebelum atau sama dengan :date.',
    'between'              => [
        'numeric' => ':Attribute harus bernilai antara :min sampai :max.',
        'file'    => ':Attribute harus berukuran antara :min sampai :max kilobita.',
        'string'  => ':Attribute harus berisi antara :min sampai :max karakter.',
        'array'   => ':Attribute harus memiliki :min sampai :max anggota.',
    ],
    'boolean'              => ':Attribute harus bernilai true atau false',
    'confirmed'            => 'Konfirmasi :attribute tidak cocok.',
    'date'                 => ':Attribute bukan tanggal yang valid.',
    'date_equals'          => ':Attribute harus berisi tanggal yang sama dengan :date.',
    'date_format'          => ':Attribute tidak cocok dengan format :format.',
    'different'            => ':Attribute dan :other harus berbeda.',
    'digits'               => ':Attribute harus terdiri dari :digits angka.',
    'digits_between'       => ':Attribute harus terdiri dari :min sampai :max angka.',
    'dimensions'           => ':Attribute tidak memiliki dimensi gambar yang valid.',
    'distinct'             => ':Attribute memiliki nilai yang duplikat.',
    'email'                => ':Attribute harus berupa alamat surel yang valid.',
    'ends_with'            => ':Attribute harus diakhiri salah satu dari berikut: :values',
    'exists'               => ':Attribute yang dipilih tidak valid.',
    'file'                 => ':Attribute harus berupa sebuah berkas.',
    'filled'               => ':Attribute harus memiliki nilai.',
    'gt'                   => [
        'numeric' => ':Attribute harus bernilai lebih besar dari :value.',
        'file'    => ':Attribute harus berukuran lebih besar dari :value kilobita.',
        'string'  => ':Attribute harus berisi lebih besar dari :value karakter.',
        'array'   => ':Attribute harus memiliki lebih dari :value anggota.',
    ],
    'gte'                  => [
        'numeric' => ':Attribute harus bernilai lebih besar dari atau sama dengan :value.',
        'file'    => ':Attribute harus berukuran lebih besar dari atau sama dengan :value kilobita.',
        'string'  => ':Attribute harus berisi lebih besar dari atau sama dengan :value karakter.',
        'array'   => ':Attribute harus terdiri dari :value anggota atau lebih.',
    ],
    'image'                => ':Attribute harus berupa gambar.',
    'in'                   => ':Attribute yang dipilih tidak valid.',
    'in_array'             => ':Attribute tidak ada di dalam :other.',
    'integer'              => ':Attribute harus berupa bilangan bulat.',
    'ip'                   => ':Attribute harus berupa alamat IP yang valid.',
    'ipv4'                 => ':Attribute harus berupa alamat IPv4 yang valid.',
    'ipv6'                 => ':Attribute harus berupa alamat IPv6 yang valid.',
    'json'                 => ':Attribute harus berupa JSON string yang valid.',
    'lt'                   => [
        'numeric' => ':Attribute harus bernilai kurang dari :value.',
        'file'    => ':Attribute harus berukuran kurang dari :value kilobita.',
        'string'  => ':Attribute harus berisi kurang dari :value karakter.',
        'array'   => ':Attribute harus memiliki kurang dari :value anggota.',
    ],
    'lte'                  => [
        'numeric' => ':Attribute harus bernilai kurang dari atau sama dengan :value.',
        'file'    => ':Attribute harus berukuran kurang dari atau sama dengan :value kilobita.',
        'string'  => ':Attribute harus berisi kurang dari atau sama dengan :value karakter.',
        'array'   => ':Attribute harus tidak lebih dari :value anggota.',
    ],
    'max'                  => [
        'numeric' => ':Attribute maksimal bernilai :max.',
        'file'    => ':Attribute maksimal berukuran :max kilobita.',
        'string'  => ':Attribute maksimal berisi :max karakter.',
        'array'   => ':Attribute maksimal terdiri dari :max anggota.',
    ],
    'mimes'                => ':Attribute harus berupa berkas berjenis: :values.',
    'mimetypes'            => ':Attribute harus berupa berkas berjenis: :values.',
    'min'                  => [
        'numeric' => ':Attribute minimal bernilai :min.',
        'file'    => ':Attribute minimal berukuran :min kilobita.',
        'string'  => ':Attribute minimal berisi :min karakter.',
        'array'   => ':Attribute minimal terdiri dari :min anggota.',
    ],
    'multiple_of'          => 'The :attribute must be a multiple of :value',
    'not_in'               => ':Attribute yang dipilih tidak valid.',
    'not_regex'            => 'Format :attribute tidak valid.',
    'numeric'              => ':Attribute harus berupa angka.',
    'password'             => 'Kata sandi salah.',
    'present'              => ':Attribute wajib ada.',
    'regex'                => 'Format :attribute tidak valid.',
    'required'             => ':Attribute wajib diisi.',
    'required_if'          => ':Attribute wajib diisi bila :other adalah :value.',
    'required_unless'      => ':Attribute wajib diisi kecuali :other memiliki nilai :values.',
    'required_with'        => ':Attribute wajib diisi bila terdapat :values.',
    'required_with_all'    => ':Attribute wajib diisi bila terdapat :values.',
    'required_without'     => ':Attribute wajib diisi bila tidak terdapat :values.',
    'required_without_all' => ':Attribute wajib diisi bila sama sekali tidak terdapat :values.',
    'same'                 => ':Attribute dan :other harus sama.',
    'size'                 => [
        'numeric' => ':Attribute harus berukuran :size.',
        'file'    => ':Attribute harus berukuran :size kilobyte.',
        'string'  => ':Attribute harus berukuran :size karakter.',
        'array'   => ':Attribute harus mengandung :size anggota.',
    ],
    'starts_with'          => ':Attribute harus diawali salah satu dari berikut: :values',
    'string'               => ':Attribute harus berupa string.',
    'timezone'             => ':Attribute harus berisi zona waktu yang valid.',
    'unique'               => ':Attribute sudah ada sebelumnya.',
    'uploaded'             => ':Attribute gagal diunggah.',
    'url'                  => 'Format :attribute tidak valid.',
    'uuid'                 => ':Attribute harus merupakan UUID yang valid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'student_category_id' => 'Kategori',
        'student_type_id' => 'Tipe',
        'school_period_id' => 'Period',
        'grade_id' => 'Tingkat',
        'grade_class_id' => 'Kelas',
        'country_id' => 'Kebangsaan',
        'province_id' => 'Provinsi',
        'city_id' => 'Kota/Kabupaten',
        'district_id' => 'Kecamatan',
        'village' => 'Kelurahan/Desa',

        'full_name' => 'Nama Lengkap',
        'name' => 'Nama',
        'place_of_birth' => 'Tempat Lahir',
        'date_of_birth' => 'Tanggal Lahir',
        'gender' => 'Jenis Kelamin',
        'goal' => 'Cita-Cita',
        'hobby' => 'Hobi',
        'order_in_family' => 'Anak ke',
        'numbers_of_siblings' => 'Jumlah Saudara',
        'numbers_of_step_families' => 'Jumlah Saudara Tiri',
        'numbers_of_adopted_families' => 'Jumlah Saudara Angkat',
        'weight' => 'Berat',
        'height' => 'Tinggi',
        'blood_type' => 'Golongan Darah',
        'mother_language' => 'Bahasa Sehari-hari',
        'address' => 'Alamat',
        'phone' => 'Telp',
        'live_with' => 'Tinggal Dengan',
        'postal_code' => 'Kode Pos',

        'address.address' => 'Alamat',
        'address.postal_code' => 'Kode Pos',
        'address.phone' => 'Telp',
        'address.email' => 'Email',
        'address.live_with' => 'Tinggal Dengan',
        'address.province_id' => 'Provinsi',
        'address.city_id' => 'Kota/Kabupaten',
        'address.district_id' => 'Kecamatan',
        'address.village_id' => 'Kelurahan/Desa',

        'parents.father.type' => 'Tipe Ayah',
        'parents.father.name' => 'Nama Ayah',
        'parents.father.place_of_birth' => 'Tempat Lahir Ayah',
        'parents.father.date_of_birth' => 'Tanggal Lahir Ayah',
        'parents.father.profession' => 'Pekerjaan Ayah',
        'parents.father.last_education' => 'Pendidikan Terakhir Ayah',
        'parents.father.religion' => 'Agama Ayah',
        'parents.father.mobile' => 'No Handphone Ayah',
        'parents.father.email' => 'Email Ayah',
        'parents.father.company' => 'Perusahaan Ayah',
        'parents.father.office_address' => 'Alamat Kantor Ayah',
        'parents.father.income' => 'Penghasilan Ayah',
        'parents.father.country_id' => 'Kebangsaan Ayah',

        'parents.mother.type' => 'Tipe Ibu',
        'parents.mother.name' => 'Nama Ibu',
        'parents.mother.place_of_birth' => 'Tempat Lahir Ibu',
        'parents.mother.date_of_birth' => 'Tanggal Lahir Ibu',
        'parents.mother.profession' => 'Pekerjaan Ibu',
        'parents.mother.last_education' => 'Pendidikan Terakhir Ibu',
        'parents.mother.religion' => 'Agama Ibu',
        'parents.mother.mobile' => 'No Handphone Ibu',
        'parents.mother.email' => 'Email Ibu',
        'parents.mother.company' => 'Perusahaan Ibu',
        'parents.mother.office_address' => 'Alamat Kantor Ibu',
        'parents.mother.income' => 'Penghasilan Ibu',
        'parents.mother.country_id' => 'Kebangsaan Ibu',

        'parents.wali.type' => 'Tipe Wali',
        'parents.wali.name' => 'Nama Wali',
        'parents.wali.place_of_birth' => 'Tempat Lahir Wali',
        'parents.wali.date_of_birth' => 'Tanggal Lahir Wali',
        'parents.wali.profession' => 'Pekerjaan Wali',
        'parents.wali.last_education' => 'Pendidikan Terakhir Wali',
        'parents.wali.religion' => 'Agama Wali',
        'parents.wali.mobile' => 'No Handphone Wali',
        'parents.wali.email' => 'Email Wali',
        'parents.wali.company' => 'Perusahaan Wali',
        'parents.wali.office_address' => 'Alamat Kantor Wali',
        'parents.wali.income' => 'Penghasilan Wali',
        'parents.wali.country_id' => 'Kebangsaan Wali',

        'previous_education.school_origin' => 'Sekolah Asal',
        'previous_education.school_address' => 'Alamat Sekolah Asal',
        'previous_education.phone' => 'Telp',
        'previous_education.head_master_name' => 'Kepala Sekolah',
        'previous_education.nisn' => 'Nisn',
        'previous_education.school_status' => 'Status Sekolah',
        'previous_education.education_system' => 'Sistem Pendidikan',
        'previous_education.period' => 'Lama Sekolah',
        'previous_education.province_id' => 'Provinsi',
        'previous_education.city_id' => 'Kota/Kabupaten',
        'previous_education.district_id' => 'Kecamatan',
        'previous_education.village_id' => 'Kelurahan/Desa',
    ],

];

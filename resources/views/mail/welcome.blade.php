@component('mail::message')
# Assalamu'alaikum Warahmatullahi Wabarakatuh,

Berikut ini detail akun saudara <br>

Email: {{ $user->getEmail() }} <br>

Password: {{ $mailData['pass'] }}

@component('mail::button', ['url' => $url])
Masuk
@endcomponent

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent

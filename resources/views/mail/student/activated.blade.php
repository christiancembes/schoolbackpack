@component('mail::message')
# Assalamu'alaikum Warahmatullahi Wabarakatuh,

Selamat status kesiswaan anda telah aktif. <br>

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')
# Assalamu'alaikum Warahmatullahi Wabarakatuh,

Berikut ini detail tagihan lanjutan <br>

Kode Unik: {{ $mailData['quotation']->getCode() }} <br>
Total: {{ $mailData['quotation']->getTotalAmountMoney() }} <br>
Diskon: {{ $mailData['quotation']->getTotalDiscountMoney() }} <br>
Total Akhir: {{ $mailData['quotation']->getFinalAmountMoney() }} <br>
<br>
Rincian Tagihan
<br>

@component('mail::table')
| Deskripsi     | Jumlah        | Diskon   |
| ------------- |:-------------:| --------:|
@foreach($mailData['quotation_details'] as $quotationDetail)
| {{ $quotationDetail->getDescription() }} | {{ $quotationDetail->getAmountMoney() }} | {{ $quotationDetail->getDiscountMoney() }} |
@endforeach
@endcomponent

Rincian Cicilan
<br>
@if($mailData['quotation_schedules'])
@component('mail::table')
| Kode          | Deskripsi     | Jumlah        | Selesai       |
| ------------- |:-------------:|:-------------:|:-------------:|
@foreach($mailData['quotation_schedules'] as $quotationSchedules)
| {{ $quotationSchedules->getCode() }} | {{ $quotationSchedules->getDescription() }} | {{ $quotationSchedules->getAmountMoney() }} | {{ $quotationSchedules->getCompletedAt() }} |
@endforeach
@endcomponent
@endif

Jika telah melakukan pembayaran mohon mengkonfirmasi pada link di bawah ini
@component('mail::button', ['url' => url('/payment-confirmation')])
Konfirmasi
@endcomponent

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent

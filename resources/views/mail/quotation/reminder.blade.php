@component('mail::message')
# Assalamu'alaikum Warahmatullahi Wabarakatuh,

Berikut ini adalah pengingat pembayaran dengan nomor {{ $mailData['quotation']->getCode() }}<br>

Kode Unik: {{ $mailData['quotation']->getCode() }} <br>
Total: {{ $mailData['quotation']->getTotalAmountMoney() }} <br>
Sisa Pembayaran: {{ $mailData['quotation']->getRemainingAmountMoney() }} <br>
Diskon: {{ $mailData['quotation']->getTotalDiscountMoney() }} <br>
Total Akhir: {{ $mailData['quotation']->getFinalAmountMoney() }} <br>
<br>
Rincian Tagihan
<br>

@component('mail::table')
| Deskripsi     | Jumlah        | Diskon   |
| ------------- |:-------------:| --------:|
@foreach($mailData['quotation_details'] as $quotationDetail)
| {{ $quotationDetail->getDescription() }} | {{ $quotationDetail->getAmountMoney() }} | {{ $quotationDetail->getDiscountMoney() }} |
@endforeach
@endcomponent

Rincian Cicilan
<br>
@if($mailData['quotation_schedules'])
@component('mail::table')
| Kode          | Deskripsi     | Jumlah        | Selesai       |
| ------------- |:-------------:|:-------------:|:-------------:|
@foreach($mailData['quotation_schedules'] as $quotationSchedules)
| {{ $quotationSchedules->getCode() }} | {{ $quotationSchedules->getDescription() }} | {{ $quotationSchedules->getAmountMoney() }} | {{ $quotationSchedules->getCompletedAt() }} |
@endforeach
@endcomponent
@endif

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')
# Assalamu'alaikum Warahmatullahi Wabarakatuh,

Request Payout Marketing telah masuk. <br>

Marketing: {{ $mailData['marketer']->getName() }} <br>
Jumlah: {{ $mailData['marketerPayout']->getAmountMoney() }} <br>

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent

@component('mail::message')
# Assalamu'alaikum Warahmatullahi Wabarakatuh,

Jika telah melakukan pembayaran mohon mengkonfirmasi pada link di bawah ini
@component('mail::button', ['url' => url('/payment-confirmation')])
Konfirmasi
@endcomponent

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent

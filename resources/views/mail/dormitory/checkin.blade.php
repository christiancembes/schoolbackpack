@component('mail::message')
# Assalamu'alaikum Warahmatullahi Wabarakatuh,

Berikut ini detil asrama anda. <br>

Kamar: {{ $mailData['dormitory']->getName() }} <br>
Kapasitas: {{ $mailData['dormitory']->getCapacity() }} <br>
Gedung: {{ $mailData['dormitory']->getBuilding() }} <br>
Lantai: {{ $mailData['dormitory']->getFloor() }} <br>

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent

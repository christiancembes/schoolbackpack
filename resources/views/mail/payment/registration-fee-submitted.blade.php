@component('mail::message')
# Assalamu'alaikum Warahmatullahi Wabarakatuh,

Konfirmasi pembayaran dengan kode {{ $mailData['paymentConfirmation']->getCode() }} telah masuk. <br>

Total: {{ $mailData['paymentConfirmation']->getAmount() }} <br>
Pengirim: {{ $mailData['paymentConfirmation']->getSenderName() }} <br>
Rekening Pengirim: {{ $mailData['paymentConfirmation']->getSenderBankAccount() }} <br>

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent

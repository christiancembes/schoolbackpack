@component('mail::message')
# Assalamu'alaikum Warahmatullahi Wabarakatuh,

Berikut ini adalah pengingat pembayaran dengan nomor {{ $mailData['paymentTransaction']->getCode() }}<br>

Total: {{ $mailData['paymentTransaction']->getTotalAmount() }} <br>
Deskripsi: {{ $mailData['paymentTransaction']->getDescription() }} <br>

Jika telah melakukan pembayaran mohon mengkonfirmasi pada link di bawah ini
@component('mail::button', ['url' => url('/payment-confirmation')])
Konfirmasi
@endcomponent

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent

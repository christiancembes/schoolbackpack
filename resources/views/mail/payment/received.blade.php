@component('mail::message')
# Assalamu'alaikum Warahmatullahi Wabarakatuh,

Pembayaran dengan kode {{ $mailData['payment_confirmation']->getCode() }} telah kami terima. <br>

Total: {{ $mailData['payment_confirmation']->getAmount() }} <br>
Pengirim: {{ $mailData['payment_confirmation']->getSenderName() }} <br>
Rekening Pengirim: {{ $mailData['payment_confirmation']->getSenderBankAccount() }} <br>

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent

{{--@if ($crud->hasAccess('create'))--}}
<a href="javascript:void(0)" onclick="confirmation(this)" data-route="{{ url($crud->route . '/confirmation/' . $crud->entry->code) }}" class="btn btn-sm btn-link" data-button-type="repayment">
    <span class="ladda-label"><i class="la la-check-double"></i> Konfirmasi Pembayaran Diterima</span>
</a>
{{--@endif--}}

@push('after_scripts')
<script>
    if (typeof confirmation != 'function') {
        $("[data-button-type=repayment]").unbind('click');

        function confirmation(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');

            $.ajax({
                url: route,
                type: 'POST',
                success: function(result) {
                    // Show an alert with the result
                    new Noty({
                          text: "Confirmation Success",
                          type: "success"
                    }).show();

                    // Hide the modal, if any
                    $('.modal').modal('hide');

                    crud.table.ajax.reload();
                },
                error: function(result) {
                    // Show an alert with the result
                    new Noty({
                        text: "Please try again.",
                        type: "warning"
                    }).show();
                }
            });
        }
    }
</script>
@endpush
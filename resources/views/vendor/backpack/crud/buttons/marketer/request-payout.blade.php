@if (auth()->user()->can('marketer'))
<a href="javascript:void(0)" class="btn btn-sm btn-link" data-toggle="modal" data-target="#payout-form">
    <span class="ladda-label"><i class="la la-dollar"></i> Request Payout</span>
</a>
@endif

@push('after_styles')
<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>
@endpush

@push('after_scripts')>
<!-- Modal -->
<div class="modal fade" id="payout-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ url('admin/master/api/marketer/request-payout/') }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Request Payout</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <input name="marketer_id" type="hidden" class="form-control" value="{{ $crud->entry->id }}">
                    </div>
                    <div class="form-group">
                        <label for="amount" class="col-form-label">Jumlah Payout:</label>
                        <input name="amount" type="number" class="form-control" id="amount">
                        <div class="invalid-feedback" id="amountError"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function(){
        $("form").submit(function(){
            $.ajax({
                url:$(this).attr("action"),
                data:$(this).serialize(),
                type:$(this).attr("method"),
                dataType: 'json',
                beforeSend: function() {
                    $("button").attr("disabled",true);
                },
                complete:function() {
                    $("button").attr("disabled",false);
                },
                success:function(res) {
                    swal("Sukses", "Request payout berhasil", "success");
                    $('#payout-form').modal('hide')
                },
                error:function(e) {
                    $("button").attr("disabled",false);
                    $('#amountError').text(e.responseJSON.errors.amount);
                    document.getElementById('amountError').style.display='block';
                }
            })
            return false;
        });
    });
</script>
@endpush
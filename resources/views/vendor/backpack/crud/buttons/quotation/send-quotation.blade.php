@if ($crud->hasAccess('create'))
<a href="javascript:void(0)" onclick="confirmation(this)" data-route="{{ url($crud->route . '/send/' . $crud->entry->id) }}" class="btn btn-sm btn-link" data-button-type="confirmation">
    <span class="ladda-label"><i class="la la-send"></i> Kirim Tagihan</span>
</a>
@endif

@push('after_scripts')
<script>
    if (typeof confirmation != 'function') {
        $("[data-button-type=confirmation]").unbind('click');

        function confirmation(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');

            $.ajax({
                url: route,
                type: 'POST',
                success: function(result) {
                    // Show an alert with the result
                    new Noty({
                          text: "Tagihan Terkirim",
                          type: "success"
                    }).show();

                    // Hide the modal, if any
                    $('.modal').modal('hide');

                    crud.table.ajax.reload();
                },
                error: function(result) {
                    // Show an alert with the result
                    new Noty({
                        text: "Please try again.",
                        type: "warning"
                    }).show();
                }
            });
        }
    }
</script>
@endpush
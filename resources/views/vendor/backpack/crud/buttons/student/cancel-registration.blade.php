
<a href="javascript:void(0)" onclick="cancelRegistration(this)" data-route="{{ url(config('backpack.base.route_prefix') . '/student/student/' . $entry->getKey() . '/cancel') }}" class="btn btn-sm btn-link" data-button-type="cancelRegistration">
    <span class="ladda-label"><i class="la la-remove"></i> Batal Pembayaran</span>
</a>

@push('after_scripts') @if (request()->ajax()) @endpush @endif
<script>
    if (typeof cancelRegistration != 'function') {
        $("[data-button-type=cancelRegistration]").unbind('click');

        function cancelRegistration(button) {
            // ask for cancelRegistration before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');

            swal({
                title: "{!! trans('backpack::base.warning') !!}",
                text: "Anda yakin ingin membatalkan siswa ini?",
                icon: "warning",
                buttons: {
		  	        cancel: {
			            text: "{!! trans('backpack::crud.cancel') !!}",
			            value: null,
                        visible: true,
                        className: "bg-secondary",
                        closeModal: true,
			        },
                    delete: {
                        text: "Proses",
                        value: true,
                        visible: true,
                        className: "bg-danger",
                    }
		        },
            }).then((value) => {
                if (value) {
                    $.ajax({
                        url: route,
                        type: 'POST',
                        success: function(result) {
                            // Show an alert with the result
                            new Noty({
                                  text: "Pembatalan Siswa Berhasil",
                                  type: "success"
                            }).show();

                            // Hide the modal, if any
                            $('.modal').modal('hide');

                            crud.table.ajax.reload();
                        },
                        error: function(result) {
                            // Show an alert with the result
                            new Noty({
                                text: "Pembatalan Siswa Gagal",
                                type: "warning"
                            }).show();
                        }
                    });
                }
            })

        }
    }
</script>
@if (!request()->ajax()) @endpush @endif
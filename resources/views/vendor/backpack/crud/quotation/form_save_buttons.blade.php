@if(isset($saveAction['active']) && !is_null($saveAction['active']['value']))
    <div id="saveActions" class="form-group">

        <input type="hidden" name="save_action" value="{{ $saveAction['active']['value'] }}">
        @if(!empty($saveAction['options']))
            <div class="btn-group" role="group">
        @endif

        <button type="submit" class="btn btn-success">
            <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
            <span data-value="{{ $saveAction['active']['value'] }}">{{ $saveAction['active']['label'] }}</span>
        </button>

        <div class="btn-group" role="group">
            @if(!empty($saveAction['options']))
                <button id="btnGroupDrop1" type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span><span class="sr-only">&#x25BC;</span></button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    @foreach( $saveAction['options'] as $value => $label)
                    <a class="dropdown-item" href="javascript:void(0);" data-value="{{ $value }}">{{ $label }}</a>
                    @endforeach
                </div>
            @endif
        </div>

        @if(!empty($saveAction['options']))
            </div>
        @endif

        <a href="javascript:void(0)" onclick="confirmation(this)" data-route="{{ url($crud->route . '/send/' . $crud->entry->id) }}" class="btn btn-default" data-button-type="confirmation">
            <span class="ladda-label"><i class="la la-send"></i> Kirim Tagihan</span>
        </a>

        @if(!$crud->hasOperationSetting('showCancelButton') || $crud->getOperationSetting('showCancelButton') == true)
            <a href="{{ $crud->hasAccess('list') ? url($crud->route) : url()->previous() }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;{{ trans('backpack::crud.cancel') }}</a>
        @endif

    </div>
@endif

@push('after_scripts')
<script>
    if (typeof confirmation != 'function') {
        $("[data-button-type=confirmation]").unbind('click');

        function confirmation(button) {
            // ask for confirmation before deleting an item
            // e.preventDefault();
            var button = $(button);
            var route = button.attr('data-route');

            $.ajax({
                url: route,
                type: 'POST',
                success: function(result) {
                    // Show an alert with the result
                    new Noty({
                          text: "Tagihan Terkirim",
                          type: "success"
                    }).show();

                    // Hide the modal, if any
                    $('.modal').modal('hide');

                    crud.table.ajax.reload();
                },
                error: function(result) {
                    // Show an alert with the result
                    new Noty({
                        text: "Please try again.",
                        type: "warning"
                    }).show();
                }
            });
        }
    }
</script>
@endpush
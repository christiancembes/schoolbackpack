@extends(backpack_view('blank'))

@php
    $defaultBreadcrumbs = [
      trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
      $crud->entity_name_plural => url($crud->route),
      trans('backpack::crud.preview') => false,
    ];

    // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
    $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp

@section('header')
    <section class="container-fluid d-print-none">
        <a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
        <h2>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}.</small>
            @if ($crud->hasAccess('list'))
                <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
            @endif
        </h2>
    </section>
@endsection

@section('content')
    <div class="row">
        <div class="{{ $crud->getShowContentClass() }}">

            <!-- Default box -->
            <div class="">
                @if ($crud->model->translationEnabled())
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <!-- Change translation button group -->
                            <div class="btn-group float-right">
                                <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{trans('backpack::crud.language')}}: {{ $crud->model->getAvailableLocales()[request()->input('locale')?request()->input('locale'):App::getLocale()] }} &nbsp; <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    @foreach ($crud->model->getAvailableLocales() as $key => $locale)
                                        <a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}?locale={{ $key }}">{{ $locale }}</a>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @else
                @endif
                <div class="card no-padding no-border">
                    <table class="table table-striped mb-0">
                        <tbody>
                        @foreach ($crud->columns() as $column)
                            <tr>
                                <td>
                                    <strong>{!! $column['label'] !!}:</strong>
                                </td>
                                <td>
                                    @if (!isset($column['type']))
                                        @include('crud::columns.text')
                                    @else
                                        @if(view()->exists('vendor.backpack.crud.columns.'.$column['type']))
                                            @include('vendor.backpack.crud.columns.'.$column['type'])
                                        @else
                                            @if(view()->exists('crud::columns.'.$column['type']))
                                                @include('crud::columns.'.$column['type'])
                                            @else
                                                @include('crud::columns.text')
                                            @endif
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        @if ($crud->buttons()->where('stack', 'line')->count())
                            <tr>
                                <td><strong>{{ trans('backpack::crud.actions') }}</strong></td>
                                <td>
                                    @include('crud::inc.button_stack', ['stack' => 'line'])
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div><!-- /.box-body -->

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" id="reportTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="score-tab" data-toggle="tab" href="#score" role="tab" aria-controls="score">Score</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="assessment-tab" data-toggle="tab" href="#assessment" role="tab" aria-controls="assessment">Assessment</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade show active" id="score" role="tabpanel">
                        <!-- Report Score -->
                        <div class="card no-padding no-border">
                            <h3 class="mt-2 ml-2">Report</h3>
                            <table class="table table-striped mb-0">
                                <tbody>
                                    <tr>
                                        <th>
                                            Mata Pelajaran
                                        </th>
                                        <th>
                                            Nilai
                                        </th>
                                        <th>
                                            Predikat
                                        </th>
                                        <th>
                                            Catatan
                                        </th>
                                    </tr>
                                    @foreach($reportScores as $reportScore)
                                        <tr>
                                            <td>
                                                {{ $reportScore->getSubject()->getName() }}
                                            </td>
                                            <td>
                                                {{ $reportScore->getScore() }}
                                            </td>
                                            <td>
                                                {{ $reportScore->getPredicate() }}
                                            </td>
                                            <td>
                                                {{ $reportScore->getNotes() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                    </div>
                    <div class="tab-pane fade" id="assessment" role="tabpanel">
                        <h3 class="mt-2 ml-2">Ekstra Kurikuler</h3>
                        <div class="card no-padding no-border">
                            <table class="table table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th>Kegiatan</th>
                                        <th>Nilai</th>
                                        <th>Deskripsi</th>
                                    </tr>
                                    @foreach($extracurricular as $value)
                                        <tr>
                                            <td>{{ $value['activity'] }}</td>
                                            <td>{{ $value['value'] }}</td>
                                            <td>{{ $value['description'] }}</td>
                                        </tr>
                                    @endforeach
                                </thead>
                            </table>
                        </div>

                        <h3 class="mt-2 ml-2">Prestasi</h3>
                        <div class="card no-padding no-border">
                            <table class="table table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th>Kegiatan</th>
                                        <th>Keterangan</th>
                                    </tr>
                                    @foreach($achievement as $value)
                                        <tr>
                                            <td>{{ $value['activity'] }}</td>
                                            <td>{{ $value['note'] }}</td>
                                        </tr>
                                    @endforeach
                                </thead>
                            </table>
                        </div>

                        <h3 class="mt-2 ml-2">Absence</h3>
                        <div class="card no-padding no-border">
                            <table class="table table-striped mb-0">
                                <thead>
                                    <tr>
                                        <th>Sakit</th>
                                        <th>Ijin</th>
                                        <th>Alfa</th>
                                    </tr>
                                    @foreach($absence as $value)
                                        <tr>
                                            <td>{{ $value['sick'] }}</td>
                                            <td>{{ $value['permit'] }}</td>
                                            <td>{{ $value['absense'] }}</td>
                                        </tr>
                                    @endforeach
                                </thead>
                            </table>
                        </div>

                        <h3 class="mt-2 ml-2">Catatan Wali Kelas</h3>
                        <div class="card no-padding no-border">
                            <p class="mt-2 ml-2">{{ $guardian_note }}</p>
                        </div>

                        <h3 class="mt-2 ml-2">Catatan Orang Tua</h3>
                        <div class="card no-padding no-border">
                            <p class="mt-2 ml-2">{{ $parent_note }}</p>
                        </div>
                    </div>
                </div>


            </div><!-- /.box -->

        </div>
    </div>
@endsection

@section('after_styles')
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
@endsection

@section('after_scripts')
    <script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
@endsection

@extends(backpack_view('blank'))

@php
    $defaultBreadcrumbs = [
      trans('backpack::crud.admin') => url(config('backpack.base.route_prefix'), 'dashboard'),
      $crud->entity_name_plural => url($crud->route),
      trans('backpack::crud.preview') => false,
    ];

    // if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
    $breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp

@section('header')
    <section class="container-fluid d-print-none">
        <a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
        <h2>
            <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
            <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}.</small>
            @if ($crud->hasAccess('list'))
                <small class=""><a href="{{ url($crud->route) }}" class="font-sm"><i class="la la-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ $crud->entity_name_plural }}</span></a></small>
            @endif
        </h2>
    </section>
@endsection

@section('content')
    <div class="row">
        <div class="{{ $crud->getShowContentClass() }}">

            <!-- Default box -->
            <div class="">
                @if ($crud->model->translationEnabled())
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <!-- Change translation button group -->
                            <div class="btn-group float-right">
                                <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{trans('backpack::crud.language')}}: {{ $crud->model->getAvailableLocales()[request()->input('locale')?request()->input('locale'):App::getLocale()] }} &nbsp; <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    @foreach ($crud->model->getAvailableLocales() as $key => $locale)
                                        <a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}?locale={{ $key }}">{{ $locale }}</a>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @else
                @endif
                <div class="card no-padding no-border">
                    <table class="table table-striped mb-0">
                        <tbody>
                        @foreach ($crud->columns() as $column)
                            <tr>
                                <td>
                                    <strong>{!! $column['label'] !!}:</strong>
                                </td>
                                <td>
                                    @if (!isset($column['type']))
                                        @include('crud::columns.text')
                                    @else
                                        @if(view()->exists('vendor.backpack.crud.columns.'.$column['type']))
                                            @include('vendor.backpack.crud.columns.'.$column['type'])
                                        @else
                                            @if(view()->exists('crud::columns.'.$column['type']))
                                                @include('crud::columns.'.$column['type'])
                                            @else
                                                @include('crud::columns.text')
                                            @endif
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        @if ($crud->buttons()->where('stack', 'line')->count())
                            <tr>
                                <td><strong>{{ trans('backpack::crud.actions') }}</strong></td>
                                <td>
                                    @include('crud::inc.button_stack', ['stack' => 'line'])
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div><!-- /.box-body -->

                <!-- Report -->
                <div class="card no-padding no-border">
                    <h3 class="mt-2 ml-2">Report</h3>
                    <table class="table table-striped mb-0">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Periode</strong>
                                </td>
                                <td>
                                    <strong>Term</strong>
                                </td>
                                <td>
                                    <strong>Aksi</strong>
                                </td>
                            </tr>
                            @foreach($reports as $report)
                                <tr>
                                    <td>
                                        {{ $report->getSchoolPeriod()->getName() }}
                                    </td>
                                    <td>
                                        {{ $report->getTerms() }}
                                    </td>
                                    <td>
                                        <a href="/{{ config('backpack.base.route_prefix').'/student/reports/'.$report->getId().'/show' }}" target="_blank">Lihat</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->

                <!-- Address -->
                <div class="card no-padding no-border">
                    <h3 class="mt-2 ml-2">Alamat</h3>
                    <table class="table table-striped mb-0">
                        <tbody>
                            <tr>
                                <th>
                                    Alamat
                                </th>
                                <td>
                                    {{ $address->address }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Kode Pos
                                </th>
                                <td>
                                    {{ $address->postal_code }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Telepon
                                </th>
                                <td>
                                    {{ $address->phone }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Tinggal Dengan
                                </th>
                                <td>
                                    {{ $address->live_with }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Provinsi
                                </th>
                                <td>
                                    {{ $address->getProvince()->getName() }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Kota
                                </th>
                                <td>
                                    {{ $address->getCity()->getName() }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Kecamatan
                                </th>
                                <td>
                                    {{ $address->getDistrict()->getName() }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Desa
                                </th>
                                <td>
                                    {{ $address->getVillage()->getName() }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->

                <!-- Orang Tua -->
                <div class="card no-padding no-border">
                    <h3 class="mt-2 ml-2">Orang Tua</h3>

                    @foreach($parents as $parent)
                        <table class="table table-striped mb-0">
                            <tbody>
                                <tr>
                                    <th>
                                        Nama {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Tempat Lahir {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->place_of_birth }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Tempat Lahir {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->place_of_birth }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Tanggal Lahir {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->date_of_birth }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Pekerjaan {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->profession }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Pendidikan Terakhir {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->last_education }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Agama {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->religion }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Tempat Lahir {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->place_of_birth }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Ho. Handphone {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->mobile }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Email {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->email }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Tempat Kerja {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->company }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Alamat Tempat Kerja {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ $parent->office_address }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        Penghasilan {{ trans('student.'.$parent->type) }}
                                    </th>
                                    <td>
                                        {{ 'Rp. ' . number_format($parent->income) }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    @endforeach
                </div><!-- /.box-body -->

                <!-- Health -->
                <div class="card no-padding no-border">
                    <h3 class="mt-2 ml-2">Kesehatan</h3>
                    <table class="table table-striped mb-0">
                        <tbody>
                            <tr>
                                <th>
                                    TBC
                                </th>
                                <td>
                                    {{ $health->tbc }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Malaria
                                </th>
                                <td>
                                    {{ $health->malaria }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Chotipa
                                </th>
                                <td>
                                    {{ $health->chotipa }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Cacar
                                </th>
                                <td>
                                    {{ $health->cacar }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Others
                                </th>
                                <td>
                                    {{ $health->others }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Physical Abnormalities
                                </th>
                                <td>
                                    {{ $health->physical_abnormalities }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->

                <!-- Prev Education -->
                <div class="card no-padding no-border">
                    <h3 class="mt-2 ml-2">Pendidikan Sebelumnya</h3>
                    <table class="table table-striped mb-0">
                        <tbody>
                            <tr>
                                <th>
                                    Sekolah Asal
                                </th>
                                <td>
                                    {{ $previousEducation->school_origin }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Alamat
                                </th>
                                <td>
                                    {{ $previousEducation->school_address }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Telepon
                                </th>
                                <td>
                                    {{ $previousEducation->phone }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Nama Kepala Sekolah
                                </th>
                                <td>
                                    {{ $previousEducation->head_master_name }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    NISN
                                </th>
                                <td>
                                    {{ $previousEducation->nisn }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Status Sekolah
                                </th>
                                <td>
                                    {{ $previousEducation->school_status }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Sistem Pendidikan
                                </th>
                                <td>
                                    {{ $previousEducation->education_system }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Lama Sekolah
                                </th>
                                <td>
                                    {{ $previousEducation->period }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Provinsi
                                </th>
                                <td>
                                    {{ $previousEducation->getProvince()->getName() }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Kota
                                </th>
                                <td>
                                    {{ $previousEducation->getCity()->getName() }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Kecamatan
                                </th>
                                <td>
                                    {{ $previousEducation->getDistrict()->getName() }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Desa
                                </th>
                                <td>
                                    {{ $previousEducation->getVillage()->getName() }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div>
    </div>
@endsection

@section('after_styles')
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
@endsection

@section('after_scripts')
    <script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
    <script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
@endsection

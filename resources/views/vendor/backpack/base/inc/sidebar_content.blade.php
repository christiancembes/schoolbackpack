<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

@can('student')
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-user-alt"></i> Siswa</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('student/student') }}'><i class='nav-icon la la-user'></i> Aktif</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('student/waiting') }}'><i class='nav-icon la la-user'></i> Menunggu Aktifasi</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('student/waiting-payment') }}'><i class='nav-icon la la-user'></i> Menunggu Pembayaran</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('student/cancel') }}'><i class='nav-icon la la-user'></i> Batal</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('student/mutation') }}'><i class='nav-icon la la-user'></i> Mutasi</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('student/lost') }}'><i class='nav-icon la la-user'></i> Hilang</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('student/pass-away') }}'><i class='nav-icon la la-user'></i> Meninggal</a></li>
    </ul>
</li>
@endcan

@can('academic')
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-book"></i> Akademis</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('student/reports') }}'><i class='nav-icon la la-book'></i> Report Semester</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('dormitory/report/term') }}'><i class='nav-icon la la-book'></i> Report Boarding Semester</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('dormitory/report/monthly') }}'><i class='nav-icon la la-book'></i> Report Boarding Bulanan</a></li>
    </ul>
</li>
@endcan

@can('payment')
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-money"></i> Pembayaran</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('payment/payment-transaction') }}'><i class='nav-icon la la-money-bill-alt'></i> Daftar Pembayaran</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('payment/payment-method') }}'><i class='nav-icon la la-money-bill-alt'></i> Metode Pembayaran</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('payment/payment-gateway') }}'><i class='nav-icon la la-money-bill-alt'></i> Gerbang Pembayaran</a></li>
    </ul>
</li>
@endcan

@can('quotation')
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-file-invoice"></i> Tagihan</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('quotation/quotation') }}'><i class='nav-icon la la-file-invoice-dollar'></i> Daftar Tagihan</a></li>
    </ul>
</li>
@endcan

@can('dormitory')
<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-bed"></i> Asrama</a>
	<ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('dormitory/dormitory') }}'><i class='nav-icon la la-bed'></i> Daftar Asrama</a></li>
	    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('dormitory/dormitory-waiting-list') }}'><i class='nav-icon la la-user-clock'></i> Daftar Tunggu</a></li>
	</ul>
</li>
@endcan

@can('marketer')
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-user-tag"></i> Marketer</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/marketer') }}'><i class='nav-icon la la-user-tag'></i> Marketer</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/marketer-payout') }}'><i class='nav-icon la la-question'></i> Marketer Payouts</a></li>
    </ul>
</li>
@endcan

@can('master-data')
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-database"></i> Master Data</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/employee') }}'><i class='nav-icon la la-database'></i> Pegawai</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/facility') }}'><i class='nav-icon la la-database'></i> Fasilitas</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/grade') }}'><i class='nav-icon la la-database'></i> Tingkat</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/grade-class') }}'><i class='nav-icon la la-database'></i> Kelas</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/major') }}'><i class='nav-icon la la-database'></i> Jurusan</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/school-period') }}'><i class='nav-icon la la-database'></i> Periode</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/student-type') }}'><i class='nav-icon la la-database'></i> Tipe</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/subject') }}'><i class='nav-icon la la-database'></i> Mata Pelajaran</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/dormitory-subject') }}'><i class='nav-icon la la-database'></i> Mata Pelajaran Asrama</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/school-fee') }}'><i class='nav-icon la la-database'></i> Biaya Sekolah</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('master/student-category') }}'><i class='nav-icon la la-database'></i> Ketegori</a></li>
    </ul>
</li>
@endcan

@can('administration')
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-map"></i> Administrasi</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('general/country') }}'><i class='nav-icon la la-map'></i> Negara</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('general/province') }}'><i class='nav-icon la la-map'></i> Provinsi</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('general/city') }}'><i class='nav-icon la la-map'></i> Kota/Kabupaten</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('general/district') }}'><i class='nav-icon la la-map'></i> Kecamatan</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('general/village') }}'><i class='nav-icon la la-map'></i> Kelurahan/Desa</a></li>
    </ul>
</li>
@endcan

@can('permission')
<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Pengguna & Izin</a>
	<ul class="nav-dropdown-items">
	  <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
	  <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
	  <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
	</ul>
</li>
@endcan
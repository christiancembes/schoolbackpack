@extends(backpack_view('blank'))

@php
    /** $widgets['before_content'][] = [
        'type'        => 'jumbotron',
        'heading'     => trans('backpack::base.welcome'),
        'content'     => trans('backpack::base.use_sidebar'),
        'button_link' => backpack_url('logout'),
        'button_text' => trans('backpack::base.logout'),
    ]; */

    Widget::add()->to('after_content')->type('div')->class('row')->content([
        Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-primary')
			->progressClass('progress-bar')
			->value($activeStudentCount)
			->description('Siswa Aktif.'),

		Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-primary')
			->progressClass('progress-bar')
			->value($waitingStudentCount)
			->description('Siswa Menunggu Aktifasi.'),

		Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-primary')
			->progressClass('progress-bar')
			->value($waitingPaymentStudentCount)
			->description('Siswa Menunggu Pembayaran Registrasi.'),

		Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-primary')
			->progressClass('progress-bar')
			->value($waitingFullPaymentStudentCount)
			->description('Siswa Menunggu Pelunasan.'),
    ]);

    Widget::add()->to('after_content')->type('div')->class('row')->content([
        Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-primary')
			->progressClass('progress-bar')
			->value($dormitory['total_capacity'])
			->description('Kapasitas Kasur Asrama'),

		Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-primary')
			->progressClass('progress-bar')
			->value($dormitory['total_capacity'] - $dormitory['total_remaining_capacity'])
			->description('Total Booking Kasur Asrama'),

		Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-primary')
			->progressClass('progress-bar')
			->value($studentQuota['capacity'])
			->description('Kuota '.$schoolPeriod->getName()),

		Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-primary')
			->progressClass('progress-bar')
			->value($studentQuota['remaining_capacity'])
			->description('Sisa Kuota '.$schoolPeriod->getName()),
    ]);

    $widgets['after_content'][] = [
        'type' => 'div',
        'class' => 'row',
        'content' => [ // widgets

            [
                'type' => 'chart',
                'wrapperClass' => 'col-md-6',
                'controller' => \App\Http\Controllers\Admin\Charts\StudentByGender::class,
                'content' => [
                    'header' => 'Siswa L/P', // optional
                ]
            ],
            [
                'type' => 'chart',
                'wrapperClass' => 'col-md-6',
                'controller' => \App\Http\Controllers\Admin\Charts\StudentBoarding::class,
                'content' => [
                    'header' => 'Siswa Fullday/Boarding', // optional
                ]
            ],
            [
                'type' => 'chart',
                'wrapperClass' => 'col-md-6',
                'controller' => \App\Http\Controllers\Admin\Charts\StudentBoardingByGender::class,
                'content' => [
                    'header' => 'Siswa Boarding L/P', // optional
                ]
            ],
            [
                'type' => 'chart',
                'wrapperClass' => 'col-md-6',
                'controller' => \App\Http\Controllers\Admin\Charts\StudentFulldayByGender::class,
                'content' => [
                    'header' => 'Siswa Fullday L/P', // optional
                ]
            ],

        ]
    ];
@endphp

@section('content')
    {{-- In case widgets have been added to a 'content' group, show those widgets. --}}
    @include(backpack_view('inc.widgets'), [ 'widgets' => app('widgets')->where('group', 'content')->toArray() ])
@endsection